'use strict';

app.claims = kendo.observable({
    onShow: function () {
        rotateForwards();
         if (window.cordova && window.navigator.simulator !== true) {
            window.screen.unlockOrientation();
         } // if (window.cordova && window.navigator.simulator

        if (localStorage.getItem("appData") === null) {
            localStorage.setItem("appData", "")
        }
        if (typeof JSON.parse(localStorage.getItem("appData")).claims !== "object") {
            $("#claimsList").empty();
            $("#claimsList").prepend('<li class="detailsBack"><center><span>There are no previous claims for this member.</span></center></li>');
        } // if (typeof JSON.parse(localStorage.getItem("appData")).claims != "object") {
        else {
            var claimsList = JSON.parse(localStorage.getItem("appData")).claims;
            $('#claimsList').empty();
            if (claimsList.length === 0) {
                $("#claimsList").prepend('<li class="detailsBack"><center><span>There are no previous claims for this member.</span></center></li>');
            } // if (claimsList.length == 0)
            else {
                $("#claimsList").prepend("<tr class='bold headingsDiv' style='padding-bottom:10px'><td class='claimsTableBorder'>Claim Reference</td><td class='claimsTableBorder hideThis'>Incident Date</td><td class='claimsTableBorder'>Incident Type</td><td class='hideThis claimsTableBorder'>Amount</td><td class='hideThis claimsTableBorder'>Product Code</td><td class='hideThis claimsTableBorder'>Claim Status</td></tr>");
                jQuery.each(claimsList, function (i) {
                    $("#claimsList").append("<tr><td class=''>" + claimsList[i]["claimReference"] + "</td><td class='hideThis'>" + claimsList[i]["incidentDate"] + "</td><td class=''>" + claimsList[i]["incidentType"] + "</td><td class='hideThis'>" + claimsList[i]["amount"] + "</td><td class='hideThis'>" + claimsList[i]["productCode"] + "</td><td class='hideThis'>" + claimsList[i]["claimStatus"] + "</td></tr>")
                }); // jQuery.each(claimsList, function (index)
            } // else
        } // else 
    }, // onShow: function ()
    afterShow: function () {}
}); //app.claims = kendo.observable

// START_CUSTOM_CODE_claims
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
(function (parent) {
    var claimsModel = kendo.observable({
        fields: {},

    });
    parent.set("claimsModel", claimsModel)

})(app.claims)
// END_CUSTOM_CODE_claims