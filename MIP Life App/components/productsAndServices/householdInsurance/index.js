'use strict';

app.householdInsurance = kendo.observable({
    onShow: function () {
        if (localStorage.getItem("appData") === null) {
            localStorage.setItem("appData", "")
        }
    },
    afterShow: function () {}
}); // app.householdInsurance = kendo.observable

// START_CUSTOM_CODE_householdInsurance
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
(function (parent) {
    var householdInsuranceModel = kendo.observable({
        fields: {
        },
        callMeBack: function () {
            window.open("http://hollard.mobi/call-me-back/household-insurance", "_blank");
        }
     }); // var householdInsuranceModel = kendi.observable 

    parent.set('householdInsuranceModel', householdInsuranceModel)

})(app.householdInsurance); // function(parent)


// END_CUSTOM_CODE_householdInsurance