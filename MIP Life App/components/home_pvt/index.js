'use strict';

app.home = kendo.observable({
    onShow: function () {
        if (localStorage.getItem("appData") === null) {
            localStorage.setItem("appData", "");
        }        
    },
    afterShow: function () {
    }
}); 

// START_CUSTOM_CODE_home
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
function checkLogin(screen) {
    if (localStorage.getItem("appData") === null) {
        localStorage.setItem("appData", "")
    } // if (localStorage.getItem("appData"))
    if (localStorage.getItem("mnt:sessionid") === null) {
        localStorage.setItem("mnt:sessionid", "");
    } // if (localStorage.getItem("mnt:sessionid") === null)
    if (localStorage.getItem("mnt:sessionid") !== "") { 
        window.app.mobileApp.navigate(screen, "slide");
    } // if (localStorage.getItem("mnt:sessionid") !== "")
    else {
        localStorage.setItem("nextScreen", screen);
        window.app.mobileApp.navigate("components/login/view.html", "slide");
    } // else 
};

(function (parent) {
    var homeModel = kendo.observable({
        viewMembershipDetails: function () {
            checkLogin("components/membershipDetails/view.html");
        }, // viewMembershipDetails

        viewPersonalDetails: function () {
            checkLogin('components/personalDetails/view.html', 'slide');
        }, // viewPersonalDetails

        viewClaims: function () {
            checkLogin('components/claims/view.html', 'slide');
        }, // viewClaims

        viewPolicies: function () {
            checkLogin('components/policies/view.html', 'slide');
        }, // viewPolicies        

        viewCarInsurance: function () {
            window.app.mobileApp.navigate('components/productsAndServices/carInsurance/view.html', 'slide');
        }, // viewCarInsurance

        viewExtendedWarranty: function () {
            window.app.mobileApp.navigate('components/productsAndServices/extendedCarWarranty/view.html', 'slide');
        },  // viewExtendedWarranty

        viewFuneralInsurance: function () {
            window.app.mobileApp.navigate('components/productsAndServices/funeralInsurance/view.html', 'slide');
        }, // viewFuneralInsurance

        viewHouseholdInsurance: function () {
            window.app.mobileApp.navigate('components/productsAndServices/householdInsurance/view.html', 'slide');
        }, // viewHouseholdInsurance

        viewLegalInsurance: function () {
            window.app.mobileApp.navigate('components/productsAndServices/legalInsurance/view.html', 'slide');
        }, // viewLegalInsurance

        viewLifeInsurance: function () {
            window.app.mobileApp.navigate('components/productsAndServices/lifeInsurance/view.html', 'slide');
        }, // viewLifeInsurance

        viewLegalContracts: function () {
            window.app.mobileApp.navigate('components/helpYourself/legalContracts/view.html', 'slide');
        }, // viewLegalContracts

        viewGlossaryOfTerms: function () {
            window.app.mobileApp.navigate('components/helpYourself/glossaryOfTerms/view.html', 'slide');
        }, // viewGlossaryOfTerms

        viewFaq: function () {
            window.app.mobileApp.navigate('components/faq/view.html', 'slide');
        }, // viewFaq

        viewContactUs: function () {
            window.app.mobileApp.navigate('components/contactUs/view.html', 'slide');
        }, // viewContactUs

        viewLogout: function () {
            window.app.mobileApp.navigate('components/logout/view.html', 'slide');
        }, // viewLogout
        viewLogin: function () {
            window.app.mobileApp.navigate('components/login/view.html', 'slide');
        }, // viewLogout
		viewCovisionApplication: function () {
            window.app.mobileApp.navigate('components/covisionApplication/step_1.html', 'slide');
        }, // viewCovisionApplication
        viewCovisionApplicationUpload: function () {
            window.app.mobileApp.navigate('components/covisionApplicationUpload/view.html', 'slide');
        }, // viewCovisionApplicationUpload
        viewGetAQuote: function () {
            window.app.mobileApp.navigate('components/quote/view.html', 'slide');
        } // viewGetAQuote
    });

    parent.set('homeModel', homeModel)
})(app.home)
// END_CUSTOM_CODE_home