'use strict';

app.legalInsurance = kendo.observable({
    onShow: function () {
        if (localStorage.getItem("appData") === null) {
            localStorage.setItem("appData", "")
        }
    },
    afterShow: function () {}
}); // app.legalInsurance = kendo.observable

// START_CUSTOM_CODE_legalInsurance
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
(function (parent) {
    var legalInsuranceModel = kendo.observable({
        fields: {
        },
        callMeBack: function() {
            window.open("http://hollard.mobi/call-me-back/legal-insurance", "_blank");
        }
    }); // var legalInsuranceModel = kendi.observable 

    parent.set('legalInsuranceModel', legalInsuranceModel)

})(app.legalInsurance); // function(parent)


// END_CUSTOM_CODE_legalInsurance