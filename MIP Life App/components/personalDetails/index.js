'use strict';

app.personalDetails = kendo.observable({
    onShow: function () {
        if (localStorage.getItem("appData") === null) {
            localStorage.setItem("appData", "")
        }
        this.model.personalDetailsModel.set("personalDetails", JSON.parse(localStorage.getItem("appData")).personalDetails[0]);
        this.model.personalDetailsModel.set("membershipDetails", JSON.parse(localStorage.getItem("appData")).membershipDetails[0]);
        console.log(this.model.personalDetailsModel.personalDetails.firstName + " " + this.model.personalDetailsModel.personalDetails.lastName);
        this.model.personalDetailsModel.set("fullName", this.model.personalDetailsModel.personalDetails.firstName + " " + this.model.personalDetailsModel.personalDetails.lastName);
    }, // onSHow: function ()
    afterShow: function () {} // afterShow: function ()
}); // app.personalDetails = kendo.observable
(function (parent) {
    var personalDetailsModel = kendo.observable({
        fields: {}, // fields
        editAddress: function () {
            window.app.mobileApp.navigate("components/personalDetails/address/view.html", "slide");            
        }, // editAddress: function ()

        editContact: function () {
            window.app.mobileApp.navigate("components/personalDetails/contact/view.html", "slide");
        }, // editContact: function ()

    }); // var personalDetailsModel = kendo.observable
    parent.set('personalDetailsModel', personalDetailsModel);
})(app.personalDetails); // (function (parent) )