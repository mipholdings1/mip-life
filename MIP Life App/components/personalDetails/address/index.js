'use strict';

app.address = kendo.observable({
    onShow: function() {
        if (localStorage.getItem("appData") == null) {
            localStorage.setItem("appData", "")
        }
        this.model.addressModel.set("personData", JSON.parse(localStorage.getItem("appData")).personalDetails[0]);
        this.model.addressModel.set("memberData", JSON.parse(localStorage.getItem("appData")).membershipDetails[0]);

    },
    afterShow: function() {}
}); // app.address = kendo.observable 


// START_CUSTOM_CODE_address
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
(function(parent) {
    var addressModel = kendo.observable({
        fields: {}, // fields

        cancel: function() {
            window.app.mobileApp.navigate("components/personalDetails/view.html", "slide");
        }, // cancel: function ()

        confirm: function() {
            var form = document.getElementById("updateAddressDetailsForm"),
                message = "Oops! There is invalid data in the form.",
                options = {
                    title: 'Invalid Fields',
                    buttonText: 'OK',
                    callback: function() {}
                } // var options
            if (validator.validate()) {
                console.log("validation successfull");
                console.log(addressModel.memberData.memberNumber + " " + addressModel.personData.addressLine1 + " " + addressModel.personData.addressLine2 + " " + addressModel.personData.addressLine3 + " " + addressModel.personData.addressLine4 + " " + addressModel.personData.addressLine5)
                var restParams1 = {
                    service: "mnt",
                    parameters: {
                        rqAuthentication: "user:admin|mip123" + "|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                        rqService: "ContractService:updateAddressInfo",
                        rqDataMode: "VAR/JSON",
                        ipcContractNumber: addressModel.memberData.memberNumber,
                        ipcAddressLine1: addressModel.personData.addressLine1,
                        ipcAddressLine2: addressModel.personData.addressLine2,
                        ipcAddressLine3: addressModel.personData.addressLine3,
                        ipcAddressLine4: addressModel.personData.addressLine4,
                        ipcAddressLine5: addressModel.personData.addressLine5,
                        ipcPostCode: addressModel.personData.postCode
                    }, // parameters

                    requestSuccess: function(data) {
                        // this will download and view the PDF document
                        // after making sure the user has a valid session to 

                        if (data.rqResponse.rqErrorMessage) {
                            if (data.rqResponse.rqFullError.indexOf('mip_MsgSesErr:6') > 0) {
                                window.app.mobileApp.navigate("components/login/view.html", "slide");
                                localStorage.setItem("nextScreen", "components/personalDetails/address/view.html");
                                //navigateTo = localStorage.getItem("loginView");
                            } // if (data.rqFullError.indexOf
                            else {
                                /*navigator.notification.alert(
                                    data.rqResponse.rqErrorMessage, // the message
                                    function() {}, // a callback
                                    "Error", // a title
                                    "OK" // the button text
                                ); // navigator.notification.alert*/  
                            } // else 
                        } //  if (data.rqResponse.rqFullError == "" || data.rqResponse.rqFullError == null)
                        else {
                            var restParams2 = {
                                service: "mnt",
                                parameters: {
                                    rqAuthentication: "user:admin|mip123" + "|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                                    rqService: "ContractService:fetchMembershipInfo",
                                    rqDataMode: "VAR/JSON"
                                }, // parameters

                                requestSuccess: function(data) {
                                    console.log(data)
                                    localStorage.setItem("appData", JSON.stringify(data.rqResponse.membershipInfo));
                                    mipAlert.show("Your address details have been successfully updated");
                                    window.app.mobileApp.navigate("components/personalDetails/view.html", "slide");
                                }, // requestSuccess

                                requestError: function(d, e, c) {
                                    mipAlert.show(e, {
                                        title: c
                                    });
                                }, // requestError

                                spinner: {
                                    message: "Refreshing Information...",
                                    title: "Saving",
                                    isFixed: true
                                }, // spinner

                                availableOfflineFor: {
                                    minutes: 120
                                } // availableOfflineFor

                            }; // restParams2

                            mipRest.request(restParams2); // updates the dataset and local storage

                        } // else
                    }, // requestSuccess


                    requestError: function(d, e, c) {
                        mipAlert.show(e, {
                            title: c
                        });

                    }, // requestError

                    spinner: {
                        message: "Saving. Please wait...",
                        title: "Saving",
                        isFixed: true
                    }, // spinner

                    availableOfflineFor: {
                        minutes: 120
                    } // availableOfflineFor
                }; // restParams1

                mipRest.request(restParams1);
            } // if (validator.validate())
            else {
                navigator.notification.alert(
                        message, // the message
                        function() {}, // a callback
                        "Invalid Data", // a title
                        "OK" // the button text
                    ) // navigator.notification.alert
            } // else

        }, // confirm: function ()

    }); // var personalDetailsModel = kendo.observable
    parent.set('addressModel', addressModel);
})(app.address); // (function (parent) )
// END_CUSTOM_CODE_addressv
