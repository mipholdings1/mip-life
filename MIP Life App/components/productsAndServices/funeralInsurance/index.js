'use strict';

app.funeralInsurance = kendo.observable({
    onShow: function () {
        localStorage.setItem("screen", "components/funeralInsurance/view.html");
        if (localStorage.getItem("appData") == null) {
            localStorage.setItem("appData", "")
        }
    },
    afterShow: function () {}
}); // app.funeralInsurance = kendo.observable

// START_CUSTOM_CODE_funeralInsurance
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
(function (parent) {
    var funeralInsuranceModel = kendo.observable({
        callMeBack: function () {
            // window.open("http://hollard.mobi/call-me-back/funeral-insurance", "_blank");
        },
        gotoLifeInsurance: function () {
             window.app.mobileApp.navigate("components/productsAndServices/lifeInsurance/view.html", "slide");
        }
         
    }); // var goldPlusMembershipModel = kendi.observable 

    parent.set('funeralInsuranceModel', funeralInsuranceModel)

})(app.funeralInsurance); // function(parent)

// END_CUSTOM_CODE_funeralInsurance