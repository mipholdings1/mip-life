'use strict';

app.covisionApplicationUpload = kendo.observable({
    onShow: function() {
        localStorage.setItem("screen", "components/covisionApplicationUpload/view.html");
        if (localStorage.getItem("appData") === null) {
            localStorage.setItem("appData", "");
            console.log("");
        }
        console.log(localStorage.getItem("appData"));
        if (app.mobileApp.view().id === "components/covisionApplicationUpload/view_sign.html") {
            $('#sign2').signature();
        }
    },

    afterShow: function() {} // afterSHow: function () 
}); // app.covisionApplicationUpload = kendo.observable 

// START_CUSTOM_CODE_covisionApplicationUpload
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
(function(parent) {
    var covisionApplicationUploadModel = kendo.observable({
        fields: {
            firstName: "",
            lastName: "",
            IDNumber: "",
            email: "",
            cellNumber: ""
        },
        uploadForm: function() {
            navigator.notification.confirm(
                'Please take a picture of your application form now.', // message
                uploadCovisionForm, // callback to invoke with index of button pressed
                'Covision Application Form', // title
                ['Ok'] // buttonLabels
            );

            function alertSuccess() {}

            function uploadCovisionForm(pressedButton) {
                console.log(pressedButton);
                if (pressedButton == 1) {
                    navigator.camera.getPicture(onSuccess, onFail, {
                        quality: 100,
                        destinationType: Camera.DestinationType.DATA_URL,
                        sourceType: 1
                    });
                }
            }
            var onSuccess = function(DATA_URL) {
                // // mipSpinner.hide();
                navigator.notification.alert(
                    "Thank you for uploading your Application. Please remember to also take a picture of your ID Document.", // the message
                    alertSuccess, // a callback
                    "Success", // a title
                    "OK" // the button text
                ); // navigator.notification.alert
                console.log(DATA_URL);
            }
            var onFail = function(e) {
                console.log(e);
                navigator.notification.alert(
                    e, // the message
                    alertSuccess, // a callback
                    "Error", // a title
                    "OK" // the button text
                ); // navigator.notification.alert
            }

        },

        uploadIDDoc: function() {
            navigator.notification.confirm(
                'Please take a picture of your ID Book now.', // message
                uploadId, // callback to invoke with index of button pressed
                'ID Book Form', // title
                ['Ok'] // buttonLabels
            );

            function alertSuccess() {}

            function uploadId(pressedButton) {
                console.log(pressedButton);
                if (pressedButton == 1) {
                    navigator.camera.getPicture(onSuccess, onFail, {
                        quality: 50,
                        destinationType: Camera.DestinationType.DATA_URL,
                        sourceType: 1
                    });
                    //mipSpinner.show();
                }
            }

            var onSuccess = function(DATA_URL) {
                mipSpinner.show()
                var year = '19' + covisionApplicationUploadModel.fields.IDNumber.toString().substring(0, 2),
                    month = covisionApplicationUploadModel.fields.IDNumber.toString().substring(2, 4),
                    day = covisionApplicationUploadModel.fields.IDNumber.toString().substring(4, 6),
                    dob = year + '/' + month + '/' + day,
                    gender = covisionApplicationUploadModel.fields.IDNumber.toString().substring(6, 10);

                if (parseInt(gender) >= 5000) {
                    var gen = 'MAL',
                        title = 'Mr';
                } else {
                    var gen = 'FEM',
                        title = 'Miss';
                }

                var blob = new Blob([DATA_URL], {
                    type: 'text/txt'
                });

                var fd = new FormData();
                fd.append('rqAuthentication', localStorage.getItem('mnt:sessionid') + '|mip123|GSMUS|current_process_date=2012/09/27&login_company_branch_obj=91211.878*102594.188');
                fd.append('rqDataMode', 'VAR/JSON');
                fd.append('rqService', 'pfmanager:CreateMtnPolicy');
                fd.append('cDOB', dob);
                fd.append('cFirstName', covisionApplicationUploadModel.fields.firstName);
                fd.append('cLastName', covisionApplicationUploadModel.fields.lastName);
                fd.append('cInitials', covisionApplicationUploadModel.fields.firstName.substring(0, 1));
                fd.append('cGender', gen);
                fd.append('cMaritalStatus', 'SIN');
                fd.append('cTitle', title);
                fd.append('cIdNumber', covisionApplicationUploadModel.fields.IDNumber.toString());
                fd.append('cCellPhone', covisionApplicationUploadModel.fields.cellNumber);
                fd.append('cEmail', covisionApplicationUploadModel.fields.email);

                fd.append('Filedata', blob);

                $.ajax({
                    type: "POST",
                    url: 'http://1502.life.mip.co.za/cgi-bin/wspd_113.sh/WService=wsb_881mnt/rest.w',
                    data: fd,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        console.log(data);

                        if (data.rqResponse.rqErrorMessage) {
                            mipSpinner.hide();
                            mipSpinner.hide();

                            navigator.notification.alert(
                                data.rqResponse.rqErrorMessage, // the message
                                alertSuccess, // a callback
                                "Error", // a title
                                "OK" // the button text
                            ); // navigator.notification.alert

                        } else {
                            mipSpinner.hide();
                            navigator.notification.alert(
                                "Thank you for uploading your ID Document.", // the message
                                alertSuccess, // a callback
                                "Success", // a title
                                "OK" // the button text
                            ); // navigator.notification.alert
                            covisionApplicationUploadModel.set("quote", data.rqResponse);
                            console.log(data);
                        }
                    },
                    error: function(data, error, code) {
                        mipSpinner.hide();

                        navigator.notification.alert(
                            error, // the message
                            alertSuccess, // a callback
                            "Error", // a title
                            "OK" // the button text
                        ); // navigator.notification.alert


                    }
                });
            };

            var onFail = function(e) {
                navigator.notification.alert(
                    e, // the message
                    function() {}, // a callback
                    "Error", // a title
                    "OK" // the button text
                ); // navigator.notification.alert
            };
        },

        submitForm: function() {
            console.log("Submit form...");

            function alertSuccess() {
                window.app.mobileApp.navigate('components/home/view.html', 'slide');
            }
            navigator.notification.alert(
                "Thank you for your application.", // the message
                alertSuccess, // a callback
                "Success", // a title
                "OK" // the button text
            ); // navigator.notification.alert
        },

        downloadForm: function() {
            console.log("Download form...");
        },
        acceptQuote: function() {

            mipSpinner.show('Accepting Quote', '', {
                isFixed: true
            });
            var blob = new Blob([$('#sign2').data("mipSignature").signaturePad.toDataURL("image/jpeg")], {
                type: 'text/txt'
            });

            var fd = new FormData();
            fd.append('rqAuthentication', localStorage.getItem('mnt:sessionid') + '|GSMUS|current_process_date=2012/09/27&login_company_branch_obj=91211.878*102594.188');
            fd.append('rqDataMode', 'VAR/JSON');
            fd.append('rqService', 'pfmanager:restPutInforce');
            fd.append('pdContractObj', quoteModel.quote.cContractObj);
            fd.append('Filedata', blob);

            $.ajax({
                type: "POST",
                url: 'http://1502.life.mip.co.za/cgi-bin/wspd_113.sh/WService=wsb_881mnt/rest.w',
                data: fd,
                processData: false,
                contentType: false,
                success: function(data) {

                    mipSpinner.hide();
                    if (data.rqResponse.rqErrorMessage) {
                        navigator.notification.alert(
                            data.rqResponse.rqErrorMessage, // the message
                            function() {}, // a callback
                            "Error", // a title
                            "OK" // the button text
                        ); // navigator.notification.alert

                    } else {
                        navigator.notification.alert(
                            'Congratulations on your new policy! Your login details will be be smsed to you shortly.', // the message
                            function() {}, // a callback
                            "Success", // a title
                            "OK" // the button text
                        ); // navigator.notification.alert
                        window.app.mobileApp.navigate("components/home/view.html", "slide");
                    }
                },
                error: function(data, error, code) {
                    mipSpinner.hide();
                    navigator.notification.alert(
                        error, // the message
                        function() {}, // a callback
                        "Error", // a title
                        "OK" // the button text
                    ); // navigator.notification.alert

                }
            });
        },
        declineQuote: function() {
            quoteModel.set("quote", "");
            window.app.mobileApp.navigate("components/home/view.html", "slide");
        }

    });
    parent.set('covisionApplicationUploadModel', covisionApplicationUploadModel);
})(app.covisionApplicationUpload) // (function(parent)
// END_CUSTOM_CODE_covisionApplicationUpload