'use strict';

app.dashboard = kendo.observable({
    onShow: function () {
        var ttContractStatusCount = JSON.parse(localStorage.getItem("appData")).rqResponse.dsContractStatusCount.ttContractStatusCount,
            INF = [],
            NEW = [],
            CAN = [],
            range = [];
        if (typeof ttContractStatusCount == "object") {
            for (var record = 0; record < ttContractStatusCount.length; record++) {
                switch (ttContractStatusCount[record].cStatusCode) {
                    case "INF":
                        INF.push(ttContractStatusCount[record].iContractCount);
                        break;
                    case "NEW":
                        NEW.push(ttContractStatusCount[record].iContractCount);
                        break;
                    case "CAN":
                        CAN.push(ttContractStatusCount[record].iContractCount);
                        break;
                    default:
                        console.log("default");
                        //default code block
                }

                if (jQuery.inArray(ttContractStatusCount.cPeriodString, range)) {
                    // Dont Push to the Array
                    console.log("dont")
                    console.log((ttContractStatusCount[record].cPeriodString));
                   // range.push(ttContractStatusCount[record].cPeriodString)
                } else {
                    console.log("push")
                    // range.push(ttContractStatusCount[record].cPeriodString);
                }
                if (range.indexOf(ttContractStatusCount[record].cPeriodString)) {
                    // Dont Push to the Array
                    console.log("dont2")
                    console.log((ttContractStatusCount[record].cPeriodString));
                   range.push(ttContractStatusCount[record].cPeriodString)
                } else {
                    console.log("push2")
                    // range.push(ttContractStatusCount[record].cPeriodString);
                }


            }
        }
        console.log(INF);
        console.log(NEW);
        console.log(CAN);
        console.log(range)
        
        // Create the Chart
        function createChart() {
            $("#chart").kendoChart({
                title: {
                    text: "Performance"
                },
                legend: {
                    position: "bottom"
                },
                seriesDefaults: {
                    type: "column"
                },
                series: [{
                    name: "In-Force",
                    data: INF
                }, {
                    name: "New",
                    data: NEW
                }, {
                    name: "Cancelled",
                    data: CAN
                }],
                valueAxis: {
                    labels: {
                        format: "{0}"
                    },
                    line: {
                        visible: false
                    },
                    axisCrossingValue: 0
                },
                categoryAxis: {
                    categories: ["May",  "Jun", "Jul", "Aug"],
                    /* line: {
                        visible: false
                    }, */ 
                },
                tooltip: {
                    visible: true,
                    format: "{0}",
                    template: "#= series.name #: #= value #"
                }
            });
        }

        $(document).ready(createChart);
        $(document).bind("kendo:skinChange", createChart);
    }, // onShow: function ()
    afterShow: function () {} // afterShow: function () 
}); //app.dashboard = kendo.observable

// START_CUSTOM_CODE_dashboard
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
(function (parent) {
    var dashboardModel = kendo.observable({
        fields: {}, // fields:

    }); // var dashboardModel = kendo.observable
    parent.set("dashboardModel", dashboardModel)

})(app.dashboard)
// END_CUSTOM_CODE_dashboard