(function() {
    // store a reference to the application object that will be created
    // later on so that we can use it if need be
    var app = {
        data: {}
    };
    kendo.UserEvents.defaultThreshold(20);
    //chad did this
    var bootstrap = function() {
        $(function() {
            app.mobileApp = new kendo.mobile.Application(document.body, {
                useNativeScrolling: true,
                // you can change the default transition (slide,  zoom or fade) 
                transition: 'slide',
                // comment out the following line to get a UI which matches the look
                // and feel of the operating system
                skin: 'flat',
                // the application needs to know which view to load first
                // initial: 'components/covisionApplication/step_5.html',
                initial: 'components/login/view.html'
            });

            var mip = Mip.Application.init({
                debug: false,
                kendoApp: app.mobileApp,
                services: {
                    // mnt: "https://nrt.hollardlifeadmin.co.za/cgi-bin/wspd_nrt.sh/WService=wsb_rsanrt/rest.w"
                    mnt: "http://1502.life.mip.co.za/cgi-bin/wspd_116.sh/WService=wsb_881mnt/rest.w"
                },
                loginView: 'components/login/view.html'
            });

            mipRest = mip.getService('Rest');
            mipAlert = mip.getService('Alert');
            mipFingerPrint = mip.getService('FingerPrint');
            mipAuth = mip.getService('Auth');
            mipDevice = mip.getService('Device');
            mipSpinner = mip.getService('Spinner');
            mipEmail = mip.getService('sendEmail');
            mipPdf = mip.getService('Pdf');
            mipUtilities = mip.getService('Utilities');

            // setting local variables to facilitate the control generation in the covision application form.
            localStorage.setItem("childTotal", 0);
            localStorage.setItem("partnerTotal", 0);
            localStorage.setItem("parentTotal", 0);
            localStorage.setItem("extendedMemberTotal", 0);

        });
    };

    if (window.cordova) {
        // this function is called by Cordova when the application is loaded by the device
        document.addEventListener('deviceready', function() {
            // enables app feedback
            // feedback.initialize('oslke2b04e3s4h6m');
            // hide the splash screen as soon as the app is ready. otherwise
            // Cordova will wait 5 very long seconds to do it for you.
            // if (navigator && navigator.splashscreen) {
            navigator.splashscreen.hide();
            // }

            var element = document.getElementById('appDrawer');
            if (typeof(element) !== 'undefined' && element !== null) {
                if (window.navigator.msPointerEnabled) {
                    $("#navigation-container").on("MSPointerDown", "a", function(event) {
                        app.keepActiveState($(this));
                    });
                } else {
                    $("#navigation-container").on("touchstart", "a", function(event) {
                        app.keepActiveState($(this));
                    });
                }
            }
            document.addEventListener("pause", onPause, false);
            document.addEventListener("resume", onResume, false);
            
            document.addEventListener('backbutton', function (evt) {
                if (localStorage.getItem("screen") != "components/home/view.html") {
                    window.app.mobileApp.navigate("components/home/view.html", "slide:reverse");
                    localStorage.setItem("screen", "components/home/view.html");
                } else if (localStorage.getItem("screen") == "components/home/view.html") {
                    navigator.notification.confirm("Are you sure you want to exit ?", function (buttonIndex) {
                        if (buttonIndex == "1") {
                            navigator.app.exitApp();
                        } else if (buttonIndex == "2") {
                            //dismiss message.
                            return;
                        }
                    }, "Confirmation", "Yes,No");
                }
            }, false);
            
            // localStorage.setItem("mnt:sessionid", "");
            localStorage.setItem("nextScreen", "");
            if (window.cordova && window.navigator.simulator !== true) {
                window.screen.lockOrientation('portrait');
            } //  if (window.cordova && window.navigator.simulator 

            bootstrap();
        }, false);
    } else {
        bootstrap();
    }

    app.keepActiveState = function _keepActiveState(item) {
        var currentItem = item;
        $("#navigation-container li a.active").removeClass("active");
        currentItem.addClass('active');
    };

    window.app = app;

    app.isOnline = function() {
        if (!navigator || !navigator.connection) {
            return true;
        } else {
            return navigator.connection.type !== 'none';
        }
    };
}());

function onPause() {
    //localStorage.setItem("mnt:sessionid", "");
    localStorage.setItem("pauseTime", "");
    localStorage.setItem("pauseTime", new Date().toString());
    //alert("pause" + localStorage.getItem("pauseTime"));
}

function onResume() {
    var curTime = new Date();
    var pauseTime = new Date(localStorage.getItem("pauseTime"));
    var diff = curTime.valueOf() - pauseTime.valueOf();
    var diffFormatted = diff / 1000 / 60; // Convert milliseconds to minuites

    if (diffFormatted > 10) { // will only logout if more than 10 minuites have passed
        localStorage.setItem("mnt:sessionid", "Session:"); // loggs out
        window.app.mobileApp.navigate("components/home/view.html", "none"); // going home
    }

}

var dontRun = false;
var brandNewDependent = true;
var fromQuote = false;