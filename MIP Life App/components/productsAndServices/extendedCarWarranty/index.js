'use strict';

app.extendedCarWarranty = kendo.observable({
    onShow: function () {
        if (localStorage.getItem("appData") === null) {
            localStorage.setItem("appData", "")
        }
    },
    afterShow: function () {}
}); // app.extendedCarWarranty = kendo.observable

// START_CUSTOM_CODE_extendedCarWarranty
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
(function (parent) {
    var extendedCarWarrantyModel = kendo.observable({
        fields: {
 
        },
        callMeBack: function () {
            window.open("http://hollard.mobi/call-me-back/extended-car-warranty", "_blank")
        },

    }); // var extendedCarWarrantyModel = kendi.observable 

    parent.set('extendedCarWarrantyModel', extendedCarWarrantyModel)

})(app.extendedCarWarranty); // function(parent)


// END_CUSTOM_CODE_extendedCarWarranty