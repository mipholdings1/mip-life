'use strict';

app.carInsurance = kendo.observable({
    onShow: function() {
        if (localStorage.getItem("appData") == null) {
            localStorage.setItem("appData", "")
        }
    },
    afterShow: function() {}
}); // app.carInsurance = kendo.observable

// START_CUSTOM_CODE_carInsurance
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes

(function(parent) {
    var carInsuranceModel = kendo.observable({
        callMeBack: function() {
            window.open("http://hollard.mobi/call-me-back/car-insurance", "_blank");
        },
        gotoHouseholdInsurance: function() {
            window.app.mobileApp.navigate("components/productsAndServices/householdInsurance/view.html", "slide");
        }
     }); // var carInsuranceModel = kendi.observable 

    parent.set('carInsuranceModel', carInsuranceModel);

})(app.carInsurance); // function(parent)


// END_CUSTOM_CODE_carInsurance