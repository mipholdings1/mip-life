'use strict';

app.logout = kendo.observable({
    onShow: function () {},
    afterShow: function () {}
});

// START_CUSTOM_CODE_logout
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes

// END_CUSTOM_CODE_logout
(function (parent) {
    var logoutModel = kendo.observable({
        fields: {},
        submit: function () {
			localStorage.setItem("appData", "");
            localStorage.setItem("mnt:sessionid", "");
            localStorage.setItem("appEnvironment", "Public");

            if (localStorage.getItem("mnt:sessionid") === "") {
                navigator.notification.alert(
                    "Successfully logged out.", // the message
                    function () {
                        window.app.mobileApp.navigate("components/home/view.html", "slide");
                    }, // a callback function ()
                    "Logout", // a title
                    "OK" // the button text
                ); // navigator.notification.alert                

            } else {
                navigator.notification.alert(
                    "Did not successfully log out.", // the message
                    function () {}, // a callback
                    "Logout", // a title
                    "OK" // the button text
                );
            }
        }, // submit: function ()
        cancel: function () {
                window.app.mobileApp.navigate("components/home/view.html");
            } // cancel: function ()
    });

    parent.set('logoutModel', logoutModel);
})(app.logout);

// START_CUSTOM_CODE_logoutModel
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes

// END_CUSTOM_CODE_logoutModel