'use strict';

app.login = kendo.observable({
    onShow: function() {
        if (localStorage.getItem("agentData") == null) {
            localStorage.setItem("agentData", "");
        } // if (localStorage.getItem("agentData") == null)
        localStorage.setItem("screen", "components/login/view.html");
    }, // onShow: function() 
    afterShow: function() {} // afterShow: function
}); // app.login = kendo.observable

// START_CUSTOM_CODE_login
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes

(function(parent) {
    var changeScreen;
    var loginModel = kendo.observable({

        fields: {
            // usercode: 'ROYALS-1754-BAH094',
            // password: 'BAH094'
            usercode: 'kennethc',
            password: 'kennethc'
        }, // fields

        submit: function() {
            console.log("hello there!");
            var restParams1 = {
                service: "mnt",
                parameters: {
                    rqAuthentication: "user:" + loginModel.fields.usercode + "|" + loginModel.fields.password + "|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                    rqService: "ProducerService:fetchAgentData",
                    rqDataMode: "VAR/JSON",
                    ipcAgentCode: loginModel.fields.usercode,
                    ipdtDate: "2016-08-01"
                }, // parameters

                requestSuccess: function(data) {
                    console.log(data);
                    if (JSON.stringify(data.rqResponse.rqFullError) !== undefined) {
                        console.log(JSON.stringify(data.rqResponse.rqFullError));
                    } // if (data.rqFullError !== "" || 
                    else {

                        if (localStorage.getItem("agentData") === null || localStorage.getItem("agentData") === undefined) {
                            localStorage.setItem("agentData", "")
                        } // if (localStorage.getItem(agentData) === null || )

                        if (localStorage.getItem("nextScreen") === null || localStorage.getItem("agentData") === undefined) {
                            localStorage.setItem("nextScreen", "");
                        } // if (localStorage.getItem("nextScreen"))

                        if (localStorage.getItem("nextScreen") !== "") {
                            changeScreen = localStorage.getItem("nextScreen");
                        } // if (localStorage.getItem("nextScreen") != "")
                        else {
                            changeScreen = "components/home/view.html";
                        } // else 
                        localStorage.setItem("agentData", JSON.stringify(data));
                        localStorage.setItem("nextScreen", "");
                        localStorage.setItem("hasLoggedIn", true);
                        window.app.mobileApp.navigate(changeScreen, "slide");
                    } // else
                }, // requestSuccess

                requestError: function(d, e, c) {
                    localStorage.setItem("mnt:sessionid", "");
                    localStorage.setItem("agentData", "")

                    mipAlert.show(e, {
                        title: c
                    }); // mipAlert.show()
                }, // requestError

                spinner: {
                    message: "Authenticating",
                    title: "",
                    isFixed: true
                }, // spinner

                availableOfflineFor: {
                    minutes: 120
                } // availableOfflineFor

            }; // restParams1

            mipRest.request(restParams1);

        }, // submit: function ()
        cancel: function() {
            if (localStorage.hasLoggedIn) {
                window.app.mobileApp.navigate("components/home/view.html", "slide");
            } // if (localStorage.hasLoggedIn)
            else {
                mipAlert.show("Please log in.");
            } // els e
        }, // cancel: function ()
    });
    parent.set('loginModel', loginModel);
})(app.login);

// END_CUSTOM_CODE_login