'use strict';
var childCounter,
    parentCounter,
    extendedCounter;

app.quote = kendo.observable({
    onShow: function() {
		localStorage.setItem("screen", "components/quote/view.html");
        console.log("reset form");

        localStorage.setItem("defaultJSON", JSON.stringify(getJSONFile("data/JSON/quoteJSON.json")));

        childCounter = 1;
        parentCounter = 1;
        extendedCounter = 1;

        $("#quoteModel").trigger("reset");

        // empty fields

        $("#select-main-cover").val("");
        $("#select-main-dob").val("");
        $("#select-partner-dob").val("");
        $("#select-partner-cover").val("");
        $("#select-child-dob").val("");
        $("#select-child-cover").val("");
        $("#select-parent-dob").val("");
        $("#select-parent-cover").val("");
        $("#select-extended-dob").val("");
        $("#select-extended-cover").val("");
        $("#select-extended-relation").val("");

        $("#child-list").empty();
        $("#parent-list").empty();
        $("#extended-list").empty();

        localStorage.setItem("partyID", 1);

        if (localStorage.getItem("appData") === null) {
            localStorage.setItem("appData", "")
        } // if (localStorage.getItem("appDate") == null)
    }, // onShow: function ()
    afterShow: function() {}, // afterShow: function ()
    onShowAccept: function() {
        // try setting this value on the function when the 
        this.model.quoteModel.set("quoteDataJSON", JSON.parse(localStorage.getItem("quoteDataJSON")));
        if (typeof JSON.parse(localStorage.getItem("quoteDataJSON")) === "object") {
            rotateForwards();
            console.log("starting rotateBackwards");
            var contractDataSource = JSON.parse(localStorage.getItem("quoteDataJSON")).rqResponse.contractQuotationResponse.contract[0].coveredBenefit;
            for (var i = 0; i < contractDataSource.length; i++) {
                contractDataSource[i].premium = contractDataSource[i].premium.formatMoney();
                contractDataSource[i].sumAssured = contractDataSource[i].sumAssured.formatMoney();

                if (contractDataSource[i].coveredBenefitCode === "0104" || contractDataSource[i].coveredBenefitCode === "0016") {
                    contractDataSource[i].premium = "Free";
                } else {
                    contractDataSource[i].premium = "R " + contractDataSource[i].premium;
                }

            }
            // var sessId = localStorage.getItem("mnt:sessionid");
            console.log('contractDataSource:');
            console.log(contractDataSource);
            $("#currentCoveredBenefits").empty();

            if (contractDataSource === undefined) {
                $("#tHeaderCoveredBenefits").hide();
                $("#currentCoveredBenefits").prepend('<li class="ulistStyleProvider bold" style="border: none"><center><span>No Covered Benefits To Show</span></center><hr align="center" width="95%" color="#64a3d6"></li>');
            } // if (contractDataSource === undefined) 
            else {
                $("#tHeader").show();
                var template = kendo.template($("#templateCoveredBenefits").html());
                var dataSource = new kendo.data.DataSource({
                    data: contractDataSource,
                    change: function() { // subscribe to the CHANGE event of the data source
                            $("#currentCoveredBenefits").html(kendo.render(template, this.view())); // populate the table
                            if (window.cordova && window.navigator.simulator !== true) {
                                window.screen.unlockOrientation();
                            } // if (window.cordova && window.navigator.simulator
                        } // changeL function() 
                }); // var dataSource = new kendo.template($("#templateCoveredBenefits").html())

                // read data from the "contract" array

                dataSource.read();

            } // else
        } // if (typeof JSON.parse(localStorage.getItem("quoteDataJSON")) == "object")
        else {
            console.log("no object returned from request");
            console.log(JSON.parse(localStorage.getItem("quoteDataJSON")));
            $('#currentRolePlayers').empty();
            $("#currentRolePlayers").prepend('<li class="ulistStyleProvider bold" style="border: none"><center><span>No Role Players To Show</span></center><hr align="center" width="95%" color="#64a3d6"></li>');
        }


    }, // onShowQuote: function ()
    afterShowAccept: function() {} // afterShow: function ()
}); // app.quote = kendo.observable

// START_CUSTOM_CODE_quote 
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes

(function(parent) {

    var quoteModel = kendo.observable({
        fields: {
            mainCoverAmount: "",
            mainDOB: "",
            partnerCoverAmount: "",
            partnerDOB: "",
            childCoverAmount: "",
            childDOB: "",
            childCounter: 0,
            parentDOB: "",
            parentCoverAmount: "",
            parentCounter: 0,
            extendedCoverAmount: "",
            extendedDOB: "",
            extendedRelation: "",
            extendedCounter: 0
        }, // fields: 
        addChild: function() {
            console.log("add child function() ");
            console.log(app.quote.quoteModel.fields);

            if ($("#select-child-dob").val() === "" || $("#select-child-cover").val() === "") {
                mipAlert.show("Please enter a date of birth and a cover amount if you wish to add another child.");
            } // if (app.quote.quoteModel.fields.childDOB === "" ||
            else {
                if (childCounter < 6) {
                    // allow user to add more children
                    var defaultJSON = JSON.parse(localStorage.getItem("defaultJSON"));

                    var childCoverAmount;
                    if ($('#select-partner-cover').val() == "") {
                        childCoverAmount = $('#select-main-cover').val();
                    } else {
                        childCoverAmount = $('#select-partner-cover').val();
                    }

                    defaultJSON.contractQuotation.contractProduct[0].coveredParty.push({
                        coveredPartyID: parseInt(localStorage.getItem("partyID")) + 1,
                        contractProductID: 1,
                        dateOfBirth: $('#select-child-dob').val(),
                        relationship: 'child',
                        isMainInsured: 'false',
                        coverBenefitCode: '1110',
                        coverAmount: childCoverAmount
                    }); // default

                    console.log("defaultJSON: add child");
                    console.log(defaultJSON);

                    localStorage.setItem('defaultJSON', JSON.stringify(defaultJSON));
                    localStorage.setItem("partyID", parseInt(localStorage.getItem("partyID")) + 1);

                    var ivalue = defaultJSON.contractQuotation.contractProduct[0].coveredParty.length - 1,
                        i = parseInt(defaultJSON.contractQuotation.contractProduct[0].coveredParty[ivalue].coveredPartyID);

                    console.log("index : " + i);

                    $("#child-list").append("<tr id='Child-" + i + "'><td>" + defaultJSON.contractQuotation.contractProduct[0].coveredParty[ivalue].dateOfBirth + "<td>Child - " + childCounter + "</td></td><td class=''><a data-role='button' onclick='app.quote.quoteModel.removeChild(" + i + ");'><img src='data/images/icon/remove.png' width='20' height='20'> </a></td></tr>");

                    $('#select-child-dob').val('');
                    $('#select-child-cover').val('');

                    childCounter++;

                } // if (app.quote.quoteModel.fields.childCounter)
                else {
                    // prompt user when exceeding 5children and allow to add more
                    // NB Add confirm dialog
                    mipAlert("For this quote only a maximum of 5 children can be added", {
                        title: "Information"
                    });
                } // else

                app.quote.quoteModel.fields.childCounter = app.quote.quoteModel.fields.childCounter + 1;

            } // else

        }, // addChild: function()
        removeChild: function(id) {
            console.log("made it!!!");

            var defaultJSON = JSON.parse(localStorage.getItem("defaultJSON"));

            console.log(defaultJSON);
            console.log(id);
            // defaultJSON.contractQuotation.contractProduct[0].coveredParty.remove

            for (var i = 0; i < defaultJSON.contractQuotation.contractProduct[0].coveredParty.length; i++) {

                console.log("in removeChild for loop");

                if (defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].relationship === "child" && parseInt(defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].coveredPartyID) === parseInt(id)) {

                    console.log("deleting...");
                    $("#Child-" + id).remove();

                    splice(i, 1);

                    console.log(defaultJSON);

                    localStorage.setItem('defaultJSON', JSON.stringify(defaultJSON));

                    childCounter--;
                }
            }
        },
        addParent: function() {
            console.log("add parent function ()");

            if ($("#select-parent-dob").val() === "" || $("#select-parent-cover").val() === "") {
                mipAlert.show("Please enter a date of birth and a cover amount if you wish to add another parent");
            } // if (app.quote.quoteModel.fields.parentDOB === "" ||
            else {
                if (parentCounter < 5) {
                    // allow user to add parent(max:4)

                    var defaultJSON = JSON.parse(localStorage.getItem("defaultJSON"));

                    defaultJSON.contractQuotation.contractProduct[0].coveredParty.push({
                        coveredPartyID: parseInt(localStorage.getItem("partyID")) + 1,
                        contractProductID: 1,
                        dateOfBirth: $('#select-parent-dob').val(),
                        relationship: 'parent',
                        isMainInsured: 'false',
                        coverBenefitCode: "1130",
                        coverAmount: $('#select-parent-cover').val()
                    }); // default

                    console.log("defaultJSON: add parent");
                    console.log(defaultJSON);

                    localStorage.setItem('defaultJSON', JSON.stringify(defaultJSON));
                    localStorage.setItem("partyID", parseInt(localStorage.getItem("partyID")) + 1);

                    var ivalue = defaultJSON.contractQuotation.contractProduct[0].coveredParty.length - 1,
                        i = parseInt(defaultJSON.contractQuotation.contractProduct[0].coveredParty[ivalue].coveredPartyID);

                    console.log("index : " + i);

                    $("#parent-list").append("<tr id='Parent-" + i + "'><td>" + defaultJSON.contractQuotation.contractProduct[0].coveredParty[ivalue].dateOfBirth + "<td>Parent - " + parentCounter + "</td></td><td class=''><a data-role='button' onclick='app.quote.quoteModel.removeParent(" + i + ");'><img src='data/images/icon/remove.png' width='20' height='20'> </a></td></tr>");

                    parentCounter++;

                    $("#select-parent-dob").val("");
                    $("#select-parent-cover").val("");

                } // if (app.quote.quoteModel.fields.parentCounter)
                else {
                    // prevent user from adding more than 4 parents
                    // NB Add confirm dialog
                    mipAlert("For this quote only a maximum of 4 parents can be added", {
                        title: "Information"
                    });
                } // else

                app.quote.quoteModel.fields.parentCounter = app.quote.quoteModel.fields.parentCounter + 1;

            } // else
        }, // addParent: function () 

        removeParent: function(id) {
            console.log("made it!!!");

            var defaultJSON = JSON.parse(localStorage.getItem("defaultJSON"));

            console.log(defaultJSON);
            console.log(id);
            // defaultJSON.contractQuotation.contractProduct[0].coveredParty.remove
            for (var i = 0; i < defaultJSON.contractQuotation.contractProduct[0].coveredParty.length; i++) {
                console.log("in removeParent for loop");
                console.log(defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].relationship);
                console.log(parseInt(defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].coveredPartyID));

                if (defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].relationship === "parent" && parseInt(defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].coveredPartyID) === parseInt(id)) {

                    console.log("deleting...");

                    $("#Parent-" + id).remove();

                    splice(i, 1);

                    parentCounter--;

                    console.log(defaultJSON);
                } // id (defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].relationship
            } //for (var i = 0; defaultJSON.contractQuotation.contractProduct[0].coveredParty.length
        }, // removeParent: function () 

        addExtendedMember: function() {
            console.log("add extended family member function() ");
            console.log(app.quote.quoteModel.fields);
            if ($("#select-extended-dob").val() === "" || $("#select-extended-cover").val() === "" || $("#select-extended-relation").val() === "") {
                mipAlert.show("Please enter a date of birth and a cover amount if you wish to add another extended family member", { title: "Error" });

            } // if (app.quote.quoteModel.fields.extendedDOB === "" 
            else {
                if (extendedCounter < 7) {
                    // add extended family member

                    var extRelation;

                    console.log($('#select-extended-relation').val());

                    if ($('#select-extended-relation').val().toLowerCase() == "aunt" || $('#select-extended-relation').val().toLowerCase() == "uncle") {
                        extRelation = "Aunt/Uncle";
                    } else if ($('#select-extended-relation').val().toLowerCase() == "brother" || $('#select-extended-relation').val().toLowerCase() == "sister") {
                        extRelation = "Sister/Brother";
                    } else if ($('#select-extended-relation').val().toLowerCase() == "niece" || $('#select-extended-relation').val().toLowerCase() == "nephew") {
                        extRelation = "Niece/Nephew";
                    } else {
                        extRelation = $('#select-extended-relation').val();
                    }

                    console.log("defaultJSON: add Extended member");

                    var defaultJSON = JSON.parse(localStorage.getItem("defaultJSON"));

                    defaultJSON.contractQuotation.contractProduct[0].coveredParty.push({
                        coveredPartyID: parseInt(localStorage.getItem("partyID")) + 1,
                        contractProductID: 1,
                        dateOfBirth: $('#select-extended-dob').val(),
                        relationship: extRelation,
                        isMainInsured: 'false',
                        coverBenefitCode: "1140",
                        coverAmount: $('#select-extended-cover').val()
                    }); // default

                    console.log("defaultJSON: Extmember add");
                    console.log(defaultJSON);

                    localStorage.setItem('defaultJSON', JSON.stringify(defaultJSON));
                    localStorage.setItem("partyID", parseInt(localStorage.getItem("partyID")) + 1);

                    var ivalue = defaultJSON.contractQuotation.contractProduct[0].coveredParty.length - 1,
                        i = parseInt(defaultJSON.contractQuotation.contractProduct[0].coveredParty[ivalue].coveredPartyID);

                    $("#extended-list").append("<tr id='Extended-member-" + i + "'><td>" + defaultJSON.contractQuotation.contractProduct[0].coveredParty[ivalue].dateOfBirth + "<td>Extended member - " + extendedCounter + "</td></td><td class=''><a data-role='button' onclick='app.quote.quoteModel.removeExtendedMember(" + i + ");'><img src='data/images/icon/remove.png' width='20' height='20'> </a></td></tr>");

                    extendedCounter++;

                    $("#select-extended-dob").val("");
                    $("#select-extended-cover").val("");
                    $("#select-extended-relation").val("");

                } // if (app.quote.quoteModel.fields.extendedCounter)
                else {
                    // 
                    // NB Add confirm dialog 
                    mipAlert.show("You can not add more than 6 extended family members on this quote.", {
                        title: 'Information'
                    });

                    $("#select-extended-dob").val("");
                    $("#select-extended-cover").val("");
                    $("#select-extended-relation").val("");
                } // else

                app.quote.quoteModel.fields.extendedCounter = app.quote.quoteModel.fields.extendedCounter + 1;

            } // else 

        }, // addExtendedMember

        removeExtendedMember: function(id) {
            console.log("made it!!!");

            var defaultJSON = JSON.parse(localStorage.getItem("defaultJSON"));

            console.log(defaultJSON);
            console.log(id);

            // defaultJSON.contractQuotation.contractProduct[0].coveredParty.remove

            for (var i = 0; i < defaultJSON.contractQuotation.contractProduct[0].coveredParty.length; i++) {
                console.log("in removeExtendedMember for loop");
                if (defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].relationship === "Aunt/Uncle" || defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].relationship === "Niece/Nephew" || defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].relationship === "Adult Child" || defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].relationship === "Sister/Brother" || defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].relationship === "Additional Partner" && parseInt(defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].coveredPartyID) === parseInt(id)) {
                    console.log("deleting...");

                    $("#Extended-" + id).remove();

                    splice(i, 1);

                    extendedCounter--;

                    console.log(defaultJSON);
                } // id (defaultJSON.contractQuotation.contractProduct[0].coveredParty[i].relationship
            } //for (var i = 0; defaultJSON.contractQuotation.contractProduct[0].coveredParty.length
        }, // removeExtendedMember: function () 

        getQuote: function() {
            var defaultJSON = JSON.parse(localStorage.getItem('defaultJSON'));


            console.log($('#select-main-dob').val());

            if ($("#select-main-cover").val() === "" || $("#select-main-dob").val() === "") {
                mipAlert.show("Please select cover amount and date of birth for the main member.");
            } // if ($("#select-main-cover").val() === "" || )
            else {

                // assign main members details to the json object
                defaultJSON.contractQuotation.contractProduct[0].coveredParty[0].contractProductID = 1;
                defaultJSON.contractQuotation.contractProduct[0].coveredParty[0].coverAmount = $('#select-main-cover').val();
                defaultJSON.contractQuotation.contractProduct[0].coveredParty[0].coverBenefitCode = "1100";
                defaultJSON.contractQuotation.contractProduct[0].coveredParty[0].coveredPartyID = 1;
                defaultJSON.contractQuotation.contractProduct[0].coveredParty[0].dateOfBirth = $('#select-main-dob').val();
                defaultJSON.contractQuotation.contractProduct[0].coveredParty[0].isMainInsured = "true";
                defaultJSON.contractQuotation.contractProduct[0].coveredParty[0].relationship = "Self";

                localStorage.setItem("defaultJSON", JSON.stringify(defaultJSON));

                // add partner 
                console.log($("#select-partner-cover").val());

                defaultJSON = JSON.parse(localStorage.getItem("defaultJSON"));

                if ($("#select-partner-cover").val() === "" || $("#select-partner-dob").val() === "") {
                    console.log("No partner specified.");
                } // if ($("#select-partner-cover").val() === "")
                else {
                    console.log('Adding a partner!!!!!!!!!!!!!!!!!!!!!!');
                    defaultJSON.contractQuotation.contractProduct[0].coveredParty.push({
                        coveredPartyID: parseInt(localStorage.getItem("partyID")) + 1,
                        contractProductID: 1,
                        dateOfBirth: $('#select-partner-dob').val(),
                        relationship: 'spouse',
                        isMainInsured: 'false',
                        coverBenefitCode: "1110",
                        coverAmount: $('#select-partner-cover').val()
                    });
                    localStorage.setItem("defaultJSON", JSON.stringify(defaultJSON));
                    localStorage.setItem("partyID", parseInt(localStorage.getItem("partyID")) + 1);
                } // else

                // add child 
                if ($("#select-child-cover").val() === "" || $("#select-child-dob").val() === "") {
                    console.log("No child specified.");
                } // if ($("#select-child-cover").val() === "")
                else {
                    app.quote.quoteModel.addChild();
                } // else

                // add parent 
                if ($("#select-parent-cover").val() === "" || $("#select-parent-dob").val() === "") {
                    console.log("No parent specified.");
                } // if ($("#select-parent-cover").val() === "")
                else {
                    app.quote.quoteModel.addParent();
                } // else

                // add extended family member 
                if ($("#select-extended-cover").val() === "" || $("#select-extended-dob").val() === "" || $("#select-extended-relation")) {
                    console.log("No extended family member specified.");
                } // if ($("#select-extended-cover").val() === "")
                else {
                    app.quote.quoteModel.addExtendedMember();
                } // else

                console.log(JSON.parse(localStorage.getItem("defaultJSON")));
                console.log(localStorage.getItem("defaultJSON"));

                var contractQuoteobject = localStorage.getItem("defaultJSON");

                console.log(contractQuoteobject);

                var restParams1 = {
                    service: "mnt",
                    parameters: {
                        rqAuthentication: "user:hollardservice|mip123" + '|GSMUS|&login_company_branch_obj=91211.878*102594.188',
                        rqService: "ContractService:getContractPremiumQuote",
                        rqDataMode: "VAR/JSON",
                        dsContractQuotation: contractQuoteobject
                    }, // parameters

                    requestSuccess: function(data) {
                        console.log(data);
                        console.log(typeof data.rqResponse.contractQuotationResponse.contract);
                        if (data.rqResponse.rqErrorMessage == undefined) {
                            console.log('1');
                            if (typeof data.rqResponse.contractQuotationResponse.contract == 'object') {
                                localStorage.setItem("quoteDataJSON", JSON.stringify(data));
                                // localStorage.setItem("defaultJSON", localStorage.getItem("stockJSON"));
                                window.app.mobileApp.navigate("components/quote/view_accept_quote.html", "slide");

                            } // if (typeof data.rqResponse.contractQuotationResponse.contract == 'Object')

                        } else {
                            console.log("hello");
                            mipAlert("Error the request failed,  please review form and try again");
                        } // else 


                    }, // requestSuccess

                    requestError: function(d, e, c) {
                        localStorage.setItem("mnt:sessionid", "");
                        localStorage.setItem("appData", "")
                        mipAlert.show(e, {
                            title: c
                        });
                    }, // requestError

                    spinner: {
                        message: "Authenticating",
                        title: "",
                        isFixed: true
                    }

                }; // restParams1
                /*
                                mipAlert.show("Oops! The server seems to be down and cannot handle any request at the moment, please try again later.", {
                                    title: "Hollard Server"
                                }); */
                console.log(restParams1)
                mipRest.request(restParams1);

            } // else

        },
        acceptQuote: function() {
            fromQuote = true;
            dontRun = false;

            window.app.mobileApp.navigate("components/covisionApplication/step_1.html", "slide");
        },
        declineQuote: function() {
            app.quote.quoteModel.fields.mainCoverAmount = "";
            app.quote.quoteModel.fields.mainDOB = "";
            app.quote.quoteModel.fields.partnerCoverAmount = "";
            app.quote.quoteModel.fields.partnerDOB = "";
            app.quote.quoteModel.fields.childCoverAmount = "";
            app.quote.quoteModel.fields.childDOB = "";
            app.quote.quoteModel.fields.childCounter = 1;
            app.quote.quoteModel.fields.parentDOB = "";
            app.quote.quoteModel.fields.parentCoverAmount = "";
            app.quote.quoteModel.fields.parentCounter = 1;
            app.quote.quoteModel.fields.extendedCoverAmount = "";
            app.quote.quoteModel.fields.extendedDOB = "";
            app.quote.quoteModel.fields.extendedRelation = "";
            app.quote.quoteModel.fields.extendedCounter = 1;
        },
        submit: function() {}, // submit: function ()
        cancel: function() {}, // cancel: function ()
        /* **************** */
    }); // var quoteModel = kendo.observable

    parent.set('quoteModel', quoteModel);
})(app.quote);

var collapsePartner = function() {
    console.log("partner fields Cleared");
    // Clear all fields
    app.quote.quoteModel.fields.partnerDOB = "";
    app.quote.quoteModel.fields.partnerCoverAmount = "";
}
var collapseChild = function() {
    console.log("child fields Cleared");
    // Clear all fields
    $("#select-child-dob").val("");
    $("#select-child-cover").val("");

    app.quote.quoteModel.fields.childDOB = "";
    app.quote.quoteModel.fields.childCoverAmount = "";
}
var collapseParent = function() {
    console.log("parent fields Cleared");
    // Clear all fields
    $("#select-parent-dob").val("");
    $("#select-parent-cover").val("");

    app.quote.quoteModel.fields.parentDOB = "";
    app.quote.quoteModel.fields.parentCoverAmount = "";

}
var collapseExtended = function() {
    console.log("extended collapsed");
    // Clear all fields
    $("#select-extended-dob").val("");
    $("#select-extended-cover").val("");
    $("#select-extended-relation").val("");

    app.quote.quoteModel.fields.extendedDOB = "";
    app.quote.quoteModel.fields.extendedCoverAmount = "";
    app.quote.quoteModel.fields.extendedRelation = "";
}

function onConfirmParent(buttonIndex) {
    console.log("buttonIndex - " + buttonIndex);
    if (buttonIndex === 1) {} // if (buttonIndex == 1)
} // function onConfirm(buttonIndex)
// END_CUSTOM_CODE_quote