'use strict';

app.contactUs = kendo.observable({
    onShow: function() {
        if (localStorage.getItem("appData") === null) {
            localStorage.setItem("appData", "")
        }
    },

    afterShow: function() {},

    onShowBranchLocations: function() {
        console.log("hello");

        var json = getJSONFile("data/JSON/covision_branches_ JSON.json")

        console.log(json);

        var dataSource = new kendo.data.DataSource({
            data: json,
            sort: {
                field: "branchName",
                dir: "asc"
            },
        }); // var dataSource = new kendo.data.dataSource

        console.log("dataSource");
        console.log(dataSource);

        $("#filterable-listview-branches").kendoMobileListView({
                dataSource: dataSource,
                template: $("#mobile-listview-filtering-template-branches").text(),
                filterable: {
                    field: "branchName",
                    operator: "startswith"
                }, // filterable
                // endlessScroll: true
            })
            .kendoTouch({
                filter: "a",
                // enableSwipe: true,
                // touchstart: touchstart,
                click: app.contactUs.contactUsModel.shareBranches,
            });; //  $("#filterable-listview").kendoMobileListView
        console.log("after listview");
    }, // onShowBranchLocations: function

    afterShowBranchLocations: function() {

    }, // afterShowBranchLocations: function

    onShowEasyPayVaal: function() {
        var json = getJSONFile("data/JSON/easypay_outlet_vaal.json");

        var dataSource = new kendo.data.DataSource({
            data: json,
            sort: {
                field: "Town",
                dir: "desc"
            },
            pageSize: 50
        });

        $("#filterable-listview-easypay-vaal").kendoMobileListView({
            dataSource: dataSource,
            template: $("#mobile-listview-filtering-template-easypay-vaal").text(),
            filterable: {
                field: "Town",
                operator: "startswith"
            },
            // endlessScroll: true
        });

    }, // onShowEasyPayOutlets: function

    afterShowEasyPayVaal: function() {

    }, // afterShowEasyPayOutlets: function

    onShowEasyPayFreestate: function() {
        var json = getJSONFile("data/JSON/easypay_outlet_freestate.json");

        var dataSource = new kendo.data.DataSource({
            data: json,
            sort: {
                field: "Town",
                dir: "desc"
            },
            pageSize: 50
        });

        $("#filterable-listview").kendoMobileListView({
            dataSource: dataSource,
            template: $("#mobile-listview-filtering-template").text(),
            filterable: {
                field: "Town",
                operator: "startswith"
            },
            // endlessScroll: true
        });

    }, // onShowEasyPayOutlets: function

    afterShowEasyPayFreestate: function() {

    }, // afterShowEasyPayOutlets: function
}); // app.contactUs = kendo.observable


// START_CUSTOM_CODE_contactUs
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
(function(parent) {
    var contactUsModel = kendo.observable({
        fields: { test: "Working" },
        viewBranches: function() {
            console.log("hello2");

            window.app.mobileApp.navigate("components/contactUs/view-branches.html", "slide");
            // window.app.mobileApp.navigate("components/contactUs/view-branches.html", "slide");

        },

        viewEasyPayVaal: function() {
            window.app.mobileApp.navigate("components/contactUs/view-easypay_vaal.html", "slide");
        },

        viewEasyPayFreestate: function() {
            window.app.mobileApp.navigate("components/contactUs/view-easypay_freestate.html", "slide");
        },

        shareOutletsVaal: function(e) {
            console.log("hello");
            var messageString = "outlet details here!";

            var options = {
                message: messageString, // not supported on some apps (Facebook, Instagram)
                subject: 'EasyPay Outlet', // fi. for email
                chooserTitle: 'Pick an app' // Android only, you can override the default share sheet title
            }

            var onSuccess = function(result) {
                console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
                console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
            }

            var onError = function(msg) {
                console.log("Sharing failed with message: " + msg);
            }
            window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
        },

        shareOutletsFreestate: function() {
            console.log("hello");
            var messageString = "outlet details here!";

            var options = {
                message: messageString, // not supported on some apps (Facebook, Instagram)
                subject: 'EasyPay Outlet', // fi. for email
                chooserTitle: 'Pick an app' // Android only, you can override the default share sheet title
            }

            var onSuccess = function(result) {
                console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
                console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
            }

            var onError = function(msg) {
                console.log("Sharing failed with message: " + msg);
            }
            window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
        },

        shareBranches: function(e) {
            console.log(e);

            console.log("hello");

            var messageString = "branch details here!";

            var options = {
                message: messageString, // not supported on some apps (Facebook, Instagram)
                subject: 'Branch Details', // fi. for email
                // chooserTitle: 'Pick an app' // Android only, you can override the default share sheet title
            };

            var onSuccess = function(result) {
                console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
                console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
            };

            var onError = function(msg) {
                console.log("Sharing failed with message: " + msg);
            };

            window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
        }
    }); // var platinumMembershipModel = kendi.observable 
    parent.set('contactUsModel', contactUsModel);
})(app.contactUs);

// END_CUSTOM_CODE_contactUs