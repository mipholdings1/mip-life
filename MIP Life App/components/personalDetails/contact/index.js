'use strict';

app.contact = kendo.observable({
    onShow: function() {
        if (localStorage.getItem("appData") === null) {
            localStorage.getItem("appData") === "";
        }
        this.model.contactModel.set("personData", JSON.parse(localStorage.getItem("appData")).personalDetails[0]);
        this.model.contactModel.set("memberData", JSON.parse(localStorage.getItem("appData")).membershipDetails[0]);
    },
    afterShow: function() {}
}); // app.policies = kendo.observable 

// START_CUSTOM_CODE_contact
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
(function(parent) {
    var contactModel = kendo.observable({
        fields: {}, // fields
        cancel: function() {
            window.app.mobileApp.navigate("components/personalDetails/view.html", "slide");
        }, // cancel: function ()

        confirm: function() {
            var form = document.getElementById("updateContactDetailsForm"),
                message = "Oops! There is invalid data in the form.",
                options = {
                    title: 'Invalid Fields',
                    buttonText: 'OK',
                    callback: function() {}
                } // var options
            if (validator.validate()) {
                console.log("validation successfull");
                var restParams1 = {
                    service: "mnt",
                    parameters: {
                        rqAuthentication: "user:admin|mip123" + "|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                        rqService: "ContractService:updateContactInfo",
                        rqDataMode: "VAR/JSON",
                        ipcContractNumber: contactModel.memberData.memberNumber,
                        ipcTelNumber: contactModel.personData.contactDetails[0].contactDetail,
                        ipcCellNumber: contactModel.personData.contactDetails[2].contactDetail,
                        ipcEmail: contactModel.personData.contactDetails[1].contactDetail,
                    }, // parameters

                    requestSuccess: function(data) {
                        // this will download and view the PDF document
                        // after making sure the user has a valid session to 

                        if (data.rqResponse.rqErrorMessage) {
                            if (data.rqResponse.rqFullError.indexOf('mip_MsgSesErr:6') > 0) {
                                window.app.mobileApp.navigate("components/login/view.html", "slide");
                                localStorage.setItem("nextScreen", "components/personalDetails/contact/view.html");
                                //navigateTo = localStorage.getItem("loginView");
                            } // if (data.rqFullError.indexOf
                            else {
                                navigator.notification.alert(
                                    data.rqResponse.rqErrorMessage, // the message
                                    function() {}, // a callback
                                    "Error", // a title
                                    "OK" // the button text
                                ); // navigator.notification.alert 
                            } // else 
                        } //  if (data.rqResponse.rqFullError == "" || data.rqResponse.rqFullError == null)
                        else {
                            var restParams2 = {
                                service: "mnt",
                                parameters: {
                                    rqAuthentication: "user:admin|mip123" + "|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                                    rqService: "ContractService:fetchMembershipInfo",
                                    rqDataMode: "VAR/JSON"
                                }, // parameters

                                requestSuccess: function(data) {
                                    console.log(data)
                                    localStorage.setItem("appData", JSON.stringify(data.rqResponse.membershipInfo));
                                    mipAlert.show("Your contact details have been successfully updated");
                                    window.app.mobileApp.navigate("components/personalDetails/view.html", "slide");
                                }, // requestSuccess

                                requestError: function(d, e, c) {
                                    mipAlert.show(e, {
                                        title: c
                                    });
                                }, // requestError

                                spinner: {
                                    message: "Refreshing information...",
                                    title: "Saving",
                                    isFixed: true
                                }, // spinner

                                availableOfflineFor: {
                                    minutes: 120
                                } // availableOfflineFor

                            }; // restParams2

                            mipRest.request(restParams2); // updates the dataset and local storage

                        } // else
                    }, // requestSuccess


                    requestError: function(d, e, c) {
                        mipAlert.show(e, {
                            title: c
                        });

                    }, // requestError

                    spinner: {
                        message: "Saving. Please wait...",
                        title: "Saving",
                        isFixed: true
                    }, // spinner

                    availableOfflineFor: {
                        minutes: 120
                    } // availableOfflineFor
                }; // restParams1

                mipRest.request(restParams1);
            } // if (validator.validate())
            else {
                navigator.notification.alert(
                        message, // the message
                        function() {}, // a callback
                        "Invalid Data", // a title
                        "OK" // the button text
                    ) // navigator.notification.alert
            } // else 
        }, // confirm: function ()
    }); // var contactModel = kendo.observable
    parent.set('contactModel', contactModel);
})(app.contact); // (function (parent) )
// END_CUSTOM_CODE_contactv
