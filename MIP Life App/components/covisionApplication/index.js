"use strict";

var coveredPeople = [];

app.covisionApplication = kendo.observable({
    onShow: function() {
        localStorage.setItem("screen", "components/covisionApplication/view.html");
        $("#residential-address").hide();
        $("residential-address-header").hide();
        if (app.mobileApp.view().id === "components/covisionApplication/step_7.html") {
            // $('#sign').signature();
            $("#debitorderform").hide();
        } // if (app.mobileApp.view().id === "components/covisionApplication/step_7.html")

        if (app.mobileApp.view().id === "components/covisionApplication/final.html") {
            this.model.covisionApplicationModel.set("contractDetails", JSON.parse(localStorage.getItem("newPolicyData")).rqResponse.dsContract.ttContract);
            $('#sign').signature();
        } // if (app.mobileApp.view().id === "components/covisionApplication/final.html")


    }, // onShow: function ()
    afterShow: function() {}, // afterSHow: function() 

    onShowPayment: function() {
        $("#payer-details").hide();
        $("#debitorderform").hide();
        var month,
            monthText,
            nextMonth,
            nextMonthText;

        localStorage.setItem("commencementMonth", "");

        var date = new Date(),
            month = date.getMonth() + 1,
            nextMonth = month + 1,
            day = date.getDate(),
            year = date.getFullYear(),
            monthText = "",
            nextMonthText = "";

        switch (month) {
            case 1:
                monthText = "January";
                break;
            case 2:
                monthText = "February";
                break;
            case 3:
                monthText = "March";
                break;
            case 4:
                monthText = "April";
                break;
            case 5:
                monthText = "May";
                break;
            case 6:
                monthText = "June";
                break;
            case 7:
                monthText = "July";
                break;
            case 8:
                monthText = "August";
                break;
            case 9:
                monthText = "September";
                break;
            case 10:
                monthText = "October";
                break;
            case 11:
                monthText = "November";
                break;
            case 12:
                monthText = "December";
                break;
        }

        switch (nextMonth) {
            case 1:
                nextMonthText = "Jan";
                break;
            case 2:
                nextMonthText = "Feb";
                break;
            case 3:
                nextMonthText = "Mar";
                break;
            case 4:
                nextMonthText = "Apr";
                break;
            case 5:
                nextMonthText = "May";
                break;
            case 6:
                nextMonthText = "Jun";
                break;
            case 7:
                nextMonthText = "Jul";
                break;
            case 8:
                nextMonthText = "Aug";
                break;
            case 9:
                nextMonthText = "Sept";
                break;
            case 10:
                nextMonthText = "Oct";
                break;
            case 11:
                nextMonthText = "Nov";
                break;
            case 12:
                nextMonthText = "Dec";
                break;
        }

        console.log(month + " " + monthText);
        console.log(nextMonth + " " + nextMonthText);

        $("#select-commencement-month").empty();
        $("#select-commencement-month").append('<option value="' + month + '">' + monthText + '</option>');
        $("#select-commencement-month").append('<option value="' + nextMonth + '">' + nextMonthText + '</option>');
    }, // onShowPayment: function()

    afterShowPayment: function() {}, // afterShowPayment()

    onShowMainMember: function() {
        console.log("from Quote " + fromQuote);
        if (fromQuote === true) {
            var data = JSON.parse(localStorage.getItem("defaultJSON")),
                idStart = data.contractQuotation.contractProduct[0].coveredParty[0].dateOfBirth.replace(/-/g, "");

            console.log(data);

            $("#main_benefit_amount").val(data.contractQuotation.contractProduct[0].coveredParty[0].coverAmount);
            $("#main_benefit_amount").val();
            $("#main-id-number").val(idStart.toString().substring(2));

            for (var i = 0; i < data.contractQuotation.contractProduct[0].coveredParty.length; i++) {
                if (data.contractQuotation.contractProduct[0].coveredParty[i].relationship.toLowerCase() === 'spouse') {
                    $("#select-main-marital").val('Married');
                    break;
                } // if (data.contractQuotation.contractProduct[0].coveredParty[i]
                else {
                    $("#select-main-marital").val('Single');
                } // else 
            } // for (var i = 0; i < data.contractQuotation.contractProduct[0].coveredParty.length; i++)
        } // if (fromQuote)
    }, // onShowMainMember 

    onShowDepList: function() {
        console.log('onShowDepList');

        var data = [],
            depId;
        $("#currentDependentList").empty();
        console.log("after empty()");

        if (localStorage.getItem("defaultJSON") === "") {
            fromQuote = false;
        } // if (localStorage.getItem("defaultJSON") === "") 
        else {
            data = JSON.parse(localStorage.getItem("defaultJSON"));
            fromQuote = true;
            console.log(data);
        } // else 

        if (fromQuote === true) {
            for (var i = 0; i < data.contractQuotation.contractProduct[0].coveredParty.length; i++) {
                if (data.contractQuotation.contractProduct[0].coveredParty[i].relationship.toLowerCase() !== 'self') {
                    if (dontRun === false) {
                        var newMember = new member(),
                            newMemberCoveredBen = new coveredBenefits(),
                            idStart = "";
                        // data.contractQuotation.contractProduct[0].coveredParty[i].dateOfBirth.replace(/-/g, "");

                        newMember.memberId = newMember.uid;
                        newMember.title = "";
                        newMember.firstNames = "";
                        newMember.lastName = "";
                        newMember.gender = "";
                        newMember.roleCode = "";
                        newMember.dateOfBirth = data.contractQuotation.contractProduct[0].coveredParty[i].dateOfBirth;
                        newMember.idNumber = "";
                        newMember.relationship = data.contractQuotation.contractProduct[0].coveredParty[i].relationship;
                        newMember.coveredBenefits = [];

                        if (data.contractQuotation.contractProduct[0].coveredParty[i].relationship.toLowerCase() === 'spouse') {
                            newMember.maritalStatus = "Married";
                        } // if (data.contractQuotation.contractProduct[0].
                        else {
                            newMember.maritalStatus = "Single";
                        } // else

                        newMemberCoveredBen.benefitAmount = data.contractQuotation.contractProduct[0].coveredParty[i].coverAmount;
                        newMemberCoveredBen.benefitCode = data.contractQuotation.contractProduct[0].coveredParty[i].coverBenefitCode;
                        newMember.coveredBenefits.push(newMemberCoveredBen);

                        coveredPeople.push(newMember);
                    } // if (dontRun === false)
                } // if (data.contractQuotation.contractProduct[0]....
            } //  for (var i = 0; i < data.contractQuotation.contractProdu...
        } // if (fromQuote)

        data = coveredPeople;

        if (typeof data === "object") {
            rotateForwards();

            console.log('onShowDepList: function () - if (typeof data === "object":');
            console.log(data);

            $("#currentDependentList").empty();

            if (data === undefined || data === null || data.length === 0) {
                $("#tHeaderDependentList").hide();
                $("#currentDependentList").prepend('<li class="ulistStyleProvider bold" style="border: none">\
                    <center>\
                        <span>No dependents to be listed.</span>\
                    </center>\
                    <hr align="center" width="95%" color="#64a3d6">\
                </li>');
            } // if (data === undefined || data === null || data.length == 0)
            else {
                console.log(':::::: editing existing dependent');
                for (var i = 0; i < data.length; i++) {
                    if (data[i].relationship.toLowerCase() !== "self") {
                        var depName = "";

                        depId = data[i].dateOfBirth;

                        if (data[i].firstNames.trim() === "" && data[i].lastName.trim() === "" || data[i].firstNames === undefined && data[i].lastName === undefined) {
                            depName = data[i].relationship;
                        } // if (data[i].firstNames.trim() === "" &&...
                        else {
                            depName = toTitleCase(data[i].firstNames) + " " + toTitleCase(data[i].lastName);
                        } // else 

                        $("#tHeaderDependentList").show();
                        $("#currentDependentList").append("<tr id='" + data[i].memberId + "'>\
                            <td>" + depName + "</td>\
                            <td>" + depId + "</td>\
                            <td>\
                                <a data-role='button' onclick='app.covisionApplication.covisionApplicationModel.removeDependent(" + i + ");'>\
                                    <img src='data/images/icon/remove.png' width='25' height='25'>\
                                </a>\
                            </td>\
                            <td>\
                                <a data-role='button' onclick='app.covisionApplication.covisionApplicationModel.editDependent(" + JSON.stringify(data[i]) + ");'>\
                                    <img src='data/images/icon/edit.png' width='30' height='30'>\
                                </a>\
                            </td>\
                        </tr>");
                    } // if (data[i].relationship !== "self")
                    else if (data[i].relationship.toLowerCase() == "self" && data.length == 1) {
                        $("#tHeaderDependentList").hide();
                        $("#currentDependentList").prepend('<li class="ulistStyleProvider bold" style="border: none">\
                            <center>\
                                <span>No dependents to be listed.</span>\
                            </center>\
                            <hr align="center" width="95%" color="#64a3d6">\
                        </li>');
                    } // else if (data[i].relationship.toLowerCase()...
                } // for (var i = 0; i < data.length; i++)
            } // else 
        } // if (typeof JSON.parse
        else {
            console.log("onShowDepList: no object returned from request");
            console.log(data);

            $('#currentDependentList').empty();
            $("#currentDependentList").prepend('<li class="ulistStyleProvider bold" style="border: none">\
                <center>\
                    <span>Currently no dependents to be listed</span>\
                </center>\
                <hr align="center" width="95%" color="#64a3d6">\
            </li>');
        } // else
    }, // onShowDepList

    afterShowDepList: function() {}, // afterShowDepList

    onShowEasyPay: function() {
        $("#easyPay-ref-num").empty();
    }, // onShowEasyPay: function ()

    afterShowEasyPay: function() {}, // afterShowEasyPay: function ()
    onShowAddDep: function() {
        console.log("brandNewDependent " + brandNewDependent);
        if (kendo.support.mobileOS.device === 'android') {
            $("#dep-dob").removeAttr("onblur");
            $("#dep-dob").attr("onchange", "getValidAges($('#dep-relation'), $('#dep-dob'));");
        } //  if (kendo.support.mobileOS.device === 'android')
        else {
            $("#dep-dob").removeAttr("onchange");
            $("#dep-dob").attr("onblur", "getValidAges($('#dep-relation'), $('#dep-dob'));");
        } // else 
        /* TODO: remove this when done */
        //brandNewDependent = false;
        if (brandNewDependent === false) {
            var depObj = JSON.parse(localStorage.getItem("depData"));

            /* TODO: remove this when done */
            // depObj = { "memberId": "45569cd5-4177-4692-8f07-d6b17cfa02b0", "title": "", "firstNames": "", "lastName": "", "gender": "", "roleCode": "", "dateOfBirth": "2009-06-18", "idNumber": "090618", "relationship": "child", "coveredBenefits": [{ "benefitAmount": "30000", "benefitCode": "1110" }], "maritalStatus": "Single" };

            console.log("::::::::: depObj.id  " + depObj.memberId);
            console.log(depObj);
            console.log(depObj.dateOfBirth);



            switch (depObj.relationship.toLowerCase()) {
                case 'aunt/uncle':
                    $("#dep-relation").val("aunt");
                    break;
                case 'niece/nephew':
                    $("#dep-relation").val("niece");
                    break;
                case 'sister/brother':
                    $("#dep-relation").val("sister");
                    break;
                default:
                    $("#dep-relation").val(depObj.relationship);
            } // switch (depObj.relationship.toLowerCase())

            $("#dep-id-number").val(depObj.idNumber);
            $("#dep-dob").val(depObj.dateOfBirth);
            $("#member-id").val(depObj.memberId);
            $("#dep-marital").val(depObj.maritalStatus);
            // $("#dep-relation").val(depObj.relationship);
            $("#dep-name").val(depObj.firstNames);
            $("#dep-surname").val(depObj.lastName);

            app.covisionApplication.covisionApplicationModel.cascadeCoverAmount();

            $("#dep_benefit_amount").val(depObj.coveredBenefits[0].benefitAmount);

            console.log("ben amount : " + $("#dep_benefit_amount").val());
            if (($("#dep-relation").val()).toLowerCase() == 'child') {
                $("#cover-amount-li").hide(500);
                console.log("ben amount : child " + $("#dep_benefit_amount").val());
            } // if (($("#dep-relation").val()).toString() == 'child')
            else {
                $("#cover-amount-li").show(500);
                console.log("ben amount : rest " + $("#dep_benefit_amount").val());
            } // else 
        } // if (brandNewDependent == false ) 
        else {
            $("#dep-dob").val("");
            $("#dep-id-number").val("");
            $("#member-id").val("");
            $("#dep_benefit_amount").val("");
            $("#dep-marital").val("");
            $("#dep-relation").val("");
        } // else 
    }, // onShowAddDep: function()
    afterShowAddDep: function() {
        if (brandNewDependent === false) {
            var depObj = JSON.parse(localStorage.getItem("depData"));
            /* todo: remove this when done */
            //depObj = { "memberId": "45569cd5-4177-4692-8f07-d6b17cfa02b0", "title": "", "firstNames": "", "lastName": "", "gender": "", "roleCode": "", "dateOfBirth": "2009-06-18", "idNumber": "090618", "relationship": "child", "coveredBenefits": [{ "benefitAmount": "30000", "benefitCode": "1110" }], "maritalStatus": "Single" };

            $("#dep-dob").val(depObj.dateOfBirth);
            console.log("new dob : " + $("#dep_dob").val());
        } // if (brandNewDependent === false) 


    }, // afterShowDep: function() 
}); //app.covisionApplication = kendo.observable

// START_CUSTOM_CODE_covisionApplication
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes

(function(parent) {
    var contractRequestData;
    var covisionApplicationModel = kendo.observable({
        fields: {
            dataDep: [],

            easyPayReference: "",
            depFirstNames: "",
            depLastName: "",
            depIdNumber: "",
            depMaritalStatus: "",
            depRelationship: "",
            depCoverAmount: "",
            depAge: "",
            famCoverAmount: "",

            /* MAIN INSURED PERSON DETAILS */
            title: "",
            firstNames: "",
            lastName: "",
            idNumber: "",
            maritalStatus: "",
            email: "",
            age: "18",
            birthDate: "",
            benefitAmount: "",
            premium: "",
            gendertc: "",
            gender: "",
            totalMonthlyIncome: "",
            cellNumber: "",
            telWork: "",
            telHome: "",
            cellOrPost: "",

            /* POSTAL DETAILS */
            postalStreet: "",
            postalSuburb: "",
            postalCity: "",
            postalCode: "",
            postalProvince: "",

            /* RESIDENTIAL DETAILS */
            residentialStreet: "",
            residentialSuburb: "",
            residentialCity: "",
            residentialCode: "",
            residentialProvince: "",


            /* BENEFICIARY DETAILS */
            beneficiaryTitle: "",
            beneficiaryName: "",
            beneficiarySurname: "",
            beneficiaryIDNumber: "",
            beneficiaryBirthDate: "",
            beneficiaryRelation: "",
            beneficiaryContactNo: "",
            beneficiaryGendertc: "",
            beneficiaryGender: "",

            /* PAYMENT DETAILS (How will you pay the total amount) */
            isPayer: true,
            payerName: "",
            payerSurname: "",
            payerMonthlyIncome: "",
            payerCell: "",
            payerTelHome: "",
            payerTelWork: "",
            paymentMethod: "",
            payerSignature: "",

            /* BANKING DETAILS (if paying by debit order) */
            accOwnerTitle: "",
            accOwnerName: "",
            accOwnerSurname: "",
            accOwnerIDNumber: "",
            accOwnerTelWork: "",
            accOwnerTelHome: "",
            accBankName: "",
            accBranchName: "",
            accNumber: "",
            accOwnerCell: "",
            accDebitDate: "",
            accBranchCode: "",
            accType: "",
            accOwnerSignature: "",
            accOwnerSignDate: new Date(),

            /* FINALISING */
            yourSignature: "",
            yourSignDate: new Date(),

            /* POLICY DETAILS */
            polNumber: "",
            planName: "",
            effectiveDate: "",
            paymenAmount: "",
            policyStatus: "",
            productCode: "",
        }, // fields: 

        uploadIDDoc: function() {
            console.log("upLoadIDoc: function ()");

            function uploadId(pressedButton) {
                console.log("function uploadId(pressedButton)");
                console.log(pressedButton);
                if (pressedButton === 1) {
                    mipSpinner.show();
                    navigator.camera.getPicture(onSuccess, onFail, {
                        quality: 50,
                        destinationType: Camera.DestinationType.DATA_URL,
                        sourceType: 1
                    }); // mipAlert.show()
                } // if (buttonPressed === 1)
            } // function uploadId(pressedButton)

            var onSuccess = function(DATA_URL) {
                mipSpinner.hide();
                var blob = new Blob([DATA_URL], {
                    type: 'text/txt'
                }); // var blob

                var contractObj = JSON.parse(localStorage.getItem("newPolicyData")).rqResponse.dsContract.ttContract[0].cContractIdentifier,
                    contractData = JSON.parse(localStorage.getItem("newPolicyData")).rqResponse.dsContract.ttContract[0],
                    processLogObj = JSON.parse(localStorage.getItem("workflowLog")).rqResponse.opdProcessLogObj,
                    mainMember;



                /* TODO: to be removed */
                mipAlert.show("Your ID Document has been uploaded successfully.", {
                    title: "Success"
                }); // mipAlert.show()
                /* ------------- */



                for (var i = 0; i < contractData.ttContractRole.length; i++) {
                    if (contractData.ttContractRole[i].cPlayerInRoleDesc === "Main Life Assured") {
                        mainMember = contractData.ttContractRole[i].cName;
                    } // if (contractData.ttContractRole[i].cPlayerInRoleDesc === "Main Life Assured") {
                } // for (var i = 0; i < contractData.ttContractRole.length)

                // var restParams1 = {
                //     service: "mnt",
                //     parameters: {
                //         rqAuthentication: 'user:hollardservice|mip123|GSMUS|&login_company_branch_obj=91211.878*102594.188',
                //         rqService: "pfUtilityService:linkMultimediaRest",
                //         rqDataMode: "VAR/JSON",
                //         ipdProcessLogObj: processLogObj,
                //         ipcRemNVP: 'CBMCT=' + contractObj,
                //         iplcFileObject: DATA_URL,
                //         ipcFileTemplate: 'client_id',
                //         ipcFileName: mainMember + '-id_document.jpg',
                //         ipcFileLabel: 'ID Document - ' + mainMember
                //     }, // parameters

                //     requestSuccess: function(data) {
                //         console.log(data);
                //         if (JSON.stringify(data.rqResponse.rqFullError) !== undefined || JSON.stringify(data.rqResponse.rqFullError) !== "") {
                //             console.log(JSON.stringify(data.rqResponse.rqFullError));
                //             mipAlert.show(data.rqResponse.rqErrorMessage);
                //         } // if (data.rqFullError !== "" || 
                //         else {
                //             /* SUCCESS LOGIC HERE */
                //             console.log("ID Uploaded Successfully");
                //             console.log(JSON.stringify(data));
                //             mipAlert.show("Your ID Document has been uploaded successfully.", {
                //                 title: "Success"
                //             }); // mipAlert.show()
                //         } // else
                //     }, // requestSuccess

                //     requestError: function(d, e, c) {
                //         localStorage.setItem("mnt:sessionid", "");
                //         localStorage.setItem("agentData", "")

                //         mipAlert.show(e, {
                //             title: c
                //         }); // mipAlert.show()
                //     }, // requestError

                //     spinner: {
                //         message: "Authenticating",
                //         title: "",
                //         isFixed: true
                //     }, // spinner

                //     availableOfflineFor: {
                //         minutes: 120
                //     } // availableOfflineFor

                // }; // restParams1
                // mipRest.request(restParams1);
            }; // var onSuccess = function (DATA_URL)

            var onFail = function(e) {

                mipSpinner.hide();

                mipAlert.show(e, {
                    title: 'Error'
                }); // mipAlert.show(e)
            }; // var onFail = function (e)
            navigator.notification.confirm(
                'Please take a picture of your ID book now.', // message
                uploadId, // callback to invoke with index of button pressed
                'ID Document', // title
                ['Ok'] // buttonLabels
            ); // navigator.notification.confirm
        }, // uploadIDDoc: function ()

        closeSignModal1: function() {

            var contractObj = JSON.parse(localStorage.getItem("newPolicyData")).rqResponse.dsContract.ttContract[0].cContractIdentifier;
            console.log(contractObj);
            //var processLogObj = JSON.parse(localStorage.getItem("workflowLog")).rqResponse.opdProcessLogObj;
            console.log(processLogObj);

            var contractData = JSON.parse(localStorage.getItem("newPolicyData")).rqResponse.dsContract.ttContract[0];
            var mainMember;
            for (var i = 0; i < contractData.ttContractRole.length; i++) {
                if (contractData.ttContractRole[i].cPlayerInRoleDesc === "Main Life Assured") {
                    mainMember = contractData.ttContractRole[i].cName;
                }
            } // for (var i = 0; i < contractData.ttContractRole.length)

            var imagedata = $('#sign').data("mipSignature").signaturePad.toDataURL();



            /* TODO: REMOVE THIS */
            $('#modalview-signature1').kendoMobileModalView('close');
            mipAlert.show("Successfully attached signature.", {
                title: "Success"
            }); // mipAlert.show()
            /* ----------------- */




            // var restParams1 = {
            //     service: "mnt",
            //     parameters: {
            //         rqAuthentication: 'user:hollardservice|mip123|GSMUS|&login_company_branch_obj=91211.878*102594.188',
            //         rqService: "pfUtilityService:linkMultimediaRest",
            //         rqDataMode: "VAR/JSON",
            //         ipdProcessLogObj: processLogObj,
            //         ipcRemNVP: 'CBMCT=' + contractObj,
            //         iplcFileObject: imagedata.split(',')[1],
            //         ipcFileTemplate: 'client_signature',
            //         ipcFileName: mainMember + '-signature.png',
            //         ipcFileLabel: 'Signature - ' + mainMember
            //     }, // parameters

            //     requestSuccess: function(data) {
            //         console.log(data);
            //         if (JSON.stringify(data.rqResponse.rqFullError) !== undefined) {
            //             console.log(JSON.stringify(data.rqResponse.rqFullError));
            //             mipAlert.show(data.rqResponse.rqErrorMessage);
            //         } // if (data.rqFullError !== "" || 
            //         else {
            //             /* SUCCESS LOGIC HERE */
            //             console.log("Signature Attached Successfully");
            //             console.log(data);
            //             $('#modalview-signature1').kendoMobileModalView('close');
            //             mipAlert.show("Successfully attached signature.", {
            //                 title: "Success"
            //             }); // mipAlert.show()
            //         } // else
            //     }, // requestSuccess

            //     requestError: function(d, e, c) {
            //         localStorage.setItem("mnt:sessionid", "");
            //         localStorage.setItem("agentData", "")

            //         mipAlert.show(e, {
            //             title: c
            //         }); // mipAlert.show()
            //     }, // requestError

            //     spinner: {
            //         message: "Authenticating",
            //         title: "",
            //         isFixed: true
            //     }, // spinner

            //     availableOfflineFor: {
            //         minutes: 120
            //     } // availableOfflineFor
            // }; // restParams1
            // mipRest.request(restParams1);
        }, // closeSignModal1: function () 

        addMainMember: function() {

            console.log("addMainMember: function ()");

            var message = "Please ensure that all mandatory fields are filled in",
                options = {
                    title: 'Invalid Fields',
                    buttonText: 'OK',
                    callback: function() {}
                }; // var options

            event.preventDefault();
            if (mainMemberFormValidate.validate()) {
                console.log("validation successfull");

                console.log("idNumber : " + covisionApplicationModel.fields.idNumber.toString());
                console.log(getBirthdate(covisionApplicationModel.fields.idNumber.toString()));

                var mainMember = new member();
                mainMember.memberId = mainMember.uid;

                mainMember.firstNames = covisionApplicationModel.fields.firstNames;
                mainMember.lastName = covisionApplicationModel.fields.lastName;
                mainMember.idNumber = covisionApplicationModel.fields.idNumber;
                mainMember.dateOfBirth = (getBirthdate(covisionApplicationModel.fields.idNumber.toString()));
                mainMember.gender = getGender(covisionApplicationModel.fields.idNumber.toString());
                mainMember.maritalStatus = covisionApplicationModel.fields.maritalStatus;
                mainMember.totalMonthlySalary = covisionApplicationModel.fields.totalMonthlySalary;
                mainMember.cellNumber = covisionApplicationModel.fields.cellNumber;
                mainMember.telWork = covisionApplicationModel.fields.telWork;
                mainMember.telHome = covisionApplicationModel.fields.telHome;
                mainMember.roleCode = "lifeA";
                mainMember.relationship = "self";
                mainMember.address = [];
                mainMember.collectionDetail = [];
                mainMember.bankAccount = [];
                mainMember.coveredBenefits = [];

                var mainMemberBenefits = new coveredBenefits();

                mainMemberBenefits.benefitCode = "1100";
                mainMemberBenefits.benefitAmount = $("#main_benefit_amount").val();

                mainMember.coveredBenefits.push(mainMemberBenefits);
                console.log(mainMember);

                coveredPeople[0] = mainMember;

                console.log("coveredPeople");
                console.log(coveredPeople);
                localStorage.setItem("mainCoverAmount", mainMemberBenefits.benefitAmount);
                window.app.mobileApp.navigate('components/covisionApplication/step_4.html', 'slide');

            } // if (validator.validate())**
            else {

                console.log("validation unsuccessfull!");
                console.log(mainMemberFormValidate);

                navigator.notification.alert(
                    message, // the message
                    function() {}, // a callback
                    options.title, // a title
                    options.buttonText // the button text
                ); // navigator.notification.alert
            } // else
        }, // addMainMember: function() 

        addMainAddressDetails: function() {

            console.log("addMainAddressDetails: function ()");

            var message = "Please ensure that all mandatory fields are filled in",
                options = {
                    title: 'Invalid Fields',
                    buttonText: 'OK',
                    callback: function() {}
                }; // var options

            console.log(switchVar.val());

            var postalAddress = new address();
            postalAddress.type = "postal";
            postalAddress.street = covisionApplicationModel.fields.postalStreet;
            postalAddress.suburb = covisionApplicationModel.fields.postalSuburb;
            postalAddress.city = covisionApplicationModel.fields.postalCity;
            postalAddress.code = covisionApplicationModel.fields.postalCode;
            postalAddress.province = covisionApplicationModel.fields.postalProvince;

            var phyAddress = new address();

            if (switchVar.val() == 'on') {
                covisionApplicationModel.fields.residentialStreet = covisionApplicationModel.fields.postalStreet;
                covisionApplicationModel.fields.residentialSuburb = covisionApplicationModel.fields.postalSuburb;
                covisionApplicationModel.fields.residentialCity = covisionApplicationModel.fields.postalCity;
                covisionApplicationModel.fields.residentialCode = covisionApplicationModel.fields.postalCode;
                covisionApplicationModel.fields.residentialProvince = covisionApplicationModel.fields.postalProvince;


                phyAddress.type = "physical";
                phyAddress.street = covisionApplicationModel.fields.residentialStreet;
                phyAddress.suburb = covisionApplicationModel.fields.residentialSuburb;
                phyAddress.city = covisionApplicationModel.fields.residentialCity;
                phyAddress.code = covisionApplicationModel.fields.residentialCode;
                phyAddress.province = covisionApplicationModel.fields.residentialProvince;

            } // if (switchVar.val()
            else {

                phyAddress.type = "physical";
                phyAddress.street = covisionApplicationModel.fields.residentialStreet;
                phyAddress.suburb = covisionApplicationModel.fields.residentialSuburb;
                phyAddress.city = covisionApplicationModel.fields.residentialCity;
                phyAddress.code = covisionApplicationModel.fields.residentialCode;
                phyAddress.province = covisionApplicationModel.fields.residentialProvince;

            } // else 

            for (var i = 0; i < coveredPeople.length; i++) {
                if (coveredPeople[i].relationship === "self") {
                    console.log("::::::::::;  Adding address details for main life ");

                    coveredPeople[i].address.push(postalAddress);
                    coveredPeople[i].address.push(phyAddress);

                    break;
                } // if (coveredPeople[i].relationship === "self")

            } // for (var i = 0l i < coveredPeople.length; i++)

            window.app.mobileApp.navigate('components/covisionApplication/step_5.html', 'slide');


        }, // addMainAddressDetails

        addBeneficiaryDetails: function() {
            console.log(":::::::::::: Add Beneficiary Details.");
            var message = "Please ensure that all mandatory fields are filled in",
                options = {
                    title: 'Invalid Fields',
                    buttonText: 'OK',
                    callback: function() {}
                }; // var options

            event.preventDefault();
            covisionApplicationModel.fields.beneficiaryRelation = $("#select-bene-relation").val();
            covisionApplicationModel.fields.beneficiaryBirthDate = $("#bene-dob").val()

            if (beneficiaryValidate.validate()) {
                var beneficiary = new member();

                beneficiary.memberId = beneficiary.uid;
                beneficiary.title = covisionApplicationModel.fields.beneficiaryTitle;
                beneficiary.firstNames = covisionApplicationModel.fields.beneficiaryName;
                beneficiary.lastName = covisionApplicationModel.fields.beneficiarySurname;
                beneficiary.idNumber = $('#bene-id-number').val();
                beneficiary.gender = getGender($('#bene-id-number').val());
                beneficiary.dateOfBirth = getBirthdate($('#bene-id-number').val());

                // if ($('#bene-gender').val() === "" || $('#bene-gender').val() === undefined) {
                //     beneficiary.gender = getGender(covisionApplicationModel.fields.idNumber.toString());
                // } else {

                // }

                // beneficiary.dateOfBirth = getBirthdate(covisionApplicationModel.fields.beneficiaryIDNumber.toString());
                beneficiary.roleCode = "Bene";
                beneficiary.relationship = covisionApplicationModel.fields.beneficiaryRelation;

                var index = coveredPeople.length;

                coveredPeople.push(beneficiary);

            } else {
                navigator.notification.alert(
                    message, // the message
                    function() {}, // a callback
                    options.title, // a title
                    options.buttonText // the button text
                ); // navigator.notification.alert
            } // else 

            console.log("coveredPeople");
            console.log(coveredPeople);

        }, // addBeneficiaryDetails: function ()

        addBeneficiary: function() {

            console.log("addBeneficiary: function ()");

            app.covisionApplication.covisionApplicationModel.addBeneficiaryDetails();

            console.log("coveredPeople");
            console.log(coveredPeople);

            window.app.mobileApp.navigate("components/covisionApplication/step_7.html", "slide");
        }, // addBeneficiary: function ()

        addPaymentDetails: function() {

            console.log("addPaymentDetails: function ()");
            localStorage.setItem("commencementMonth", $("#select-commencement-month").val());
            console.log("isPayer2 : " + covisionApplicationModel.fields.isPayer);
            if (covisionApplicationModel.fields.isPayer) {
                // add bank and payment details to the main member object
                console.log("isPayer : " + covisionApplicationModel.fields.isPayer);

                for (var i = 0; i < coveredPeople.length; i++) {
                    console.log("outside for loop: main as payer");
                    if (coveredPeople[i].relationship === "self") {
                        console.log(covisionApplicationModel.fields.paymentMethod);

                        var mainCollectionDetail = new collectionDetail();

                        if (covisionApplicationModel.fields.paymentMethod === "debitOrder") {
                            var mainBankAccount = new bankAccount();

                            // add banking details 
                            mainBankAccount.accountType = covisionApplicationModel.fields.accType;
                            mainBankAccount.bank = covisionApplicationModel.fields.accBankName;
                            mainBankAccount.branchName = covisionApplicationModel.fields.accBranchName;
                            mainBankAccount.branchCode = covisionApplicationModel.fields.accBranchCode;
                            mainBankAccount.accountNumber = covisionApplicationModel.fields.accNumber;

                            // push banking details into the object 
                            coveredPeople[i].bankAccount.push(mainBankAccount);

                        } // if (covisionApplicationModel.fields.paymentMethod === "debitOrder")
                        else {
                            covisionApplicationModel.fields.paymentMethod = "cash";
                        } // else 

                        // add collection details
                        mainCollectionDetail.collectionMethod = covisionApplicationModel.fields.paymentMethod;
                        mainCollectionDetail.collectionDate = covisionApplicationModel.fields.accDebitDate;

                        // Push collection details into the object 
                        coveredPeople[i].collectionDetail.push(mainCollectionDetail);

                        // exit the loop once the main member has been found
                        break;
                    } // if (coveredPeople[i].relationship === "self")
                } // for (var i = 0; i < coveredPeople.length; i++)
                console.log("coveredPeople >>>>> |");
                console.log(coveredPeople);
            } // if (covisionApplicationModel.fields.isPayer) 
            else {

                // add new member object(payer) to the data model
                var payer = new member();
                payer.memberId = payer.uid;
                payer.firstNames = covisionApplicationModel.fields.payerName;
                payer.lastName = covisionApplicationModel.fields.payerSurname;
                payer.idNumber = covisionApplicationModel.fields.payerIDNumber;
                payer.totalMonthlySalary = covisionApplicationModel.fields.payerMonthlyIncome;
                payer.cellNumber = covisionApplicationModel.fields.payerCell;
                payer.telWork = covisionApplicationModel.fields.payerTelWork;
                payer.telHome = covisionApplicationModel.fields.payerTelHome;
                payer.roleCode = "policyp";
                payer.relationship = covisionApplicationModel.fields.payerRelation;
                payer.collectionDetail = [];
                payer.bankAccount = [];
                coveredPeople.push(payer);
                console.log(covisionApplicationModel.fields.paymentMethod);
                console.log(payer);
                if (covisionApplicationModel.fields.paymentMethod === "debitOrder") {
                    console.log("before For loop")
                    for (i = 0; i < coveredPeople.length; i++) {
                        console.log("inside for loop " + i);
                        if (coveredPeople[i].roleCode.toLowerCase() === "policyp") {
                            console.log("the policy payer!")
                            var payerBankAccount = new bankAccount(),
                                payerCollectionDetail = new collectionDetail();

                            // add banking details 
                            payerBankAccount.accountType = covisionApplicationModel.fields.accType;
                            payerBankAccount.bank = covisionApplicationModel.fields.accBankName;
                            payerBankAccount.branchName = covisionApplicationModel.fields.accBranchName;
                            payerBankAccount.branchCode = covisionApplicationModel.fields.accBranchCode;
                            payerBankAccount.accountNumber = covisionApplicationModel.fields.accNumber;

                            // push banking details into the object 
                            coveredPeople[i].bankAccount.push(payerBankAccount);

                            // add collection details
                            payerCollectionDetail.collectionMethod = covisionApplicationModel.fields.paymentMethod;
                            payerCollectionDetail.collectionDate = covisionApplicationModel.fields.accDebitDate;

                            // Push collection details into the object 
                            coveredPeople[i].collectionDetail.push(payerCollectionDetail);
                        } // if (coveredPeople[i].relationship.toLowerCase() === 'policyp')

                    } // for (var i = 0; i < coveredPeople.length; i++)
                } // if (covisionApplicationModel.fields.paymentMethod === "debitOrder")
                else {
                    // assign payment method to cash regardless of it being easyPay or not
                    covisionApplicationModel.fields.paymentMethod = "cash";
                } // else 
            } // else 

            // if (typeof JSON.parse(localStorage.getItem("agentData")).rqResponse.dsProducer === "object") {
            var agentData = new member();
            
            agentData.memberId = "3901e385-2cfd-4f07-a9a2-7f7838e3b338";
            agentData.firstNames = "ROYALS-1754-BAH094";
            agentData.roleCode = "agent";

            coveredPeople.push(agentData);

            // } // if (typeof JSON.parse(localStorage.getItem("agentData")))
            // console.log(coveredPeople);
            app.covisionApplication.covisionApplicationModel.generateRequestXML();

        }, // addPaymentDetails: function ()

        generateRequestXML: function() {

            var accordXML = createAccordXML(coveredPeople);

            console.log("generateRequestXML: function ()");
            console.log(coveredPeople);
            // /console.log(accordXML);

            console.log(accordXML);
            localStorage.setItem('mnt:sessionid', 'Session:GSTSS|9773713');

            var fd = new FormData();
            fd.append('rqAuthentication', "user:hollardservice|mip123" + '|GSMUS|&login_company_branch_obj=91211.878*102594.188');
            fd.append('rqDataMode', 'VAR/XML');
            fd.append('rqService', 'AcordHoldingService:PolicyNewBusiness');
            fd.append('dsHoldingMaintenanceRequest', accordXML);

            var params = {
                rqAuthentication: "user:hollardservice|mip123|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                rqDataMode: "VAR/XML",
                rqService: "AcordHoldingService:PolicyNewBusiness",
                dsHoldingMaintenanceRequest: accordXML
            }

            console.log("form data");
            console.log(fd);
            // console.log(params);

            mipSpinner.show();

            $.ajax({
                type: "POST",
                url: 'https://nrt.hollardlifeadmin.co.za/cgi-bin/wspd_nrt.sh/WService=wsb_rsanrt_services/rest.w',
                // url: 'http://1502.life.mip.co.za/cgi-bin/wspd_116.sh/WService=wsb_881mnt/rest.w',
                data: fd,
                processData: false,
                contentType: false,
                success: function(data) {
                    console.log(data);
                    // Error Handling in xml
                    console.log(data);

                    var serviceError = data.getElementsByTagName('ResultInfoDesc');
                    var systemError = data.getElementsByTagName('rqErrorMessage');
                    var messages = "";

                    mipSpinner.hide();
                    if (systemError.length !== 0) {
                        mipAlert.show(systemError[0].childNodes[0].nodeValue, {
                            title: 'Error'
                        }); // mipAlert.show(systemError[0].childNodes[0].nodeValue
                    } // if (systemError.length != 0)
                    else if (serviceError.length !== 0) {
                        for (var i = 0; i < serviceError.length; i++) {
                            messages = messages + data.getElementsByTagName('ResultInfoDesc')[i].childNodes[0].nodeValue + "\n";
                        } // for ( var i = 0; i < serviceError.length; i++)

                        mipAlert.show(messages, {
                            title: 'Error'
                        }); // mipAlert.show()
                    } // if (serviceError.length != 0)
                    else {
                        // the rest comes here
                        console.log("AcordHoldingService:PolicyNewBusiness - SUCCESS!");
                        console.log(data);

                        if (data.getElementsByTagName("Policy").length !== 0) {
                            var policies = data.getElementsByTagName("Policy");
                            if (policies === undefined || policies === "" || policies == []) {
                                messages = "";
                                for (var i = 0; i < data.getElementsByTagName("ResultInfoDesc").length; i++) {
                                    messages = messages + data.getElementsByTagName('ResultInfoDesc')[i].childNodes[0].nodeValue + "\n";
                                }
                                mipAlert.show("Creation of the policy has failed: " + data.getElementsByTagName("ResultInfoDesc")[0].childNodes);
                                console.log("Creation of the policy has failed: " + data.getElementsByTagName("ResultInfoDesc")[0].childNodes);
                            } // if (policies === undefined || policies === "") 
                            else {
                                var contractNumber = policies[0].getElementsByTagName("PolNumber")[0].childNodes[0].nodeValue;
                                console.log("SUCCESS!!!!!! " + contractNumber);
                                var restParams1 = {
                                    service: "mnt",
                                    parameters: {
                                        rqAuthentication: "user:hollardservice|mip123|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                                        rqService: "ContractService:fetchContractData",
                                        rqDataMode: "VAR/JSON",
                                        ipcContractNumber: contractNumber
                                    }, // parameters

                                    requestSuccess: function(data) {
                                        console.log("ContractService:fetchContractData - SUCCESS!");
                                        console.log(data);

                                        if (data.rqResponse.rqErrorMessage) {
                                            navigator.notification.alert(
                                                data.rqResponse.rqErrorMessage, // the message
                                                function() {}, // a callback
                                                "Error", // a title
                                                "OK" // the button text
                                            ); // navigator.notification.alert
                                        } // if (data.rqResponse.rqErrorMessage)
                                        else {
                                            window.app.mobileApp.navigate('components/covisionApplication/final.html', 'slide');
                                            localStorage.setItem("newPolicyData", JSON.stringify(data));

                                            var ttContract = data.rqResponse.dsContract.ttContract[0];

                                            covisionApplicationModel.fields.planName = JSON.stringify(ttContract.ttContractProduct[0].cProductName);
                                            covisionApplicationModel.fields.effectiveDate = JSON.stringify(ttContract.dtCommencementDate);
                                            covisionApplicationModel.fields.paymenAmount = JSON.stringify(ttContract.ttContractCollectionDetails[0].dPremium);
                                            covisionApplicationModel.fields.policyStatus = JSON.stringify(ttContract.ttContractStatus[0].cContractStatus);
                                            covisionApplicationModel.fields.productCode = JSON.stringify(ttContract.ttContractProduct[0].cCode);

                                            /* ========================================================== */
                                            /* == Fire of workflow process == */

                                            // find main Contract Role player
                                            var applicantName,
                                                appliedDate,
                                                applicantEmailAddress,
                                                applicantPremium,
                                                applicantContactNumber,
                                                brokerName = "kennethc",
                                                brokerContactNo = "0720866181",
                                                brokerEmail = "ralwinnj@mip.co.za";

                                            for (var i = 0; i < ttContract.ttContractRole.length; i++) {
                                                if (ttContract.ttContractRole[i].cPlayerInRoleDesc === 'Main Life Assured') {
                                                    applicantName = ttContract.ttContractRole[i].cFirstName + " " + ttContract.ttContractRole[i].cLastName;
                                                    appliedDate = new Date();
                                                    applicantEmailAddress = "ralwinnj@mip.co.za";
                                                    applicantPremium = ttContract.ttContractCollectionDetails[0].dPremium;
                                                    applicantContactNumber = "0634336931";
                                                    break;
                                                } // if (policies === undefined || policies === "")
                                            } //  for (var i = 0; i < ttContract.ttContractRole.length; i++)

                                            // console.log(applicantName + " -- " + appliedDate);
                                            var ipcRemNVP = 'CBMCT=' + ttContract.cContractIdentifier;
                                            var ipcRefNVP = 'pf_ApplicantName=' + applicantName + ',pf_AppliedDate=' + appliedDate + ',pf_ApplicantEmailAddress=' + applicantEmailAddress + ',pf_ApplicantPremium=' + applicantPremium + ',pf_ApplicantContactNumber=' + applicantContactNumber + ',pf_BrokerName=' + brokerName + ',pf_BrokerCont=' + brokerContactNo + ',pf_BrokerEmail=' + brokerEmail;

                                            //window.dataJSON = ttContract;
                                            // var params2 = {
                                            //     service: "mnt",
                                            //     parameters: {
                                            //         rqAuthentication: 'user:hollardservice|mip123|GSMUS|&login_company_branch_obj=91211.878*102594.188',
                                            //         rqDataMode: "VAR/JSON",
                                            //         //rqService: 'pfUtilityService:launchRestWFProcess',
                                            //         ipcProcessCode: 'wf_Mob_New_Bus_Test',
                                            //         ipcRemNVP: ipcRemNVP,
                                            //         ipcRefNVP: ipcRefNVP
                                            //     }, // parameters

                                            //     requestSuccess: function(data) {
                                            //         console.log(params2.parameters.rqService + " - SUCCESS!");
                                            //         console.log(data);
                                            //         mipSpinner.hide();
                                            //         if (data.rqResponse.rqErrorMessage) {
                                            //             navigator.notification.alert(
                                            //                 data.rqResponse.rqErrorMessage, // the message
                                            //                 function() {}, // a callback
                                            //                 "Error", // a title
                                            //                 "OK" // the button text
                                            //             ); // navigator.notification.alert
                                            //         } // if (data.rqResponse.rqErrorMessage)
                                            //         else {
                                            //             console.log(params2.parameters.rqService + " - SUCCESS!");
                                            //             console.log(JSON.stringify(data));
                                            //             localStorage.setItem("workflowLog", JSON.stringify(data));
                                            //             window.app.mobileApp.navigate('components/covisionApplication/final.html', 'slide');
                                            //         } // else
                                            //     }, // requestSuccess: function ()

                                            //     requestError: function(d, e, c) {
                                            //         localStorage.setItem("mnt:sessionid", "");
                                            //         localStorage.setItem("appData", "");
                                            //         mipAlert.show(e, {
                                            //             title: c
                                            //         }); // mipAlert.show()
                                            //     }, // requestError

                                            //     spinner: {
                                            //         message: "Authenticating",
                                            //         title: "",
                                            //         isFixed: true
                                            //     }, // spinner

                                            //     availableOfflineFor: {
                                            //         minutes: 120
                                            //     } // availableOfflineFor
                                            // }; // params
                                            // mipRest.request(params2);
                                        } // else


                                    }, // requestSuccess

                                    requestError: function(d, e, c) {
                                        console.log("ERROR: " + e);
                                        // localStorage.setItem("mnt:sessionid", "");
                                        // localStorage.setItem("appData", "");
                                        mipAlert.show(e, {
                                            title: c
                                        }); // mipAlert.show(e)
                                    }, // requestError

                                    spinner: {
                                        message: "Authenticating",
                                        title: "",
                                        isFixed: true
                                    }, // spinner

                                    availableOfflineFor: {
                                        minutes: 120
                                    } // availableOfflineFor

                                }; // restParams1
                                mipRest.request(restParams1);
                            } // else 
                        } // if (typeof data.getElementsByTagName("Policy") === 'object') 
                    } // else
                }, // success: function (data)

                error: function(data, error, code) {
                    console.log(error);
                    mipSpinner.hide();
                    mipAlert.show(error, {
                        title: 'Error'
                    }); // mipAlert.show(error)
                }, // error: function (data, error, code)
            }); // $.ajax
        }, // generateRequestXML: function ()

        removeDependent: function(index) {
            var i = parseInt(index);
            console.log("removeDependent: function (index)");
            console.log(coveredPeople[i]);
            $("#" + coveredPeople[i].memberId).remove();
            coveredPeople.splice(i, 1);
        }, // removeDependent: function ()

        gotoDependentAdd: function() {
            console.log("gotoDependentAdd: function ()");

            brandNewDependent = true; // false by default

            covisionApplicationModel.fields.depIdNumber = "";
            covisionApplicationModel.fields.depFirstNames = "";
            covisionApplicationModel.fields.depLastName = "";
            covisionApplicationModel.fields.depMaritalStatus = "";
            covisionApplicationModel.fields.depRelationship = "";

            window.app.mobileApp.navigate("components/covisionApplication/add_dependent.html", "slide");

            $("#option-spouse").show();
            for (var i = 0; i < coveredPeople.length; i++) {
                if (coveredPeople[i].relationship.toLowerCase === 'spouse') {
                    console.log('=== "spouse" ');
                    $("#option-spouse").hide();
                    break;
                } // if (coveredPeople[i]...
            } // for(var i = 0; i...
        }, // gotoDependentAdd: function ()

        goBackDependentList: function() {
            console.log("goBackDependentList: function ()");
            console.log("Clear the fields used on the form");

            covisionApplicationModel.fields.depIdNumber = "";
            covisionApplicationModel.fields.depFirstNames = "";
            covisionApplicationModel.fields.depLastName = "";
            covisionApplicationModel.fields.depMaritalStatus = "";
            covisionApplicationModel.fields.depRelationship = "";

            dontRun = true;
            brandNewDependent = false;

            console.log("goBackDependentList");
            console.log(coveredPeople);

            window.app.mobileApp.navigate("components/covisionApplication/step_5.html", "slide:left reverse");
        }, // goBackDependentList 

        addNewDependent: function() {
            var message = "Please ensure that all mandatory fields are filled in",
                options = {
                    title: 'Invalid Fields',
                    buttonText: 'OK',
                    callback: function() {}
                }; // var options

            event.preventDefault();
            if (addDependentFormValidate.validate()) {
                var uid,
                    dependent = [];

                console.log("validation successfull");
                console.log("addNewDependent: funtion ()");
                console.log(coveredPeople);

                if (brandNewDependent === true) {
                    uid = "";
                } else {
                    uid = $("#member-id").val();
                }

                console.log(uid);


                for (var i = 0; i < coveredPeople.length; i++) {
                    if (uid == coveredPeople[i].uid) {
                        console.log("success");
                        dependent = coveredPeople[i];
                        coveredPeople.splice(i, 1);
                        break;
                    } // if (uid == coveredPeople[i].uid)
                    else {
                        console.log("failure!");
                        dependent = new member();
                        dependent.memberId = dependent.uid;
                    } // else
                } // for (var i = 0; i < coveredPeople.length; i++)

                if (coveredPeople.length === 0) {
                    dependent = new member();
                } // if (coveredPeople.keb)

                var roleCode = "lifea",
                    benefitCode = "",
                    depId;

                // default to a marital status of 'single' if relation is 'child'
                if ($("#dep-relation").val().toLowerCase() !== 'spouse') {
                    // $("#dep-marital").val("single");
                } // if ($("#dep-relation").val().toLowerCase() === 'child')

                dependent.title = "";
                dependent.firstNames = $("#dep-name").val().toLowerCase();
                dependent.lastName = $("#dep-surname").val().toLowerCase();
                dependent.maritalStatus = $("#dep-marital").val().toLowerCase();
                dependent.dateOfBirth = $("#dep-dob").val().toLowerCase();
                dependent.gender = $('#dep-gender').val();
                dependent.roleCode = roleCode;
                dependent.relationship = $("#dep-relation").val().toLowerCase();
                dependent.coveredBenefits = [];

                var dependentBenefits = new coveredBenefits();
                var age = getAgeFromDate($("#dep-dob").val());
                // determine benefitCode
                switch ($("#dep-relation").val().toLowerCase()) {
                    case "spouse":
                        benefitCode = "1110";
                        break;
                    case "child":
                        benefitCode = "1110";
                        break;
                    case "parent":
                        benefitCode = "1130";
                        break;
                    case "aunt":
                        benefitCode = "1140";
                        break;
                    case "uncle":
                        benefitCode = "1140";
                        break;
                    case "nephew":
                        benefitCode = "1140";
                        if (age < 21) {
                            benefitCode = "1170";
                        } else {
                            benefitCode = "1140";
                        }
                        break;
                    case "niece":
                        benefitCode = "1140";
                        if (age < 21) {
                            benefitCode = "1170";
                        } else {
                            benefitCode = "1140";
                        }
                        break;
                    case "adult child":
                        benefitCode = "1140";
                        break;
                    case "sister":
                        benefitCode = "1140";
                        if (age < 21) {
                            benefitCode = "1170";
                        } else {
                            benefitCode = "1140";
                        }
                        break;
                    case "brother":
                        benefitCode = "1140";
                        if (age < 21) {
                            benefitCode = "1170";
                        } else {
                            benefitCode = "1140";
                        }
                        break;
                } // switch ($("#dep-relation").val().toLowerCase())

                // assign benefitCode  
                dependentBenefits.benefitCode = benefitCode;
                dependentBenefits.benefitAmount = $("#dep_benefit_amount").val();

                // push benefit details to dataModel
                dependent.coveredBenefits.push(dependentBenefits);

                console.log(":::::::::: dependent object");
                console.log(dependent);

                coveredPeople.push(dependent);

                $("#dep_benefit_amount").val("");
                $("#dep-name").val("");
                $("#dep-surname").val("");
                $("#dep-id-number").val("");
                $("#dep-gender").val("");
                $("#dep-relation").val("");

                dontRun = true;
                brandNewDependent = false;
                $("#member-id").val("");
                window.app.mobileApp.navigate("components/covisionApplication/step_5.html", "slide:left reverse");

            } // if (validator.validate())**
            else {

                console.log("validation unsuccessfull!");
                console.log(addDependentFormValidate);

                navigator.notification.alert(
                    message, // the message
                    function() {}, // a callback
                    options.title, // a title
                    options.buttonText // the button text
                ); // navigator.notification.alert
            } // else
        }, // addNewDependent: function ()

        getEasyPayRef: function() {

            console.log("getEasyPayRef: function ()");

            var restParams1 = {
                service: "mnt",
                parameters: {
                    rqAuthentication: "user:hollardservice|mip123" + "|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                    rqDataMode: "VAR/JSON",
                    rqService: 'ContractService:getContractDetails',
                    ipcContractNumber: covisionApplicationModel.fields.policyNumber
                }, // parameters

                requestSuccess: function(data) {
                    console.log(restParams1.parameters.rqService + " - SUCCESS!");
                    console.log(data);
                    if (JSON.stringify(data.rqResponse.rqFullError) !== undefined) {

                        mipAlert.show(data.rqResponse.rqErrorMessage, {
                            title: 'Error'
                        }); // mipAlert.show(data.rqResponse.rqErrorMessage

                        console.log(JSON.stringify(data.rqResponse.rqFullError));

                    } // if (data.rqFullError !== "" || 
                    else {

                        // assign contract obj to the model
                        covisionApplicationModel.fields.policyNumber = data.rqResponse.opcContractNumber;
                        covisionApplicationModel.fields.policyObj = data.rqResponse.opdContractObj;

                        var restParams2 = {
                            service: "mnt",
                            parameters: {
                                rqAuthentication: "user:hollardservice|mip123" + "|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                                rqDataMode: "VAR/JSON",
                                rqService: "pfUtilityService:fetchEntityCriterionData",
                                pcOwningEntityMnemonic: "CBMCT",
                                pdOwningObj: covisionApplicationModel.fields.policyObj
                            }, // parameters

                            requestSuccess: function(data) {

                                console.log(restParams2.parameters.rqService + " - SUCCESS!");
                                console.log(data);

                                if (JSON.stringify(data.rqResponse.rqFullError) !== undefined) {
                                    console.log(JSON.stringify(data.rqResponse.rqFullError));

                                } // if (data.rqFullError !== "" ||
                                else {
                                    var ttEntityCriterion = data.rqResponse.dsEntityCriteria.ttEntityCriterion;

                                    for (var i = 0; i < ttEntityCriterion.length; i++) {
                                        for (var iType = 0; iType < ttEntityCriterion[i].ttCriterionType.length; iType++) {
                                            console.log(ttEntityCriterion[i].ttCriterionType[iType].cCriterionCode);
                                            if (ttEntityCriterion[i].ttCriterionType[iType].cCriterionCode === "EasyPayRef") {
                                                covisionApplicationModel.fields.easyPayReference = ttEntityCriterion[i].cCriterionValue;

                                                console.log(covisionApplicationModel.fields.easyPayReference + "::::::" + ttEntityCriterion[i].cCriterionValue);

                                                $("#easyPay-ref-num").empty();
                                                $("#easyPay-ref-num").prepend('<span id="easyPay-ref-num" class="darkgreen">' + covisionApplicationModel.fields.easyPayReference + '</span>');
                                                $("#easypay-reference").show(600);

                                            } // if (ttEntityCriterion[i].ttCriterionType[i].cCriterionCode === "EasyPayRef")
                                        } // for (var index = 0; index < ttEntityCriterion[i].ttCriterionType.length; index++)
                                    } // for (var i = 0; i < ttEntityCriterion.length; i++) {
                                } // else
                            }, // requestSuccess: function (data)

                            requestError: function(d, e, c) {
                                localStorage.setItem("mnt:sessionid", "");
                                localStorage.setItem("appData", "");
                                mipAlert.show(e, {
                                    title: c
                                }); // mipAlert.show(e)
                            }, // requestError

                            spinner: {
                                message: "Authenticating",
                                title: "",
                                isFixed: true
                            }, // spinner

                            availableOfflineFor: {
                                minutes: 120
                            } // availableOfflineFor

                        }; // var restParams2
                        console.log("about to make 2nd request");
                        mipRest.request(restParams2);
                    } // else
                }, // requestSuccess

                requestError: function(d, e, c) {
                    localStorage.setItem("mnt:sessionid", "");
                    localStorage.setItem("appData", "");
                    mipAlert.show(e, {
                        title: c
                    }); // mipAlert.show(e,
                }, // requestError

                spinner: {
                    message: "Authenticating",
                    title: "",
                    isFixed: true
                }, // spinner

                availableOfflineFor: {
                    minutes: 120
                } // availableOfflineFor

            }; // restParams1
            mipRest.request(restParams1);
        }, // getEasyPayRef: function () 

        newApplication: function() {

            /* Start a new application/ if coming from the quote screen that will be handle in the onShow function of the next view[onShowMainMember] */
            console.log("newApplication: function ()");


            covisionApplicationModel.fields.depCoverAmount = "";
            covisionApplicationModel.fields.depFirstNames = "";
            covisionApplicationModel.fields.depLastName = "";
            covisionApplicationModel.fields.depIdNumber = "";
            covisionApplicationModel.fields.depMaritalStatus = "";
            covisionApplicationModel.fields.depRelationship = "";
            covisionApplicationModel.fields.depCoverAmount = "";
            covisionApplicationModel.fields.famCoverAmount = "";

            covisionApplicationModel.fields.title = "";
            covisionApplicationModel.fields.firstNames = "";
            covisionApplicationModel.fields.lastName = "";
            covisionApplicationModel.fields.idNumber = "";
            covisionApplicationModel.fields.maritalStatus = "";
            covisionApplicationModel.fields.email = "";
            covisionApplicationModel.fields.age = "";
            covisionApplicationModel.fields.birthDate = "";
            covisionApplicationModel.fields.benefitAmount = "";
            covisionApplicationModel.fields.premium = "";
            covisionApplicationModel.fields.gendertc = "";
            covisionApplicationModel.fields.gender = "";
            covisionApplicationModel.fields.totalMonthlyIncome = "";
            covisionApplicationModel.fields.cellNumber = "";
            covisionApplicationModel.fields.telWork = "";
            covisionApplicationModel.fields.telHome = "";
            covisionApplicationModel.fields.cellOrPost = "";

            /* POSTAL DETAILS */
            covisionApplicationModel.fields.postalStreet = "";
            covisionApplicationModel.fields.postalSuburb = "";
            covisionApplicationModel.fields.postalCity = "";
            covisionApplicationModel.fields.postalCode = "";

            /* RESIDENTIAL DETAILS */
            covisionApplicationModel.fields.residentialStreet = "";
            covisionApplicationModel.fields.residentialSuburb = "";
            covisionApplicationModel.fields.residentialCity = "";
            covisionApplicationModel.fields.residentialCode = "";

            /* BENEFICIARY DETAILS */
            covisionApplicationModel.fields.beneficiaryTitle = "";
            covisionApplicationModel.fields.beneficiaryName = "";
            covisionApplicationModel.fields.beneficiarySurname = "";
            covisionApplicationModel.fields.beneficiaryIDNumber = "";
            covisionApplicationModel.fields.beneficiaryBirthDate = "";
            covisionApplicationModel.fields.beneficiaryRelation = "";
            covisionApplicationModel.fields.beneficiaryContactNo = "";
            covisionApplicationModel.fields.beneficiaryGendertc = "";
            covisionApplicationModel.fields.beneficiaryGender = "";

            /* PAYMENT DETAILS (How will you pay the total amount) */
            covisionApplicationModel.fields.isPayer = true;
            covisionApplicationModel.fields.payerName = "";
            covisionApplicationModel.fields.payerSurname = "";
            covisionApplicationModel.fields.payerMonthlyIncome = "";
            covisionApplicationModel.fields.payerCell = "";
            covisionApplicationModel.fields.payerTelHome = "";
            covisionApplicationModel.fields.payerTelWork = "";
            covisionApplicationModel.fields.paymentMethod = "";
            covisionApplicationModel.fields.payerSignature = "";

            /* BANKING DETAILS (if paying by debit order) */
            covisionApplicationModel.fields.accOwnerTitle = "";
            covisionApplicationModel.fields.accOwnerName = "";
            covisionApplicationModel.fields.accOwnerSurname = "";
            covisionApplicationModel.fields.accOwnerIDNumber = "";
            covisionApplicationModel.fields.accOwnerTelWork = "";
            covisionApplicationModel.fields.accOwnerTelHome = "";
            covisionApplicationModel.fields.accBankName = "";
            covisionApplicationModel.fields.accBranchName = "";
            covisionApplicationModel.fields.accNumber = "";
            covisionApplicationModel.fields.accOwnerCell = "";
            covisionApplicationModel.fields.accDebitDate = "";
            covisionApplicationModel.fields.accBranchCode = "";
            covisionApplicationModel.fields.accType = "";
            covisionApplicationModel.fields.accOwnerSignature = "";

            /* FINALISING */
            covisionApplicationModel.fields.yourSignature = "";
            covisionApplicationModel.fields.yourSignDate = new Date();

            coveredPeople = [];

            console.log(covisionApplicationModel.fields);

            $("input[type=text]").val("");
            $("input[type=tell]").val("");
            $("input[type=email]").val("");
            $("input[type=number]").val("");
            $("input[type=select]").val("");

            window.app.mobileApp.navigate("components/covisionApplication/step_3.html", "slide");
        }, // newApplication: function ()

        scanEasyPay: function() {
            $("#easyPay-ref-num").empty();
            cordova.plugins.barcodeScanner.scan(
                function(result) {
                    mipAlert("We got a barcode\n" +
                        "Result: " + result.text + "\n" +
                        "Format: " + result.format + "\n" +
                        "Cancelled: " + result.cancelled);

                    var restParams1 = {
                        parameters: {
                            rqAuthentication: "user:hollardservice|mip123|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                            rqDataMode: "VAR/JSON",
                            rqService: "ContractService:FetchContractByCriterion",
                            ipcCriterionCode: "EasyPayRef",
                            ipcCriterionValue: result.text
                        }, // parameters

                        requestSuccess: function(data) {
                            console.log("requestSuccess!  ContractService:FetchContractByCriterion");
                            console.log(data);

                            covisionApplicationModel.fields.policyNumber = data.rqResponse.dsContract.ttContract[0].cContractNumber;
                            covisionApplicationModel.fields.easyPayReference = result.text;
                            $("#easyPay-ref-num").append('<span class="darkgreen">CONTRACT NUMBER</span>\
                                <br>\
                                <span class="lightgreen">' + data.rqResponse.dsContract.ttContract[0].cContractNumber + '</span>\
                                <br>\
                                <hr width="75%" color="#006652">\
                                <br>\
                                <span>EASYPAY NUMBER</span>\
                                <br>\
                                <spanclass="lightgreen">' + result.text + '</span>');
                        }, // requestSuccess

                        requestError: function(d, e, c) {
                            console.log(d + " -- " + e + " -- " + c);
                            mipAlert.show(e, {
                                title: c
                            }); // mipAlert.show(e)
                        }, // requestError

                        spinner: {
                            message: "Logging in...",
                            title: "Login",
                            isFixed: true
                        }, // spinner

                        availableOfflineFor: {
                            minutes: 120
                        } // availableOfflineFor

                    }; // restParams1
                    mipRest.request(restParams1);
                }, // function(result)
                function(error) {
                    alert("Scanning failed: " + error);
                }, {
                    "preferFrontCamera": false, // iOS and Android
                    "showFlipCameraButton": true, // iOS and Android
                    "prompt": "Place a barcode inside the scan area", // supported on Android only
                    "formats": "CODE_128", // default: all but PDF_417 and RSS_EXPANDED
                    "orientation": "portrait" // Android only (portrait|landscape), default unset so it rotates with the device
                }
            ); //  cordova.plugins.barcodeScanner.scan
        }, // scanEasyPay: function ()

        completeApplication: function() {
            mipAlert.show("The policy has been successfully created.", { title: "Success" });
            window.app.mobileApp.navigate("components/home/view.html", "slide");
        }, // completeApplication: function () 

        editDependent: function(depObj) {
            console.log("editDependent: function() - Navigate to add dependent screeen;");
            console.log(depObj);
            localStorage.setItem("depData", JSON.stringify(depObj));
            brandNewDependent = false;
            window.app.mobileApp.navigate("components/covisionApplication/add_dependent.html", "slide");
        }, // editDependent: function() 

        gotoBeneficiaryDetails: function() {
            navigator.notification.confirm(
                'Please confirm that the details entered are complete as you will not be able to change these at a later stage in the application process. Tap Ok to continue or Cancel to review.', // message
                onConfirmDepDetails, // callback to invoke with index of button pressed
                "Confirm", // title
                ['Ok', 'Cancel'] // buttonLabels
            ); // navigator.notification.confirm
            var mainCover = localStorage.getItem("mainCoverAmount");
            app.covisionApplication.covisionApplicationModel.fields.famCoverAmount = mainCover;
            for (var i = 0; i < coveredPeople.length; i++) {
                if (coveredPeople[i].relationship === 'spouse') {
                    app.covisionApplication.covisionApplicationModel.fields.famCoverAmount = coveredPeople[i].coveredBenefits[0].benefitAmount;
                    break;
                } //   if (coveredPeople[i].relationship = 'spouse') 
            } // for (var i = 0; i < coveredPeople.length; i++)
        }, // gotoBeneficiaryDetails: function 

        startApplication: function() {
            if (fromQuote === true) {
                window.app.mobileApp.navigate("components/covisionApplication/step_2.html", "slide");
            } // if (fromQuote === true)
            else {
                /* DEPENDENT DETAILS */
                covisionApplicationModel.fields.depCoverAmount = "";
                covisionApplicationModel.fields.depFirstNames = "";
                covisionApplicationModel.fields.depLastName = "";
                covisionApplicationModel.fields.depIdNumber = "";
                covisionApplicationModel.fields.depMaritalStatus = "";
                covisionApplicationModel.fields.depRelationship = "";
                covisionApplicationModel.fields.depCoverAmount = "";

                /* MAIN DETAILS */
                covisionApplicationModel.fields.title = "";
                covisionApplicationModel.fields.firstNames = "";
                covisionApplicationModel.fields.lastName = "";
                covisionApplicationModel.fields.idNumber = "";
                covisionApplicationModel.fields.maritalStatus = "";
                covisionApplicationModel.fields.email = "";
                covisionApplicationModel.fields.age = "";
                covisionApplicationModel.fields.birthDate = "";
                covisionApplicationModel.fields.benefitAmount = "";
                covisionApplicationModel.fields.premium = "";
                covisionApplicationModel.fields.gendertc = "";
                covisionApplicationModel.fields.gender = "";
                covisionApplicationModel.fields.totalMonthlyIncome = "";
                covisionApplicationModel.fields.cellNumber = "";
                covisionApplicationModel.fields.telWork = "";
                covisionApplicationModel.fields.telHome = "";
                covisionApplicationModel.fields.cellOrPost = "";

                /* POSTAL DETAILS */
                covisionApplicationModel.fields.postalStreet = "";
                covisionApplicationModel.fields.postalSuburb = "";
                covisionApplicationModel.fields.postalCity = "";
                covisionApplicationModel.fields.postalCode = "";

                /* RESIDENTIAL DETAILS */
                covisionApplicationModel.fields.residentialStreet = "";
                covisionApplicationModel.fields.residentialSuburb = "";
                covisionApplicationModel.fields.residentialCity = "";
                covisionApplicationModel.fields.residentialCode = "";

                /* BENEFICIARY DETAILS */
                covisionApplicationModel.fields.beneficiaryTitle = "";
                covisionApplicationModel.fields.beneficiaryName = "";
                covisionApplicationModel.fields.beneficiarySurname = "";
                covisionApplicationModel.fields.beneficiaryIDNumber = "";
                covisionApplicationModel.fields.beneficiaryBirthDate = "";
                covisionApplicationModel.fields.beneficiaryRelation = "";
                covisionApplicationModel.fields.beneficiaryContactNo = "";
                covisionApplicationModel.fields.beneficiaryGendertc = "";
                covisionApplicationModel.fields.beneficiaryGender = "";

                /* PAYMENT DETAILS (How will you pay the total amount) */
                covisionApplicationModel.fields.isPayer = true;
                covisionApplicationModel.fields.payerName = "";
                covisionApplicationModel.fields.payerSurname = "";
                covisionApplicationModel.fields.payerMonthlyIncome = "";
                covisionApplicationModel.fields.payerCell = "";
                covisionApplicationModel.fields.payerTelHome = "";
                covisionApplicationModel.fields.payerTelWork = "";
                covisionApplicationModel.fields.paymentMethod = "";
                covisionApplicationModel.fields.payerSignature = "";

                /* BANKING DETAILS (if paying by debit order) */
                covisionApplicationModel.fields.accOwnerTitle = "";
                covisionApplicationModel.fields.accOwnerName = "";
                covisionApplicationModel.fields.accOwnerSurname = "";
                covisionApplicationModel.fields.accOwnerIDNumber = "";
                covisionApplicationModel.fields.accOwnerTelWork = "";
                covisionApplicationModel.fields.accOwnerTelHome = "";
                covisionApplicationModel.fields.accBankName = "";
                covisionApplicationModel.fields.accBranchName = "";
                covisionApplicationModel.fields.accNumber = "";
                covisionApplicationModel.fields.accOwnerCell = "";
                covisionApplicationModel.fields.accDebitDate = "";
                covisionApplicationModel.fields.accBranchCode = "";
                covisionApplicationModel.fields.accType = "";
                covisionApplicationModel.fields.accOwnerSignature = "";

                /* FINALISING */
                covisionApplicationModel.fields.yourSignature = "";
                covisionApplicationModel.fields.yourSignDate = new Date();

                coveredPeople = [];

                console.log(covisionApplicationModel.fields);

                $("input[type=text]").val("");
                $("input[type=tell]").val("");
                $("input[type=email]").val("");
                $("input[type=number]").val("");

                $("#main-benefit-amount").val("");
                $("#main-id-number").val("");
                $("#select-main-marital").val("");
                $("#select-main-salary").val("");

                $("#dep-name").val("");
                $("#dep-surname").val("");
                $("#dep-id-number").val("");
                $("#member-id").val("");
                $("#dep-benefit-amount").val("");
                $("#dep-marital").val("");
                $("#dep-relation").val("");
                $("#currentDependentList").empty();

                $("#select-bene-relation").val("");

                localStorage.setItem("defaultJSON", "");
                coveredPeople = [];

                fromQuote = false;

                window.app.mobileApp.navigate("components/covisionApplication/step_2.html", "slide");
            } // else 
        }, // startApplication: function () 
        cascadeCoverAmount: function() {
            // var mainCoverAmount = '15000', // use for testing[must be gotten from the main cover amount that will be assigned to localStorage]
            var mainCoverAmount = parseInt(localStorage.getItem("mainCoverAmount")),
                spouseCoverAmount = ["", 50000, 45000, 40000, 35000, 30000, 25000, 20000, 15000, 10000, 5000],
                parentCoverAmount = ["", 20000, 19000, 18000, 17000, 16000, 15000, 14000, 13000, 12000, 11000, 10000, 9000, 8000, 7000, 6000, 5000, 4000, 3000, 2000, 1000],
                extmemberCoverAmount = ["", 10000, 9000, 8000, 7000, 6000, 5000, 4000, 3000, 2000, 1000],
                initialValue = $("#dep_benefit_amount").val();

            mainCoverAmount = 50000;

            $("#dep-dob").val("");
            $("#dep_benefit_amount").empty();

            if (($("#dep-relation").val()).toLowerCase() == 'child') {
                $("#cover-amount-li").hide(500);
                $("#dep_benefit_amount").removeAttr("required");
            } // if (($("#dep-relation").val()).toString() == 'child')
            else if (($("#dep-relation").val()).toLowerCase() == 'spouse') {
                $("#cover-amount-li").show(500);
                for (var i = 0; i < spouseCoverAmount.length; i++) {
                    if (spouseCoverAmount[i] <= mainCoverAmount) {
                        if (spouseCoverAmount[i] === "") {
                            $("#dep_benefit_amount").append('<option value="' + spouseCoverAmount[i] + '"></option>');
                        } // if (spouseCoverAmount[i] == "")
                        else {
                            $("#dep_benefit_amount").append('<option value="' + spouseCoverAmount[i] + '">R ' + spouseCoverAmount[i] + '</option>');
                        } // else
                    } // if (spouseCoverAmount[i] <= mainCoverAmount) 
                } // for (var i = 0; i < spouseCoverAmount.length; i++)
            } // if (($("#dep-relation").val()).toString() == 'spouse')
            else if (($("#dep-relation").val()).toLowerCase() == 'parent') {
                $("#cover-amount-li").show(500);
                for (var i = 0; i < parentCoverAmount.length; i++) {
                    if (parentCoverAmount[i] <= mainCoverAmount) {
                        if (parentCoverAmount[i] === "") {
                            $("#dep_benefit_amount").append('<option value="' + parentCoverAmount[i] + '"></option>');
                        } // if (parentCoverAmount[i] == "")
                        else {
                            $("#dep_benefit_amount").append('<option value="' + parentCoverAmount[i] + '">R ' + parentCoverAmount[i] + '</option>');
                        } // else
                    } // if (parentCoverAmount[i] <= mainCoverAmount) 
                } // for (var i = 0; i < parentCoverAmount.length; i++)
            } // if (($("#dep-relation").val()).toString() == 'parent')
            else {
                $("#cover-amount-li").show(500);
                for (var i = 0; i < extmemberCoverAmount.length; i++) {
                    if (extmemberCoverAmount[i] <= mainCoverAmount) {
                        if (extmemberCoverAmount[i] === "") {
                            $("#dep_benefit_amount").append('<option value="' + extmemberCoverAmount[i] + '"></option>');
                        } // if (extmemberCoverAmount[i] == "")
                        else {
                            $("#dep_benefit_amount").append('<option value="' + extmemberCoverAmount[i] + '">R ' + extmemberCoverAmount[i] + '</option>');
                        } // else
                    } // if (extmemberCoverAmount[i] <= mainCoverAmount) 
                } // for (var i = 0; i < extmemberCoverAmount.length; i++)
            } // else
            $("#dep_benefit_amount").val(initialValue);
        }, // cascadeCoverAmount: function ()
        testCovered: function() {
            console.log(coveredPeople);
        },
    }); // var covisionApplicationModel = kendo.observable
    parent.set("covisionApplicationModel", covisionApplicationModel);
})(app.covisionApplication);
// END_CUSTOM_CODE_covisionApplication

function onConfirmDepDetails(buttonIndex) {
    if (parseInt(buttonIndex) === 1) {
        fromQuote = false;
        window.app.mobileApp.navigate("components/covisionApplication/step_6.html", "slide");
    } // if (parseInt(buttonIndex)
    else {
        // Do nothing...
    } // else 
}