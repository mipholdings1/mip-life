'use strict';

app.policies = kendo.observable({
    onShow: function () {
        if (localStorage.getItem("appData") === null) {
            localStorage.setItem("appData", "");
            console.log("");
        }
        console.log(localStorage.getItem("appData"));
    },

    afterShow: function () {}
}); // app.policies = kendo.observable 
function attachResource(entry) {
    var personalDetails = JSON.parse(localStorage.getItem("appData")).personalDetails[0],
        membershipDetails = JSON.parse(localStorage.getItem("appData")).membershipDetails[0];
    window.plugins.socialsharing.share("Please find attached your policy document",
        "Policy document for " + personalDetails.firstName + " " + personalDetails.lastName + "(" + membershipDetails.memberNumber + ")",
        entry.toURL()
    );
}

function transferFile(uri, filePath, callback) {
    var transfer = new FileTransfer();
    transfer.download(
        uri,
        filePath,
        callback,
        function (error) {
            console.log('Failed to download file: ' + error.code);
        }
    );
}
// START_CUSTOM_CODE_policies
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
(function (parent) {
    var policiesModel = kendo.observable({

        viewPolicy: function () {
            var restParams1 = {
                service: "mnt",
                parameters: {
                    rqAuthentication: localStorage.getItem("mnt:sessionid"),
                    rqDataMode: "VAR/JSON"
                }, // parameters

                requestSuccess: function (data) {
                    // this will download and view the PDF document
                    // after making sure the user has a valid session to 

                    if (data.rqResponse.rqErrorMessage) {
                        if (data.rqResponse.rqFullError.indexOf('mip_MsgSesErr:6') > 0) {
                            window.app.mobileApp.navigate("components/login/view.html", "slide");
                            localStorage.setItem("nextScreen", "components/policies/view.html");
                            //navigateTo = localStorage.getItem("loginView");
                        } // if (data.rqFullError.indexOf
                        else {
                            navigator.notification.alert(
                                data.rqResponse.rqErrorMessage, // the message
                                function () {}, // a callback
                                "Error", // a title
                                "OK" // the button text
                            ); // navigator.notification.alert 
                        } // else 
                    } //  if (data.rqResponse.rqFullError == "" || data.rqResponse.rqFullError == null)
                    else {
                        mipSpinner.show();
                        var params = {
                                rqAuthentication: "user:roberte|roberte|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                                rqDataMode: "VAR/JSON"
                            } // params
                        $.ajax({
                            url: "http://1502.life.mip.co.za/cgi-bin/wspd_113.sh/WService=wsb_815mnt/rest.w",
                            type: "POST",
                            data: params,
                            success: function (data) {
                                mipSpinner.hide();
                                console.log(data);
                                var documentObj = JSON.parse(localStorage.getItem("appData")).documents[0].documentObj;
                                var sessionId = data.rqResponse.rqAuthentication.replace("Session:", "");
                                var url = "http://1502.life.mip.co.za/cgi-bin/wspd_113.sh/WService=wsb_815mnt/run.w?run=mmfetch&obj=" + documentObj + "&ctx=" + sessionId + ":",
                                    fileName = "Policy_Doc_Hollard".replace(" ", "_"),
                                    filenameObject = {
                                        "ResourceReference": fileName,
                                        "VersionNumber": "1",
                                        "ContentTypeKey": "pdf"
                                    },
                                    options = {
                                        download: true, //Download PDF to device before viewing
                                        replace: true, //If download == true and replace == true then we will redownload the pdf before viewing, if replace == false we will just open the one on disk
                                        path: 'cdvfile://localhost/persistent/',
                                        spinner: {
                                            title: 'Downloading',
                                            message: 'Fetching document...',
                                            isFixed: true
                                        } // spinner
                                    } // options
                                console.log(url);
                                mipPdf.view(url, filenameObject, options);
                            }, 
                            error: function(e){
                                console.log(e);
                                mipSpinner.hide()
                            }
                        });
                    } // else
                }, // requestSuccess

                requestError: function (d, e, c) {
                    console.log(d);
                    console.log(e);
                    console.log(c);
                    mipAlert.show(e, {
                        title: c
                    });
                }, // requestError

                spinner: {
                    message: "Checking Session...",
                    title: "Download",
                    isFixed: true
                }, // spinner

                availableOfflineFor: {
                    minutes: 120
                } // availableOfflineFor

            }; // restParams1

            mipRest.request(restParams1);

        }, // viewPolicy: function ()
        mailPolicy: function () {
            var restParams1 = {
                service: "mnt",
                parameters: {
                    rqAuthentication: localStorage.getItem("mnt:sessionid"),
                    rqDataMode: "VAR/JSON"
                }, // parameters

                requestSuccess: function (data) {
                    // this will download and view the PDF document
                    // after making sure the user has a valid session to 

                    if (data.rqResponse.rqErrorMessage) {
                        if (data.rqResponse.rqFullError.indexOf('mip_MsgSesErr:6') > 0) {
                            window.app.mobileApp.navigate("components/login/view.html", "slide");
                            localStorage.setItem("nextScreen", "components/policies/view.html");
                            //navigateTo = localStorage.getItem("loginView");
                        } // if (data.rqFullError.indexOf
                        else {
                            navigator.notification.alert(
                                data.rqResponse.rqErrorMessage, // the message
                                function () {}, // a callback
                                "Error", // a title
                                "OK" // the button text
                            ); // navigator.notification.alert 
                        } // else 
                    } //  if (data.rqResponse.rqFullError == "" || data.rqResponse.rqFullError == null)
                    else {
						mipSpinner.show();							
                        var params = {
                                rqAuthentication: "user:roberte|roberte|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                                rqDataMode: "VAR/JSON"
                            } // params
                        $.ajax({
                            url: "http://1502.life.mip.co.za/cgi-bin/wspd_113.sh/WService=wsb_815mnt/rest.w",
                            type: "POST",
                            data: params,
                            success: function (data) {
                                mipSpinner.hide();
                                var documentObj = JSON.parse(localStorage.getItem("appData")).documents[0].documentObj;
                                var sessionId = data.rqResponse.rqAuthentication.replace("Session:", "");
                                var url = "http://1502.life.mip.co.za/cgi-bin/wspd_113.sh/WService=wsb_815mnt/run.w?run=mmfetch&obj=" + documentObj + "&ctx=" + sessionId + ":";
                                var encodedUri = encodeURI(url),
                                    fileName = "policy_doc_2015.pdf",
                                    fullFilePath = 'cdvfile://localhost/persistent/' + fileName;

                                window.resolveLocalFileSystemURL(fullFilePath, attachResource,
                                    function (error) {
                                        transferFile(encodedUri, fullFilePath, attachResource);
                                    }
                                );
                            }
                        });
                    } // else
                }, // requestSuccess

                requestError: function (d, e, c) {
                    console.log(d);
                    console.log(e);
                    console.log(c);
                    mipAlert.show(e, {
                        title: c
                    });
                }, // requestError

                spinner: {
                    message: "Checking Session...",
                    title: "Download",
                    isFixed: true
                }, // spinner

                availableOfflineFor: {
                    minutes: 120
                } // availableOfflineFor

            }; // restParams1

            mipRest.request(restParams1);

        }, // mailPolicy: function ()

    });
    parent.set('policiesModel', policiesModel);
})(app.policies) // (function(parent)
// END_CUSTOM_CODE_policies