'use strict';

var coveredPeople = [];

app.covisionApplication = kendo.observable({
    onShow: function() {
        localStorage.setItem("screen", "components/covisionApplication/view.html");
        $("#residential-address").hide();
        $("residential-address-header").hide();
        if (app.mobileApp.view().id === "components/covisionApplication/step_7.html") {
            // $('#sign').signature();
            $("#debitorderform").hide();
        } // if (app.mobileApp.view().id === "components/covisionApplication/step_7.html")

        if (app.mobileApp.view().id === "components/covisionApplication/final.html") {
            this.model.covisionApplicationModel.set("contractDetails", JSON.parse(localStorage.getItem("newPolicyData")).rqResponse.dsContract.ttContract);
            $('#sign').signature();
        } // if (app.mobileApp.view().id === "components/covisionApplication/final.html")


    }, // onShow: function ()
    afterShow: function() {}, // afterSHow: function() 

    onShowPayment: function() {
        $("#payer-details").hide()
        $("#debitorderform").hide();

        localStorage.setItem("commencementMonth", "");

        var date = new Date(),
            month = date.getMonth() + 1,
            nextMonth = month + 1,
            day = date.getDate(),
            year = date.getFullYear(),
            monthText = "",
            nextMonthText = "";

        switch (month) {
            case 1:
                monthText = "January";
                break;
            case 2:
                monthText = "February";
                break;
            case 3:
                monthText = "March";
                break;
            case 4:
                monthText = "April";
                break;
            case 5:
                monthText = "May";
                break;
            case 6:
                monthText = "June";
                break;
            case 7:
                monthText = "July";
                break;
            case 8:
                monthText = "August";
                break;
            case 9:
                monthText = "September";
                break;
            case 10:
                monthText = "October";
                break;
            case 11:
                monthText = "November";
                break;
            case 12:
                monthText = "December";
                break;
        }

        switch (nextMonth) {
            case 1:
                nextMonthText = "Jan";
                break;
            case 2:
                nextMonthText = "Feb";
                break;
            case 3:
                nextMonthText = "Mar";
                break;
            case 4:
                nextMonthText = "Apr";
                break;
            case 5:
                nextMonthText = "May";
                break;
            case 6:
                nextMonthText = "Jun";
                break;
            case 7:
                nextMonthText = "Jul";
                break;
            case 8:
                nextMonthText = "Aug";
                break;
            case 9:
                nextMonthText = "Sept";
                break;
            case 10:
                nextMonthText = "Oct";
                break;
            case 11:
                nextMonthText = "Nov";
                break;
            case 12:
                nextMonthText = "Dec";
                break;
        }

        console.log(month + " " + monthText);
        console.log(nextMonth + " " + nextMonthText);

        $("#select-commencement-month").empty();
        $("#select-commencement-month").append('<option value="' + month + '">' + monthText + '</option>');
        $("#select-commencement-month").append('<option value="' + nextMonth + '">' + nextMonthText + '</option>');

    },

    afterShowPayment: function() {},

    onShowMainMember: function() {
        console.log(fromQuote)
        if (fromQuote == true) {
            console.log("fromQuote");

            // var data = { "contractQuotation": { "contractProduct": [{ "contractProductID": "1", "campaignCode": "COV003", "productCode": "8040", "coveredParty": [{ "coveredPartyID": "1", "contractProductID": "1", "dateOfBirth": "1992-04-09", "relationship": "Self", "isMainInsured": "true", "coverBenefitCode": "1100", "coverAmount": "50000" }, { "coveredPartyID": 2, "contractProductID": "1", "dateOfBirth": "2011-04-06", "relationship": "child", "isMainInsured": "false", "coverBenefitCode": "1110", "coverAmount": "10000" }, { "coveredPartyID": 3, "contractProductID": "1", "dateOfBirth": "2016-01-01", "relationship": "child", "isMainInsured": "false", "coverBenefitCode": "1110", "coverAmount": "10000" }, { "coveredPartyID": 4, "contractProductID": "1", "dateOfBirth": "1966-12-12", "relationship": "parent", "isMainInsured": "false", "coverBenefitCode": "1130", "coverAmount": "13000" }, { "coveredPartyID": 5, "contractProductID": "1", "dateOfBirth": "1990-12-15", "relationship": "Aunt/Uncle", "isMainInsured": "false", "coverBenefitCode": "1140", "coverAmount": "8000" }] }] } };
            var data = JSON.parse(localStorage.getItem("defaultJSON"));
            console.log(data);

            var idStart = data.contractQuotation.contractProduct[0].coveredParty[0].dateOfBirth.replace(/-/g, "");

            console.log(data.contractQuotation.contractProduct[0].coveredParty[0].dateOfBirth.replace(/-/g, ""))

            $("#main_benefit_amount").val(data.contractQuotation.contractProduct[0].coveredParty[0].coverAmount);
            $("#main_benefit_amount").val();

            $("#main-id-number").val(idStart.toString().substring(2));

            for (var i = 0; i < data.contractQuotation.contractProduct[0].coveredParty.length; i++) {

                if (data.contractQuotation.contractProduct[0].coveredParty[i].relationship.toLowerCase() === 'spouse') {
                    $("#select-main-marital").val('Married');
                    break;
                } // if (data.contractQuotation.contractProduct[0].coveredParty[i]
                else {
                    $("#select-main-marital").val('Single');
                } // else 
            } // for (var i = 0; i < data.contractQuotation.contractProduct[0].coveredParty.length; i++)
        } // if (fromQuote)
    }, // onShowMainMember 

    onShowDepList: function() {
        console.log('onShowDepList');

        var data = [],
            depId;
        $("#currentDependentList").empty();
        console.log("after empty()");


        if (localStorage.getItem("defaultJSON") == "") {
            fromQuote = false;
        } else {
            data = JSON.parse(localStorage.getItem("defaultJSON"));
            fromQuote = true;
            console.log(data);
        }

        if (fromQuote == true) {

            // var data = { "contractQuotation": { "contractProduct": [{ "contractProductID": "1", "campaignCode": "COV003", "productCode": "8040", "coveredParty": [{ "coveredPartyID": "1", "contractProductID": "1", "dateOfBirth": "", "relationship": "Self", "isMainInsured": "true", "coverBenefitCode": "1100", "coverAmount": "" }, { "coveredPartyID": 2, "contractProductID": "1", "dateOfBirth": "2011-04-06", "relationship": "child", "isMainInsured": "false", "coverBenefitCode": "1110", "coverAmount": "10000" }, { "coveredPartyID": 3, "contractProductID": "1", "dateOfBirth": "2016-01-01", "relationship": "child", "isMainInsured": "false", "coverBenefitCode": "1110", "coverAmount": "10000" }, { "coveredPartyID": 4, "contractProductID": "1", "dateOfBirth": "1966-12-12", "relationship": "parent", "isMainInsured": "false", "coverBenefitCode": "1130", "coverAmount": "13000" }, { "coveredPartyID": 5, "contractProductID": "1", "dateOfBirth": "1990-12-15", "relationship": "Aunt/Uncle", "isMainInsured": "false", "coverBenefitCode": "1140", "coverAmount": "8000" }] }] } };

            for (var i = 0; i < data.contractQuotation.contractProduct[0].coveredParty.length; i++) {
                if (data.contractQuotation.contractProduct[0].coveredParty[i].relationship.toLowerCase() !== 'self') {
                    if (dontRun == false) {
                        var newMember = new member();

                        newMember.memberId = newMember.uid;

                        var newMemberCoveredBen = new coveredBenefits();
                        var idStart = data.contractQuotation.contractProduct[0].coveredParty[i].dateOfBirth.replace(/-/g, "");

                        console.log(newMember.id);

                        newMember.title = " ";
                        newMember.firstNames = " ";
                        newMember.lastName = " ";
                        newMember.gender = " ";
                        newMember.roleCode = " ";
                        newMember.dateOfBirth = data.contractQuotation.contractProduct[0].coveredParty[i].dateOfBirth;
                        newMember.idNumber = idStart.substring(2);
                        newMember.relationship = data.contractQuotation.contractProduct[0].coveredParty[i].relationship;
                        newMember.coveredBenefits = [];

                        if (data.contractQuotation.contractProduct[0].coveredParty[i].relationship.toLowerCase() === 'spouse') {
                            newMember.maritalStatus = "Married";
                        } // if (data.contractQuotation.contractProduct[0].
                        else {
                            newMember.maritalStatus = "Single";
                        } // else
                        console.log(newMember);
                        newMemberCoveredBen.benefitAmount = data.contractQuotation.contractProduct[0].coveredParty[i].coverAmount;
                        newMemberCoveredBen.benefitCode = data.contractQuotation.contractProduct[0].coveredParty[i].coverBenefitCode;

                        newMember.coveredBenefits.push(newMemberCoveredBen);

                        coveredPeople.push(newMember);

                    }
                }

            }

        } // if (fromQuote)
        data = coveredPeople;
        console.log("coveredPeople onShow");
        console.log(data);

        if (typeof data === "object") {
            rotateForwards();

            console.log('onShowDepList - if (typeof data === "object":');
            console.log(data);

            $("#currentDependentList").empty();

            if (data === undefined || data === null || data.length == 0) {

                $("#tHeaderDependentList").hide();
                $("#currentDependentList").prepend('<li class="ulistStyleProvider bold" style="border: none"><center><span>No dependents to be listed.</span></center><hr align="center" width="95%" color="#64a3d6"></li>');

            } // if (data === undefined || data === null || data.length == 0)
            else {

                console.log(data.length);

                var depId;

                for (var i = 0; i < data.length; i++) {

                    if (data[i].relationship.toLowerCase() !== "self") {

                        depId = data[i].idNumber;

                        $("#tHeaderDependentList").show();
                        $("#currentDependentList").append("<tr id='" + data[i].memberId + "'><td>" + data[i].relationship + "</td><td>" + depId + "</td><td><a data-role='button' onclick='app.covisionApplication.covisionApplicationModel.removeDependent(" + i + ");'><img src='data/images/icon/remove.png' width='25' height='25'></a></td><td><a data-role='button' onclick='app.covisionApplication.covisionApplicationModel.editDependent(" + JSON.stringify(data[i]) + ");'><img src='data/images/icon/edit.png' width='30' height='30'></a></td></tr>");

                        console.log("<tr id='" + data[i].memberId + "'><td>" + data[i].relationship + "</td><td>" + depId + "</td><td><a data-role='button' onclick='app.covisionApplication.covisionApplicationModel.removeDependent(" + depId + ");'><img src='data/images/icon/remove.png' width='30' height='30'></a></td><td><a data-role='button' onclick='app.covisionApplication.covisionApplicationModel.editDependent(" + JSON.stringify(data[i]) + ");'><img src='data/images/icon/edit.png' width='30' height='30'></a></td></tr>");

                    } // if (data[i].relationship !== "self")

                } // for (var i = 0; i < data.length; i++)

            } // else 

            // TODO: make sure the main member does not display on this list
            // for (var i = 0; i < data.length; i++) {\
            //     if (data[i].relationship === "self") {
            //         delete(data[i]); 
            //     } // if (data[i].relationship === "self")
            // } // for (var i = 0; i < data.length; i++) 

        } // if (typeof JSON.parse
        else {

            console.log("onShowDepList: no object returned from request");
            console.log(data)

            $('#currentDependentList').empty();
            $("#currentDependentList").prepend('<li class="ulistStyleProvider bold" style="border: none"><center><span>Currently no dependents to be listed</span></center><hr align="center" width="95%" color="#64a3d6"></li>');

        } // else

        // kendo.bind($("#currentDependentList"), app.covisionApplication);

    }, // onShowDepList

    afterShowDepList: function() {}, // afterShowDepList

    onShowEasyPay: function() {
        $("#easyPay-ref-num").empty();
    }, // onShowEasyPay: function ()

    afterShowEasyPay: function() {}, // afterShowEasyPay: function ()
    onShowAddDep: function() {

        if (brandNewDependent == false) {
            var depObj = JSON.parse(localStorage.getItem("depData"));

            console.log("::::::::: depObj.id  " + depObj.memberId);

            $("#dep-id-number").val(depObj.idNumber);
            $("#member-id").val(depObj.memberId);
            $("#dep_benefit_amount").val(depObj.coveredBenefits[0].benefitAmount);
            $("#dep-marital").val(depObj.maritalStatus);
            $("#dep-relation").val(depObj.relationship);
        } // if (brandNewDependent == false ) 
        else {
            $("#dep-id-number").val("");
            $("#member-id").val("");
            $("#dep_benefit_amount").val("");
            $("#dep-marital").val("");
            $("#dep-relation").val("");
        } // else 

    },

    afterShowDep: function() {},

}); //app.covisionApplication = kendo.observable

// START_CUSTOM_CODE_covisionApplication
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes

(function(parent) {
    var contractRequestData;
    var covisionApplicationModel = kendo.observable({
        fields: {
            dataDep: [],

            easyPayReference: "",

            depFirstNames: "",
            depLastName: "",
            depIdNumber: "",
            depMaritalStatus: "",
            depRelationship: "",
            depCoverAmount: "",

            /* MAIN INSURED PERSON DETAILS */
            title: "",
            firstNames: "",
            lastName: "",
            idNumber: "",
            maritalStatus: "",
            email: "",
            age: "",
            birthDate: "",
            benefitAmount: "",
            premium: "",
            gendertc: "",
            gender: "",
            totalMonthlyIncome: "",
            cellNumber: "",
            telWork: "",
            telHome: "",
            cellOrPost: "",

            /* CHILD INSURED DETAILS */
            childName: "",
            childSurname: "",
            childInitails: "",
            childTitle: "",
            childIDNumber: "",
            childBenefitAmount: "",
            childBirthDate: "",
            childGendertc: "",
            childGender: "",

            /* PARENT INSURED DETAILS */
            parentName: "",
            parentSurname: "",
            parentInitails: "",
            parentTitle: "",
            parentIDNumber: "",
            parentBirthDate: "",
            parentBenefitAmount: "",
            parentGendertc: "",
            parentGender: "",

            /* SPOUSE INSURED DETAILS */
            spouseName: "",
            spouseSurname: "",
            spouseInitails: "",
            spouseTitle: "",
            spouseIDNumber: "",
            spouseBirthDate: "",
            spouseBenefitAmount: "",
            spouseGendertc: "",
            spouseGender: "",

            /* POSTAL DETAILS */
            postalStreet: "",
            postalSuburb: "",
            postalCity: "",
            postalCode: "",

            /* RESIDENTIAL DETAILS */
            residentialStreet: "",
            residentialSuburb: "",
            residentialCity: "",
            residentialCode: "",

            /* BENEFICIARY DETAILS */
            beneficiaryTitle: "",
            beneficiaryName: "",
            beneficiarySurname: "",
            beneficiaryIDNumber: "",
            beneficiaryBirthDate: "",
            beneficiaryRelation: "",
            beneficiaryContactNo: "",
            beneficiaryGendertc: "",
            beneficiaryGender: "",

            /* PAYMENT DETAILS (How will you pay the total amount) */
            isPayer: true,
            payerName: "",
            payerSurname: "",
            payerMonthlyIncome: "",
            payerCell: "",
            payerTelHome: "",
            payerTelWork: "",
            paymentMethod: "",
            payerSignature: "",

            /* BANKING DETAILS (if paying by debit order) */
            accOwnerTitle: "",
            accOwnerName: "",
            accOwnerSurname: "",
            accOwnerIDNumber: "",
            accOwnerTelWork: "",
            accOwnerTelHome: "",
            accBankName: "",
            accBranchName: "",
            accNumber: "",
            accOwnerCell: "",
            accDebitDate: "",
            accBranchCode: "",
            accType: "",
            accOwnerSignature: "",
            accOwnerSignDate: new Date(),

            /* FINALISING */
            yourSignature: "",
            yourSignDate: new Date(),

            /* POLICY DETAILS */
            polNumber: "",
            planName: "",
            effectiveDate: "",
            paymenAmount: "",
            policyStatus: "",
            productCode: "",
        }, // fields: 

        uploadIDDoc: function() {
            console.log("upLoadIDoc: function ()");

            function uploadId(pressedButton) {
                console.log("function uploadId(pressedButton)");
                console.log(pressedButton);
                if (pressedButton === 1) {
                    mipSpinner.show();
                    navigator.camera.getPicture(onSuccess, onFail, {
                        quality: 50,
                        destinationType: Camera.DestinationType.DATA_URL,
                        sourceType: 1
                    });
                }
            }
            var onSuccess = function(DATA_URL) {
                    //mipSpinner.hide();
                    var blob = new Blob([DATA_URL], {
                        type: 'text/txt'
                    });
                    var contractObj = JSON.parse(localStorage.getItem("newPolicyData")).rqResponse.dsContract.ttContract[0].cContractIdentifier;
                    var contractData = JSON.parse(localStorage.getItem("newPolicyData")).rqResponse.dsContract.ttContract[0];
                    var processLogObj = JSON.parse(localStorage.getItem("workflowLog")).rqResponse.opdProcessLogObj;
                    var mainMember;

                    for (var i = 0; i < contractData.ttContractRole.length; i++) {
                        if (contractData.ttContractRole[i].cPlayerInRoleDesc === "Main Life Assured") {
                            mainMember = contractData.ttContractRole[i].cName;
                        } // if (contractData.ttContractRole[i].cPlayerInRoleDesc === "Main Life Assured") {
                    } // for (var i = 0; i < contractData.ttContractRole.length)

                    var fd = new FormData();
                    fd.append('rqAuthentication', localStorage.getItem('mnt:sessionid') + '|GSMUS|&login_company_branch_obj=91211.878*102594.188');
                    fd.append('rqDataMode', 'VAR/JSON');
                    fd.append('rqService', 'pfUtilityService:linkMultimediaRest');
                    fd.append('ipdProcessLogObj', processLogObj);
                    fd.append('ipcRemNVP', encodeURIComponent('CBMCT=' + contractObj));
                    fd.append('iplcFileObject', DATA_URL);
                    fd.append('ipcFileTemplate', 'client_id');
                    fd.append('ipcFileName', mainMember + '-id_document.jpg');
                    fd.append('ipcFileLabel', 'ID Document - ' + mainMember);
                    console.log("form data : done;");

                    $.ajax({
                        type: "POST",
                        // url: 'https://nrt.hollardlifeadmin.co.za/cgi-bin/wspd_nrt.sh/WService=wsb_rsanrt/rest.w',
                        url: "http://1502.life.mip.co.za/cgi-bin/wspd_116.sh/WService=wsb_881mnt/rest.w",
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            console.log("pfUtilityService:linkMultimediaRest - SUCCESS!");
                            console.log(data);

                            mipSpinner.hide();

                            if (data.rqResponse.rqErrorMessage) {
                                navigator.notification.alert(
                                    data.rqResponse.rqErrorMessage, // the message
                                    function() {}, // a callback
                                    "Error", // a title
                                    "OK" // the button text
                                ); // navigator.notification.alert

                            } // if (data.rqResponse.rqErrorMessage)
                            else {
                                console.log(JSON.stringify(data));

                                navigator.notification.alert(
                                    "Your ID Document has been uploaded successfully.", // the message
                                    function() {}, // a callback
                                    "Success", // a title
                                    "OK" // the button text 
                                ); // navigator.notification.alert
                            } // else
                        }, // success: function (data)

                        error: function(data, error, code) {

                                mipSpinner.hide();

                                mipAlert.show(error, {
                                    title: 'Error'
                                }); // mipAlert.show(error)
                            } // error: function (data, error, code)
                    }); // $.ajax
                } // var onSuccess = function (DATA_URL)

            var onFail = function(e) {

                    mipSpinner.hide();

                    mipAlert.show(e, {
                        title: 'Error'
                    }); // mipAlert.show(e)
                } // var onFail = function (e)
            navigator.notification.confirm(
                'Please take a picture of your ID book now.', // message
                uploadId, // callback to invoke with index of button pressed
                'ID Document', // title
                ['Ok'] // buttonLabels
            ); // navigator.notification.confirm
        }, // uploadIDDoc: function ()

        closeSignModal1: function() {

            var contractObj = JSON.parse(localStorage.getItem("newPolicyData")).rqResponse.dsContract.ttContract[0].cContractIdentifier;
            console.log(contractObj);
            var processLogObj = JSON.parse(localStorage.getItem("workflowLog")).rqResponse.opdProcessLogObj;
            console.log(processLogObj);

            var contractData = JSON.parse(localStorage.getItem("newPolicyData")).rqResponse.dsContract.ttContract[0];
            var mainMember;
            for (var i = 0; i < contractData.ttContractRole.length; i++) {
                if (contractData.ttContractRole[i].cPlayerInRoleDesc === "Main Life Assured") {
                    mainMember = contractData.ttContractRole[i].cName;
                }
            } // for (var i = 0; i < contractData.ttContractRole.length)

            var imagedata = $('#sign').data("mipSignature").signaturePad.toDataURL();

            var fd = new FormData();
            fd.append('rqAuthentication', localStorage.getItem('mnt:sessionid') + '|GSMUS|&login_company_branch_obj=91211.878*102594.188');
            fd.append('rqDataMode', 'VAR/JSON');
            fd.append('rqService', 'pfUtilityService:linkMultimediaRest');
            fd.append('ipdProcessLogObj', processLogObj)
            fd.append('ipcRemNVP', encodeURIComponent('CBMCT=' + contractObj));
            fd.append('iplcFileObject', imagedata.split(',')[1]);
            fd.append('ipcFileTemplate', 'client_signature');
            fd.append('ipcFileName', mainMember + '-signature.png');
            fd.append('ipcFileLabel', 'Signature - ' + mainMember);
            console.log("form data : done;");


            $.ajax({
                type: "POST",
                // url: 'https://nrt.hollardlifeadmin.co.za/cgi-bin/wspd_nrt.sh/WService=wsb_rsanrt/rest.w',
                url: "http://1502.life.mip.co.za/cgi-bin/wspd_116.sh/WService=wsb_881mnt/rest.w",
                data: fd,
                processData: false,
                contentType: false,
                success: function(data) {

                    if (data.rqResponse.rqErrorMessage) {

                        mipSpinner.hide();

                        mipAlert.show(data.rqResponse.rqErrorMessage, {
                            title: 'Error'
                        }); // mipAlert.show(data.rqResponse.rqErrorMessage

                    } //  if (data.rqResponse.rqErrorMessage)
                    else {
                        mipSpinner.hide();
                        navigator.notification.alert(
                            "Success", // the message
                            function() {}, // a callback
                            "Success", // a title
                            "OK" // the button text
                        ); // navigator.notification.alert
                        console.log(data);
                        $('#modalview-signature1').kendoMobileModalView('close');
                        //window.app.mobileApp.navigate("components/login/view.html", "slide");
                    } // else 
                }, // success: function (data)

                error: function(data, error, code) {

                        mipSpinner.hide();

                        mipAlert.show(error, {
                            title: 'Error'
                        }); // mipAlert.,show(error)

                    } // error: function (data, error, code)
            }); // $.ajax
        }, // closeSignModal1: function () 

        addMainMember: function() {

            console.log("addMainMember: function ()");

            var message = "Please ensure that all mandatory fields are filled in",
                options = {
                    title: 'Invalid Fields',
                    buttonText: 'OK',
                    callback: function() {}
                } // var options

            event.preventDefault();
            if (mainMemberFormValidate.validate()) {
                console.log("validation successfull");

                console.log("idNumber : " + covisionApplicationModel.fields.idNumber.toString());
                console.log(getBirthdate(covisionApplicationModel.fields.idNumber.toString()));

                var mainMember = new member();
                mainMember.memberId = mainMember.uid;

                mainMember.firstNames = covisionApplicationModel.fields.firstNames;
                mainMember.lastName = covisionApplicationModel.fields.lastName;
                mainMember.idNumber = covisionApplicationModel.fields.idNumber;
                mainMember.dateOfBirth = (getBirthdate(covisionApplicationModel.fields.idNumber.toString()));
                mainMember.gender = getGender(covisionApplicationModel.fields.idNumber.toString());
                mainMember.maritalStatus = covisionApplicationModel.fields.maritalStatus;
                mainMember.totalMonthlySalary = covisionApplicationModel.fields.totalMonthlySalary;
                mainMember.cellNumber = covisionApplicationModel.fields.cellNumber;
                mainMember.telWork = covisionApplicationModel.fields.telWork;
                mainMember.telHome = covisionApplicationModel.fields.telHome;
                mainMember.roleCode = "lifeA";
                mainMember.relationship = "self";
                mainMember.address = [];
                mainMember.collectionDetail = [];
                mainMember.bankAccount = [];
                mainMember.coveredBenefits = [];

                var mainMemberBenefits = new coveredBenefits();

                mainMemberBenefits.benefitCode = "1100";
                mainMemberBenefits.benefitAmount = $("#main_benefit_amount").val();

                mainMember.coveredBenefits.push(mainMemberBenefits);
                console.log(mainMember);

                coveredPeople[0] = mainMember;

                console.log("coveredPeople");
                console.log(coveredPeople);

                window.app.mobileApp.navigate('components/covisionApplication/step_4.html', 'slide');

            } // if (validator.validate())**
            else {

                console.log("validation unsuccessfull!");
                console.log(mainMemberFormValidate);

                navigator.notification.alert(
                        message, // the message
                        function() {}, // a callback
                        options.title, // a title
                        options.buttonText // the button text
                    ) // navigator.notification.alert
            } // else
        }, // addMainMember: function() 

        addMainAddressDetails: function() {

            console.log("addMainAddressDetails: function ()");

            var message = "Please ensure that all mandatory fields are filled in",
                options = {
                    title: 'Invalid Fields',
                    buttonText: 'OK',
                    callback: function() {}
                } // var options

            console.log(switchVar.val());

            var postalAddress = new address();
            postalAddress.type = "postal";
            postalAddress.street = covisionApplicationModel.fields.postalStreet;
            postalAddress.suburb = covisionApplicationModel.fields.postalSuburb;
            postalAddress.city = covisionApplicationModel.fields.postalCity;
            postalAddress.code = covisionApplicationModel.fields.postalCode;

            if (switchVar.val() == 'on') {
                covisionApplicationModel.fields.residentialStreet = covisionApplicationModel.fields.postalStreet;
                covisionApplicationModel.fields.residentialSuburb = covisionApplicationModel.fields.postalSuburb;
                covisionApplicationModel.fields.residentialCity = covisionApplicationModel.fields.postalCity;
                covisionApplicationModel.fields.residentialCode = covisionApplicationModel.fields.postalCode;

                var phyAddress = new address();

                phyAddress.type = "physical";
                phyAddress.street = covisionApplicationModel.fields.residentialStreet;
                phyAddress.suburb = covisionApplicationModel.fields.residentialSuburb;
                phyAddress.city = covisionApplicationModel.fields.residentialCity;
                phyAddress.code = covisionApplicationModel.fields.residentialCode;

            } // if (switchVar.val()
            else {

                var phyAddress = new address();

                phyAddress.type = "physical";
                phyAddress.street = covisionApplicationModel.fields.residentialStreet;
                phyAddress.suburb = covisionApplicationModel.fields.residentialSuburb;
                phyAddress.city = covisionApplicationModel.fields.residentialCity;
                phyAddress.code = covisionApplicationModel.fields.residentialCode;

            } // else 

            for (var i = 0; i < coveredPeople.length; i++) {
                if (coveredPeople[i].relationship === "self") {
                    console.log("::::::::::;  Adding address details for main life ");

                    coveredPeople[i].address.push(postalAddress);
                    coveredPeople[i].address.push(phyAddress);

                    break;
                } // if (coveredPeople[i].relationship === "self")

            } // for (var i = 0l i < coveredPeople.length; i++)

            window.app.mobileApp.navigate('components/covisionApplication/step_5.html', 'slide');


        }, // addMainAddressDetails

        addBeneficiaryDetails: function() {
            console.log(":::::::::::: Add Beneficiary Details.");
            var message = "Please ensure that all mandatory fields are filled in",
                options = {
                    title: 'Invalid Fields',
                    buttonText: 'OK',
                    callback: function() {}
                } // var options

            event.preventDefault();
            covisionApplicationModel.fields.beneficiaryRelation = $("#select-bene-relation").val();


            if (beneficiaryValidate.validate()) {
                var beneficiary = new member();

                beneficiary.memberId = beneficiary.uid;
                beneficiary.title = covisionApplicationModel.fields.beneficiaryTitle;
                beneficiary.firstNames = covisionApplicationModel.fields.beneficiaryName;
                beneficiary.lastName = covisionApplicationModel.fields.beneficiarySurname;
                beneficiary.idNumber = covisionApplicationModel.fields.beneficiaryIDNumber;
                beneficiary.gender = getGender(covisionApplicationModel.fields.beneficiaryIDNumber.toString());
                beneficiary.dateOfBirth = getBirthdate(covisionApplicationModel.fields.beneficiaryIDNumber.toString());
                beneficiary.roleCode = "Bene";
                beneficiary.relationship = covisionApplicationModel.fields.beneficiaryRelation;

                var index = coveredPeople.length;

                coveredPeople.push(beneficiary);

            } else {
                navigator.notification.alert(
                        message, // the message
                        function() {}, // a callback
                        options.title, // a title
                        options.buttonText // the button text
                    ) // navigator.notification.alert
            }

            console.log("coveredPeople");
            console.log(coveredPeople);

        }, // addBeneficiaryDetails: function ()

        addBeneficiary: function() {

            console.log("addBeneficiary: function ()");

            app.covisionApplication.covisionApplicationModel.addBeneficiaryDetails();

            console.log("coveredPeople");
            console.log(coveredPeople);

            window.app.mobileApp.navigate("components/covisionApplication/step_7.html", "slide");
        }, // addBeneficiary: function ()

        addPaymentDetails: function() {

            console.log("addPaymentDetails: function ()");
            localStorage.setItem("commencementMonth", $("#select-commencement-month").val());
            console.log("isPayer2 : " + covisionApplicationModel.fields.isPayer);
            if (covisionApplicationModel.fields.isPayer) {
                // add bank and payment details to the main member object
                console.log("isPayer : " + covisionApplicationModel.fields.isPayer);

                for (var i = 0; i < coveredPeople.length; i++) {
                    if (coveredPeople[i].relationship === "self") {
                        console.log(covisionApplicationModel.fields.paymentMethod);

                        var mainCollectionDetail = new collectionDetail();

                        if (covisionApplicationModel.fields.paymentMethod === "debitOrder") {
                            var mainBankAccount = new bankAccount();

                            // add banking details 
                            mainBankAccount.accountType = covisionApplicationModel.fields.accType;
                            mainBankAccount.bank = covisionApplicationModel.fields.accBankName;
                            mainBankAccount.branchName = covisionApplicationModel.fields.accBranchName;
                            mainBankAccount.branchCode = covisionApplicationModel.fields.accBranchCode;
                            mainBankAccount.accountNumber = covisionApplicationModel.fields.accNumber;

                            // push banking details into the object 
                            coveredPeople[i].bankAccount.push(mainBankAccount);

                        } // if (covisionApplicationModel.fields.paymentMethod === "debitOrder")
                        else {
                            covisionApplicationModel.fields.paymentMethod = "cash";

                        } // else 

                        // add collection details
                        mainCollectionDetail.collectionMethod = covisionApplicationModel.fields.paymentMethod;
                        mainCollectionDetail.collectionDate = covisionApplicationModel.fields.collectionDate;

                        // Push collection details into the object 
                        coveredPeople[i].collectionDetail.push(mainCollectionDetail);

                        // exit the loop once the main member has been found
                        break;

                    } // if (coveredPeople[i].relationship === "self")

                } // for (var i = 0; i < coveredPeople.length; i++)

                console.log("coveredPeople");
                console.log(coveredPeople);

            } // if (covisionApplicationModel.fields.isPayer) 
            else {

                // add new member object(payer) to the data model
                var payer = new member();
                payer.memberId = payer.uid;
                payer.firstNames = covisionApplicationModel.fields.payerName;
                payer.lastName = covisionApplicationModel.fields.payerSurname;
                payer.idNumber = covisionApplicationModel.fields.payerIDNumber
                payer.totalMonthlySalary = covisionApplicationModel.fields.payerMonthlyIncome;
                payer.cellNumber = covisionApplicationModel.fields.payerCell;
                payer.telWork = covisionApplicationModel.fields.payerTelWork;
                payer.telHome = covisionApplicationModel.fields.payerTelHome;
                payer.roleCode = "policyP";
                payer.relationship = covisionApplicationModel.fields.payerRelation;
                payer.collectionDetail = [];
                payer.bankAccount = [];

                var payerBankAccount = new bankAccount(),
                    payerCollectionDetail = new collectionDetail(),
                    i = coveredPeople.length;
                console.log(covisionApplicationModel.fields.paymentMethod);
                if (covisionApplicationModel.fields.paymentMethod === "debitOrder") {
                    for (var i = 0; i < coveredPeople.length; i++) {
                        if (coveredPeople[i].relationship.toLowerCase() === "policyp") {
                            // add banking details 
                            payerBankAccount.accountType = covisionApplicationModel.fields.accType;
                            payerBankAccount.bank = covisionApplicationModel.fields.accBankName;
                            payerBankAccount.branchName = covisionApplicationModel.fields.accBranchName;
                            payerBankAccount.branchCode = covisionApplicationModel.fields.accBranchCode;
                            payerBankAccount.accountNumber = covisionApplicationModel.fields.accNumber;

                            // push banking details into the object 
                            coveredPeople[i].bankAccount.push(payerBankAccount);

                            // add collection details
                            payerCollectionDetail.collectionMethod = covisionApplicationModel.fields.paymentMethod;
                            payerCollectionDetail.collectionDate = covisionApplicationModel.fields.collectionDate;

                            // Push collection details into the object 
                            coveredPeople[i].collectionDetail.push(payerCollectionDetail);

                        } // if (coveredPeople[i].relationship.toLowerCase() === 'policyp')

                    } // for (var i = 0; i < coveredPeople.length; i++)
                } // if (covisionApplicationModel.fields.paymentMethod === "debitOrder")
                else {

                    // assign payment method to cash regardless of it being easyPay or not
                    covisionApplicationModel.fields.paymentMethod = "cash";

                } // else 


            } // else 

            if (typeof JSON.parse(localStorage.getItem("agentData")).rqResponse.dsProducer === "object") {
                var agentData = new member();
                // agentData.memberId = agentData.uid;
                agentData.memberId = "3901e385-2cfd-4f07-a9a2-7f7838e3b338";
                // agentData.firstNames = JSON.parse(localStorage.getItem("agentData")).rqResponse.dsProducer.ttProducer[0].cProducerCode;
                agentData.firstNames = "ROYALS-1754-BAH094";
                agentData.roleCode = "agent";

                coveredPeople.push(agentData);

            } // if (typeof JSON.parse(localStorage.getItem("agentData")))

            app.covisionApplication.covisionApplicationModel.generateRequestXML();

        }, // addPaymentDetails: function ()

        generateRequestXML: function() {

            // TODO: form-level validation to be added in
            // TODO: do the request for new business here

            console.log("generateRequestXML: function ()");
            console.log(coveredPeople);
            console.log(covisionApplicationModel);

            var accordXML = createAccordXML(coveredPeople);
            // accordXML = '<TXLife><UserAuthRequest/><TXLifeRequest PrimaryObjectID="new_Policy"><TransRefGUID>adf6e647-5923-423b-8157-8445206dbe21</TransRefGUID><TransType tc="103">NewPolicy</TransType><InquiryLevel tc="1">ObjectOnly</InquiryLevel><OLifE><Party id="Person_self_9112075215081" tc="1" DataRep="Full"><PartyTypeCode tc="1">Person</PartyTypeCode><FullName>Hdud Hdhdd</FullName><ResidenceCountry>ZAF</ResidenceCountry><IDReferenceNo>9112075215081</IDReferenceNo><IDReferenceType tc="4">NationalIdentityNumber</IDReferenceType><Address id="StreeAddress_0_self_9112075215081" DataRep="Partial"><AddressTypeCode tc="17">Mailing</AddressTypeCode><Line1>43</Line1><Line3>Usudyd</Line3><Line4>Jfhfhhf</Line4><City>Bxb</City><AddressState>undefined</AddressState><Zip>1234</Zip><AddressCountry>SouthAfrica</AddressCountry></Address><Address id="StreeAddress_1_self_9112075215081" DataRep="Partial"><AddressTypeCode tc="1">Residential</AddressTypeCode><Line1>43</Line1><Line3>Usudyd</Line3><Line4>Jfhfhhf</Line4><City>Bxb</City><AddressState>undefined</AddressState><Zip>1234</Zip><AddressCountry>SouthAfrica</AddressCountry></Address><Phone id="Phone3_0" DataRep="Partial"><PhoneTypeCode tc="12">Mobile</PhoneTypeCode><DialNumber>0720866181</DialNumber></Phone><Client><PrefLanguage tc="9">English</PrefLanguage></Client><Person id="Person_self_9112075215081" DataRep="Full"><FirstName>Hdud</FirstName><Initials>H</Initials><LastName>Hdhdd</LastName><MarStat tc="2">Single</MarStat><Gender><tc>1</tc><Gender>male</Gender></Gender><BirthDate>1991-12-07</BirthDate><Prefix>Mr</Prefix></Person> </Party><Party id="Person_spouse_9312311395081" tc="1" DataRep="Full"><PartyTypeCode tc="1">Person</PartyTypeCode><FullName>Spouse Johnson</FullName><ResidenceCountry>ZAF</ResidenceCountry><IDReferenceNo>9312311395081</IDReferenceNo><IDReferenceType tc="4">NationalIdentityNumber</IDReferenceType><Client><PrefLanguage tc="9">English</PrefLanguage></Client><Person id="Person_spouse_9312311395081" DataRep="Full"><FirstName>Spouse</FirstName><Initials>S</Initials><LastName>Johnson</LastName><MarStat tc="2">Single</MarStat><Gender><tc>2</tc><Gender>female</Gender></Gender><BirthDate>1993-12-31</BirthDate><Prefix>Ms</Prefix></Person> </Party><Party id="Person_Brother_9412013834088" tc="1" DataRep="Full"><PartyTypeCode tc="1">Person</PartyTypeCode><FullName>Bene Johnson </FullName><ResidenceCountry>ZAF</ResidenceCountry><IDReferenceNo>9412013834088</IDReferenceNo><IDReferenceType tc="4">NationalIdentityNumber</IDReferenceType><Client><PrefLanguage tc="9">English</PrefLanguage></Client><Person id="Person_Brother_9412013834088" DataRep="Full"><FirstName>Bene</FirstName><Initials>B</Initials><LastName>Johnson </LastName><MarStat tc="2">Single</MarStat><Gender><tc>2</tc><Gender>female</Gender></Gender><BirthDate>1994-12-01</BirthDate><Prefix>Mrs</Prefix></Person> </Party><Campaign id="Campaign_"><CampaignCode>COV003</CampaignCode></Campaign><Holding id="Holding_1"><HoldingTypeCode tc="2">Policy</HoldingTypeCode><HoldingForm tc="1">Individual</HoldingForm><Policy id="Policy_1" ProductID="" CarrierPartyID="" BankID=""><LineOfBusiness tc="1">Life</LineOfBusiness><PolicyStatus tc="57">Quoted</PolicyStatus><ApplicationInfo><ApplicationType tc="1">New Application</ApplicationType></ApplicationInfo><ProductCode>8040</ProductCode><EffDate>2016-09-01</EffDate><PaymentMode tc="4">MonthOrMonthly</PaymentMode><PaymentMethod tc="1045300022">cash</PaymentMethod><PaymentDraftDay>25</PaymentDraftDay><Life id="Life_LifeProductSeriesIndividualFuneralBenefit"><Coverage id="self_cover_9112075215081"><ProductCode>1100</ProductCode><OLifEExtension VendorCode="453" ExtensionCode="CoverageExtension"> <CoverageExtension><PackageCode>50000</PackageCode>   </CoverageExtension></OLifEExtension><EffDate>2016-09-01</EffDate><LifeParticipant id="Participant_self_9112075215081" PartyID="Person_self_9112075215081"><LifeParticipantRoleCode tc="91">self</LifeParticipantRoleCode></LifeParticipant></Coverage><Coverage id="family_cover"><ProductCode>1110</ProductCode><OLifEExtension VendorCode="453" ExtensionCode="CoverageExtension"><CoverageExtension><PackageCode>40000</PackageCode></CoverageExtension></OLifEExtension><EffDate>2016-09-01</EffDate><EffDate>2016-09-01</EffDate><LifeParticipant id="Participant_spouse_9312311395081" PartyID="Person_spouse_9312311395081"><LifeParticipantRoleCode tc="1">spouse</LifeParticipantRoleCode></LifeParticipant></Coverage></Life></Policy></Holding><Relation id="Relation_Insured" OriginatingObjectID="holding_1" RelatedObjectID="Person_self_9112075215081"><StartDate>2016-05-01</StartDate><OriginatingObjectType tc="4">Holding</OriginatingObjectType><RelatedObjectType tc="6">Party</RelatedObjectType><RelationRoleCode tc="32">Insured</RelationRoleCode></Relation><Relation id="Relation_Owner" OriginatingObjectID="holding_1" RelatedObjectID="Person_self_9112075215081"><StartDate>2016-05-01</StartDate><OriginatingObjectType tc="4">Holding</OriginatingObjectType><RelatedObjectType tc="6">Party</RelatedObjectType><RelationRoleCode tc="8">Owner</RelationRoleCode><InterestPercent>100</InterestPercent></Relation><Relation id="Relation_Payer" OriginatingObjectID="holding_1" RelatedObjectID="Person_self_9112075215081"><StartDate>2016-05-01</StartDate><OriginatingObjectType tc="4">Holding</OriginatingObjectType><RelatedObjectType tc="6">Party</RelatedObjectType><RelationRoleCode tc="31">PolicyPayer</RelationRoleCode></Relation><Relation id="Relation_spouse_9312311395081" OriginatingObjectID="Person_self_9112075215081" RelatedObjectID="Person_spouse_9312311395081"><StartDate>2016-05-01</StartDate><OriginatingObjectType tc="6">Party</OriginatingObjectType><RelatedObjectType tc="6">Party</RelatedObjectType><RelationRoleCode tc="1">Spouse</RelationRoleCode></Relation><Relation id="Relation_Brother_9412013834088" OriginatingObjectID="Person_self_9112075215081" RelatedObjectID="Person_Brother_9412013834088"><StartDate>2016-05-01</StartDate><OriginatingObjectType tc="4">Holding</OriginatingObjectType><RelatedObjectType tc="6">Party</RelatedObjectType><RelationRoleCode tc="34">Benefactor</RelationRoleCode><InterestPercent>100</InterestPercent></Relation></OLifE></TXLifeRequest></TXLife>'

            console.log(accordXML);

            var fd = new FormData();
            fd.append('rqAuthentication', "user:hollardservice|mip123" + '|GSMUS|&login_company_branch_obj=91211.878*102594.188');
            fd.append('rqDataMode', 'VAR/XML');
            fd.append('rqService', 'AcordHoldingService:PolicyNewBusiness');
            fd.append('dsHoldingMaintenanceRequest', accordXML);

            mipSpinner.show();

            $.ajax({
                type: "POST",
                // url: 'https://nrt.hollardlifeadmin.co.za/cgi-bin/wspd_nrt.sh/WService=wsb_rsanrt/rest.w?',
                url: "http://1502.life.mip.co.za/cgi-bin/wspd_116.sh/WService=wsb_881mnt/rest.w",
                data: fd,
                processData: false,
                contentType: false,
                success: function(data) {
                    //var systemErrorXML = $.parseXML("<rqResponse><rqAuthentication>Session:GSTSS|9578024</rqAuthentication><rqErrorMessage>Error reading XML from a MEMPTR or LONGCHAR. (13036)</rqErrorMessage><rqFullError><![CDATA[Type: <Unknown> (PROGRESS:13036)Error reading XML from a MEMPTR or LONGCHAR. (13036)Origin: mapRequestAndMakeCall rest.w,rest.w|run-web-object web/objects/stateaware.p|run-web-object web/objects/web-util.p|SYSTEM-TRIGGER /apps/dynalife/nrt/miprunlib/web/objects/webdisp.p|/apps/dynalife/nrt/miprunlib/web/objects/web-disp.p]]></rqFullError><rqWarningMessage>The following parameters were provided but are not valid for this service call, please check the spelling of the parameter names: dsHoldingMaintenanceRequest</rqWarningMessage></rqResponse>"),
                    //    serviceErrorXML = $.parseXML('<rqResponse><rqAuthentication>Session:GSTSS|9577877</rqAuthentication><dsHoldingMaintenanceResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><UserAuthResponse><SvrDate>2016-08-16</SvrDate><SvrTime>10:33:48.293+02:00</SvrTime><UserSessionKey>9577877</UserSessionKey><UserSessionKeyExpDate>2016-08-16</UserSessionKeyExpDate><UserSessionKeyExpTime>10:33:48.293+02:00</UserSessionKeyExpTime><TransResult><ResultCode tc="1">SUCCESS</ResultCode></TransResult></UserAuthResponse><TXLifeResponse PrimaryObjectID="Holding_1"><TransRefGUID>ERROR</TransRefGUID><TransExeDate>2016-08-16</TransExeDate><TransExeTime>10:33:48.293+02:00</TransExeTime><TransType tc=""/><TransResult><ResultCode tc="5">FAILURE</ResultCode><ResultInfo id="ResultInfo_1"><ResultInfoDesc>Please provide a valid &lt;TransRefGUID&gt;.</ResultInfoDesc><ResultInfoCode tc="200">General Data Error</ResultInfoCode></ResultInfo><ResultInfo id="ResultInfo_1"><ResultInfoDesc>Please provide a valid &lt;TransRefGUID&gt;.</ResultInfoDesc><ResultInfoCode tc="200">General Data Error</ResultInfoCode></ResultInfo></TransResult><OLifE></OLifE></TXLifeResponse></dsHoldingMaintenanceResponse></rqResponse>');

                    //data = systemErrorXML;
                    //data = serviceErrorXML;

                    // rqError Handling in xml
                    console.log("This")
                    console.log(data);
                    console.log(typeof data.getElementsByTagName('rqErrorMessage'));
                    console.log(typeof data.getElementsByTagName('ResultInfoDesc'));

                    var serviceError = data.getElementsByTagName('ResultInfoDesc');
                    var systemError = data.getElementsByTagName('rqErrorMessage');
                    var messages = "";

                    mipSpinner.hide();
                    console.log(data.getElementsByTagName('ResultCode'));
                    if (systemError.length != 0) {
                        mipAlert.show(systemError[0].childNodes[0].nodeValue, {
                            title: 'Error'
                        }); // mipAlert.show(systemError[0].childNodes[0].nodeValue
                    } // if (systemError.length != 0)
                    else if (serviceError.length != 0) {
                        for (var i = 0; i < serviceError.length; i++) {
                            messages = messages + data.getElementsByTagName('ResultInfoDesc')[i].childNodes[0].nodeValue + "\n";
                        } // for ( var i = 0; i < serviceError.length; i++)

                        mipAlert.show('Error: \n' + messages, {
                            title: 'Error'
                        }); // mipAlert.show()
                    } // if (serviceError.length != 0)
                    else {
                        // the rest comes here
                        console.log("AcordHoldingService:PolicyNewBusiness - SUCCESS!");
                        console.log(data);

                        if (data.getElementsByTagName("Policy").length != 0) {
                            var policies = data.getElementsByTagName("Policy");

                            console.log(policies);

                            if (policies === undefined || policies === "" || policies == []) {

                                messages = "";

                                for (var i = 0; i < data.getElementsByTagName("ResultInfoDesc").length; i++) {
                                    messages = messages + data.getElementsByTagName('ResultInfoDesc')[i].childNodes[0].nodeValue + "\n"
                                }
                                mipAlert("Creation of the policy has failed: " + data.getElementsByTagName("ResultInfoDesc")[0].childNodes);

                            } // if (policies === undefined || policies === "") 
                            else {
                                var contractNumber = policies[0].getElementsByTagName("PolNumber")[0].childNodes[0].nodeValue;

                                console.log(contractNumber);

                                var restParams1 = {
                                    service: "mnt",
                                    parameters: {
                                        // rqAuthentication: "user:admin|mip123" + "|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                                        rqAuthentication: "user:hollardservice|mip123" + "|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                                        rqService: "ContractService:fetchContractData",
                                        rqDataMode: "VAR/JSON",
                                        ipcContractNumber: contractNumber
                                    }, // parameters

                                    requestSuccess: function(data) {

                                        console.log("ContractService:fetchContractData - SUCCESS!");
                                        console.log(data);

                                        localStorage.setItem("newPolicyData", JSON.stringify(data));

                                        var ttContract = data.rqResponse.dsContract.ttContract[0];

                                        covisionApplicationModel.fields.planName = JSON.stringify(ttContract.ttContractProduct[0].cProductName);
                                        covisionApplicationModel.fields.effectiveDate = JSON.stringify(ttContract.dtCommencementDate);
                                        covisionApplicationModel.fields.paymenAmount = JSON.stringify(ttContract.ttContractCollectionDetails[0].dPremium);
                                        covisionApplicationModel.fields.policyStatus = JSON.stringify(ttContract.ttContractStatus[0].cContractStatus);
                                        covisionApplicationModel.fields.productCode = JSON.stringify(ttContract.ttContractProduct[0].cCode);

                                        /* ========================================================== */
                                        /* == Fire of workflow process == */

                                        // find main Contract Role player
                                        var applicantName,
                                            appliedDate,
                                            applicantEmailAddress,
                                            applicantPremium,
                                            applicantContactNumber,
                                            brokerName = "kennethc",
                                            brokerContactNo = "0720866181",
                                            brokerEmail = "ralwinnj@mip.co.za";

                                        for (var i = 0; i < ttContract.ttContractRole.length; i++) {
                                            if (ttContract.ttContractRole[i].cPlayerInRoleDesc === 'Main Life Assured') {

                                                applicantName = ttContract.ttContractRole[i].cFirstName + " " + ttContract.ttContractRole[i].cLastName;
                                                appliedDate = new Date();
                                                applicantEmailAddress = "ralwinnj@mip.co.za";
                                                applicantPremium = ttContract.ttContractCollectionDetails[0].dPremium;
                                                applicantContactNumber = "0634336931";
                                                break;

                                            } // if (policies === undefined || policies === "")
                                        }

                                        // console.log(applicantName + " -- " + appliedDate);
                                        var ipcRemNVP = 'CBMCT=' + ttContract.cContractIdentifier;
                                        var ipcRefNVP = 'pf_ApplicantName=' + applicantName + ',pf_AppliedDate=' + appliedDate + ',pf_ApplicantEmailAddress=' + applicantEmailAddress + ',pf_ApplicantPremium=' + applicantPremium + ',pf_ApplicantContactNumber=' + applicantContactNumber + ',pf_BrokerName=' + brokerName + ',pf_BrokerCont=' + brokerContactNo + ',pf_BrokerEmail=' + brokerEmail;

                                        //window.dataJSON = ttContract;
                                        var params2 = {
                                            service: "mnt",
                                            parameters: {
                                                // rqAuthentication: 'user:admin|mip123|GSMUS|&login_company_branch_obj=91211.878*102594.188',
                                                rqAuthentication: 'user:hollardservice|mip123|GSMUS|&login_company_branch_obj=91211.878*102594.188',
                                                rqDataMode: "VAR/JSON",
                                                // rqService: 'pfUtilityService:launchRestWFProcess',
                                                // ipcProcessCode: 'wf_Mob_New_Bus_Test',
                                                // ipcRemNVP: ipcRemNVP,
                                                // ipcRefNVP: ipcRefNVP
                                            }, // parameters

                                            requestSuccess: function(data) {

                                                console.log(params2.parameters.rqService + " - SUCCESS!");
                                                console.log(data);

                                                mipSpinner.hide();

                                                if (data.rqResponse.rqErrorMessage) {
                                                    navigator.notification.alert(
                                                        data.rqResponse.rqErrorMessage, // the message
                                                        function() {}, // a callback
                                                        "Error", // a title
                                                        "OK" // the button text
                                                    ); // navigator.notification.alert

                                                } // if (data.rqResponse.rqErrorMessage)
                                                else {

                                                    console.log(params2.parameters.rqService + " - SUCCESS!");
                                                    console.log(JSON.stringify(data));

                                                    localStorage.setItem("workflowLog", JSON.stringify(data));

                                                    window.app.mobileApp.navigate('components/covisionApplication/final.html', 'slide');
                                                } // else
                                            }, // requestSuccess: function ()

                                            requestError: function(d, e, c) {

                                                localStorage.setItem("mnt:sessionid", "");
                                                localStorage.setItem("appData", "")

                                                mipAlert.show(e, {
                                                    title: c
                                                });
                                            }, // requestError

                                            spinner: {
                                                message: "Authenticating",
                                                title: "",
                                                isFixed: true
                                            }, // spinner

                                            availableOfflineFor: {
                                                minutes: 120
                                            } // availableOfflineFor
                                        }; // params

                                        mipRest.request(params2);

                                    }, // requestSuccess

                                    requestError: function(d, e, c) {

                                        localStorage.setItem("mnt:sessionid", "");
                                        localStorage.setItem("appData", "")

                                        mipAlert.show(e, {
                                            title: c
                                        }); // mipAlert.show(e)

                                    }, // requestError

                                    spinner: {
                                        message: "Authenticating",
                                        title: "",
                                        isFixed: true
                                    }, // spinner

                                    availableOfflineFor: {
                                        minutes: 120
                                    } // availableOfflineFor

                                }; // restParams1

                                mipRest.request(restParams1);

                            } // else 

                        } // if (typeof data.getElementsByTagName("Policy") === 'object')

                    } // else

                }, // success: function (data)

                error: function(data, error, code) {

                        mipSpinner.hide();

                        mipAlert.show(error, {
                            title: 'Error'
                        }); // mipAlert.show(error)
                    } // error: function (data, error, code)
            }); // $.ajax
        }, // generateRequestXML: function ()

        removeDependent: function(index) {

            console.log("removeDependent: function (index)");

            var i = parseInt(index);

            console.log(coveredPeople[i]);

            $("#" + coveredPeople[i].memberId).remove();


            coveredPeople.splice(i, 1);
            // delete(coveredPeople[i]);

        }, // removeDependent: function ()

        gotoDependentAdd: function() {

            console.log("gotoDependentAdd: function ()");

            brandNewDependent = true; // false by default

            covisionApplicationModel.fields.depIdNumber = "";
            covisionApplicationModel.fields.depFirstNames = "";
            covisionApplicationModel.fields.depLastName = "";
            covisionApplicationModel.fields.depMaritalStatus = "";
            covisionApplicationModel.fields.depRelationship = "";

            window.app.mobileApp.navigate("components/covisionApplication/add_dependent.html", "slide");

        }, // gotoDependentAdd: function ()

        goBackDependentList: function() {

            console.log("goBackDependentList: function ()");
            console.log("Clear the fields used on the form");

            covisionApplicationModel.fields.depIdNumber = "";
            covisionApplicationModel.fields.depFirstNames = "";
            covisionApplicationModel.fields.depLastName = "";
            covisionApplicationModel.fields.depMaritalStatus = "";
            covisionApplicationModel.fields.depRelationship = "";

            dontRun = true;
            brandNewDependent = false;

            console.log("goBackDependentList");
            console.log(coveredPeople);

            window.app.mobileApp.navigate("components/covisionApplication/step_5.html", "slide:left reverse");

        }, // goBackDependentList 

        addNewDependent: function() {

            var message = "Please ensure that all mandatory fields are filled in",
                options = {
                    title: 'Invalid Fields',
                    buttonText: 'OK',
                    callback: function() {}
                } // var options

            event.preventDefault();
            if (addDependentFormValidate.validate()) {
                console.log("validation successfull");
                console.log("addNewDependent: funtion ()");
                console.log(coveredPeople);

                covisionApplicationModel.fields.depRelationship = $("#dep-relation").val();
                covisionApplicationModel.fields.depIdNumber = $("#dep-id-number").val();
                covisionApplicationModel.fields.depMaritalStatus = $("#dep-marital").val();

                // add the new dependent to the json object
                if (covisionApplicationModel.fields.depRelationship === 'spouse' || covisionApplicationModel.fields.depRelationship === 'child') {
                    covisionApplicationModel.fields.famCoverAmount = $("#dep_benefit_amount").val();

                } // if (covisionApplicationModel.fields.depRelationship === 'spouse
                else {
                    covisionApplicationModel.fields.depCoverAmount = $("#dep_benefit_amount").val();

                } // els e

                covisionApplicationModel.fields.depCoverAmount = $("#dep_benefit_amount").val();

                console.log("pushDependent: function");
                console.log(coveredPeople);
                console.log(covisionApplicationModel);
                console.log($("#" + $("#dep-id-number").val().substring(0, 6)));

                var uid;

                if (brandNewDependent == true) {
                    uid = "";

                } else {
                    uid = $("#member-id").val();
                }

                console.log(uid);

                for (var i = 0; i < coveredPeople.length; i++) {
                    if (uid == coveredPeople[i].uid) {
                        console.log("success");
                        var dependent = coveredPeople[i];
                        break;
                    } else {
                        console.log("failure!");
                        var dependent = new member();
                        dependent.memberId = dependent.uid;
                    }
                }

                if (coveredPeople.length == 0) {
                    var dependent = new member()
                }

                var roleCode = "lifea",
                    benefitCode = "",
                    depId;

                // default to a marital status of 'single' if relation is 'child'
                if (covisionApplicationModel.fields.depRelationship.toLowerCase() === 'child') {
                    covisionApplicationModel.fields.depMaritalStatus = "single";

                } // if (covisionApplicationModel.fields.depRelationship.toLowerCase() === 'child')

                // assign values to the dataModel

                dependent.memberId = dependent.uid;
                dependent.title = "";
                dependent.firstNames = covisionApplicationModel.fields.depFirstNames;
                dependent.lastName = covisionApplicationModel.fields.depLastName;
                dependent.idNumber = covisionApplicationModel.fields.depIdNumber.toString();
                dependent.maritalStatus = covisionApplicationModel.fields.depMaritalStatus;
                dependent.gender = getGender(covisionApplicationModel.fields.depIdNumber.toString());
                dependent.dateOfBirth = getBirthdate(covisionApplicationModel.fields.depIdNumber.toString());
                dependent.roleCode = roleCode;
                dependent.relationship = covisionApplicationModel.fields.depRelationship;
                dependent.coveredBenefits = [];

                var dependentBenefits = new coveredBenefits();

                console.log(":::::::::: depe");
                console.log(dependent);

                // determine benefitCode
                switch (covisionApplicationModel.fields.depRelationship) {
                    case "spouse":
                        benefitCode = "1110";
                        break;
                    case "child":
                        benefitCode = "1110";
                        break;
                    case "parent":
                        benefitCode = "1130";
                        break;
                    case "aunt":
                        benefitCode = "1140";
                        break;
                    case "uncle":
                        benefitCode = "1140";
                        break;
                    case "nephew":
                        benefitCode = "1140";
                        break;
                    case "niece":
                        benefitCode = "1140";
                        break;
                    case "adult child":
                        benefitCode = "1140";
                        break;
                    case "sister":
                        benefitCode = "1140";
                        break;
                    case "brother":
                        benefitCode = "1140";
                        break;
                    case "additionalPartner":
                        benefitCode = "1140";
                        break;
                }

                // assign benefitCode  
                console.log("benefitCode : " + benefitCode);

                dependentBenefits.benefitCode = benefitCode;
                dependentBenefits.benefitAmount = covisionApplicationModel.fields.depCoverAmount;

                // push benefit details to dataModel
                dependent.coveredBenefits.push(dependentBenefits);

                if (dependent.memberId != uid) {
                    coveredPeople.push(dependent);
                }

                //write results to the log
                console.log("index");
                console.log(coveredPeople);

                covisionApplicationModel.fields.depIdNumber = "";
                covisionApplicationModel.fields.depFirstNames = "";
                covisionApplicationModel.fields.depLastName = "";
                covisionApplicationModel.fields.depMaritalStatus = "";
                covisionApplicationModel.fields.depRelationship = "";

                $("#dep_benefit_amount").val("");
                $("#dep-name").val("");
                $("#dep-surname").val("");
                $("#dep-id-number").val("");
                $("#dep-relation").val("");

                dontRun = true;
                brandNewDependent = false;
                $("#member-id").val("");
                window.app.mobileApp.navigate("components/covisionApplication/step_5.html", "slide:left reverse");

            } // if (validator.validate())**
            else {

                console.log("validation unsuccessfull!");
                console.log(addDependentFormValidate);

                navigator.notification.alert(
                        message, // the message
                        function() {}, // a callback
                        options.title, // a title
                        options.buttonText // the button text
                    ) // navigator.notification.alert
            } // else

        }, // addNewDependent: function ()

        getEasyPayRef: function() {

            console.log("getEasyPayRef: function ()");

            var restParams1 = {
                service: "mnt",
                parameters: {
                    // rqAuthentication: "user:admin|mip123" + "|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                   rqAuthentication: "user:hollardservice|mip123" + "|GSMUS|&login_company_branch_obj=91211.878*102594.188", 
                    rqDataMode: "VAR/JSON",
                    rqService: 'ContractService:getContractDetails',
                    ipcContractNumber: covisionApplicationModel.fields.policyNumber
                }, // parameters

                requestSuccess: function(data) {
                    console.log(restParams1.parameters.rqService + " - SUCCESS!")
                    console.log(data);
                    if (JSON.stringify(data.rqResponse.rqFullError) !== undefined) {

                        mipAlert.show(data.rqResponse.rqErrorMessage, {
                            title: 'Error'

                        }); // mipAlert.show(data.rqResponse.rqErrorMessage

                        console.log(JSON.stringify(data.rqResponse.rqFullError));

                    } // if (data.rqFullError !== "" || 
                    else {

                        // assign contract obj to the model
                        covisionApplicationModel.fields.policyNumber = data.rqResponse.opcContractNumber;
                        covisionApplicationModel.fields.policyObj = data.rqResponse.opdContractObj;

                        var restParams2 = {
                            service: "mnt",
                            parameters: {
                                // rqAuthentication: "user:admin|mip123" + "|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                                rqAuthentication: "user:hollardservice|mip123" + "|GSMUS|&login_company_branch_obj=91211.878*102594.188",
                                rqDataMode: "VAR/JSON",
                                rqService: "pfUtilityService:fetchEntityCriterionData",
                                pcOwningEntityMnemonic: "CBMCT",
                                pdOwningObj: covisionApplicationModel.fields.policyObj
                            }, // parameters

                            requestSuccess: function(data) {

                                console.log(restParams2.parameters.rqService + " - SUCCESS!");
                                console.log(data);

                                if (JSON.stringify(data.rqResponse.rqFullError) !== undefined) {
                                    console.log(JSON.stringify(data.rqResponse.rqFullError));

                                } // if (data.rqFullError !== "" ||
                                else {

                                    var ttEntityCriterion = data.rqResponse.dsEntityCriteria.ttEntityCriterion;

                                    for (var i = 0; i < ttEntityCriterion.length; i++) {
                                        for (var iType = 0; iType < ttEntityCriterion[i].ttCriterionType.length; iType++) {

                                            console.log(ttEntityCriterion[i].ttCriterionType[iType].cCriterionCode);

                                            if (ttEntityCriterion[i].ttCriterionType[iType].cCriterionCode === "EasyPayRef") {

                                                covisionApplicationModel.fields.easyPayReference = ttEntityCriterion[i].cCriterionValue;

                                                console.log(covisionApplicationModel.fields.easyPayReference + "::::::" + ttEntityCriterion[i].cCriterionValue);

                                                $("#easyPay-ref-num").empty();
                                                $("#easyPay-ref-num").prepend('<span id="easyPay-ref-num">' + covisionApplicationModel.fields.easyPayReference + '</span>');
                                                $("#easypay-reference").show(600);

                                            } // if (ttEntityCriterion[i].ttCriterionType[i].cCriterionCode === "EasyPayRef")

                                        } // for (var index = 0; index < ttEntityCriterion[i].ttCriterionType.length; index++)

                                    } // for (var i = 0; i < ttEntityCriterion.length; i++) {

                                } // else

                            }, // requestSuccess: function (data)

                            requestError: function(d, e, c) {

                                localStorage.setItem("mnt:sessionid", "");
                                localStorage.setItem("appData", "");

                                mipAlert.show(e, {
                                    title: c
                                }); // mipAlert.show(e)

                            }, // requestError

                            spinner: {
                                message: "Authenticating",
                                title: "",
                                isFixed: true
                            }, // spinner

                            availableOfflineFor: {
                                minutes: 120
                            } // availableOfflineFor

                        }; // var restParams2

                        console.log("about to make 2nd request")

                        mipRest.request(restParams2);

                    } // else

                }, // requestSuccess

                requestError: function(d, e, c) {

                    localStorage.setItem("mnt:sessionid", "");
                    localStorage.setItem("appData", "");

                    mipAlert.show(e, {
                        title: c
                    }); // mipAlert.show(e,

                }, // requestError

                spinner: {
                    message: "Authenticating",
                    title: "",
                    isFixed: true
                }, // spinner

                availableOfflineFor: {
                    minutes: 120
                } // availableOfflineFor

            }; // restParams1

            mipRest.request(restParams1);

        }, // getEasyPayRef: function () 

        newApplication: function() {
            console.log("newApplication: function ()");
            console.log($("#select-main-cover").val());

            covisionApplicationModel.fields.depCoverAmount = "";
            covisionApplicationModel.fields.depFirstNames + "";
            covisionApplicationModel.fields.depLastName = "";
            covisionApplicationModel.fields.depIdNumber = "";
            covisionApplicationModel.fields.depMaritalStatus = "";
            covisionApplicationModel.fields.depRelationship = "";
            covisionApplicationModel.fields.depCoverAmount = "";

            covisionApplicationModel.fields.title = "";
            covisionApplicationModel.fields.firstNames = "";
            covisionApplicationModel.fields.lastName = "";
            covisionApplicationModel.fields.idNumber = "";
            covisionApplicationModel.fields.maritalStatus = "";
            covisionApplicationModel.fields.email = "";
            covisionApplicationModel.fields.age = "";
            covisionApplicationModel.fields.birthDate = "";
            covisionApplicationModel.fields.benefitAmount = "";
            covisionApplicationModel.fields.premium = "";
            covisionApplicationModel.fields.gendertc = "";
            covisionApplicationModel.fields.gender = "";
            covisionApplicationModel.fields.totalMonthlyIncome = "";
            covisionApplicationModel.fields.cellNumber = "";
            covisionApplicationModel.fields.telWork = "";
            covisionApplicationModel.fields.telHome = "";
            covisionApplicationModel.fields.cellOrPost = "";

            /* POSTAL DETAILS */
            covisionApplicationModel.fields.postalStreet = "";
            covisionApplicationModel.fields.postalSuburb = "";
            covisionApplicationModel.fields.postalCity = "";
            covisionApplicationModel.fields.postalCode = "";

            /* RESIDENTIAL DETAILS */
            covisionApplicationModel.fields.residentialStreet = "";
            covisionApplicationModel.fields.residentialSuburb = "";
            covisionApplicationModel.fields.residentialCity = "";
            covisionApplicationModel.fields.residentialCode = "";

            /* BENEFICIARY DETAILS */
            covisionApplicationModel.fields.beneficiaryTitle = "";
            covisionApplicationModel.fields.beneficiaryName = "";
            covisionApplicationModel.fields.beneficiarySurname = "";
            covisionApplicationModel.fields.beneficiaryIDNumber = "";
            covisionApplicationModel.fields.beneficiaryBirthDate = "";
            covisionApplicationModel.fields.beneficiaryRelation = "";
            covisionApplicationModel.fields.beneficiaryContactNo = "";
            covisionApplicationModel.fields.beneficiaryGendertc = "";
            covisionApplicationModel.fields.beneficiaryGender = "";

            /* PAYMENT DETAILS (How will you pay the total amount) */
            covisionApplicationModel.fields.isPayer = true;
            covisionApplicationModel.fields.payerName = "";
            covisionApplicationModel.fields.payerSurname = "";
            covisionApplicationModel.fields.payerMonthlyIncome = "";
            covisionApplicationModel.fields.payerCell = "";
            covisionApplicationModel.fields.payerTelHome = "";
            covisionApplicationModel.fields.payerTelWork = "";
            covisionApplicationModel.fields.paymentMethod = "";
            covisionApplicationModel.fields.payerSignature = "";

            /* BANKING DETAILS (if paying by debit order) */
            covisionApplicationModel.fields.accOwnerTitle = "";
            covisionApplicationModel.fields.accOwnerName = "";
            covisionApplicationModel.fields.accOwnerSurname = "";
            covisionApplicationModel.fields.accOwnerIDNumber = "";
            covisionApplicationModel.fields.accOwnerTelWork = "";
            covisionApplicationModel.fields.accOwnerTelHome = "";
            covisionApplicationModel.fields.accBankName = "";
            covisionApplicationModel.fields.accBranchName = "";
            covisionApplicationModel.fields.accNumber = "";
            covisionApplicationModel.fields.accOwnerCell = "";
            covisionApplicationModel.fields.accDebitDate = "";
            covisionApplicationModel.fields.accBranchCode = "";
            covisionApplicationModel.fields.accType = "";
            covisionApplicationModel.fields.accOwnerSignature = "";

            /* FINALISING */
            covisionApplicationModel.fields.yourSignature = "";
            covisionApplicationModel.fields.yourSignDate = new Date();

            coveredPeople = [];

            console.log(covisionApplicationModel.fields);

            $("input[type=text]").val("");
            $("input[type=tell]").val("");
            $("input[type=email]").val("");
            $("input[type=number]").val("");


            window.app.mobileApp.navigate("components/covisionApplication/step_3.html", "slide");


        }, // newApplication: function ()

        scanEasyPay: function() {
            $("#easyPay-ref-num").empty();
            cordova.plugins.barcodeScanner.scan(
                function(result) {
                    alert("We got a barcode\n" +
                        "Result: " + result.text + "\n" +
                        "Format: " + result.format + "\n" +
                        "Cancelled: " + result.cancelled);



                    var restParams1 = {
                        parameters: {
                            // rqAuthentication: "user:admin|mip123" + '|GSMUS|&login_company_branch_obj=91211.878*102594.188',
                            rqAuthentication: "user:hollardservice|mip123" + '|GSMUS|&login_company_branch_obj=91211.878*102594.188',
                            rqDataMode: "VAR/JSON",
                            rqService: "ContractService:FetchContractByCriterion",
                            ipcCriterionCode: "EasyPayRef",
                            ipcCriterionValue: result.text
                        }, // parameters

                        requestSuccess: function(data) {

                            console.log("requestSuccess!  ContractService:FetchContractByCriterion");
                            console.log(data);
                            covisionApplicationModel.fields.policyNumber = data.rqResponse.dsContract.ttContract[0].cContractNumber;
                            covisionApplicationModel.fields.easyPayReference = result.text;
                            $("#easyPay-ref-num").append('<span>CONTRACT NUMBER</span><br><span class="lightgreen">' + data.rqResponse.dsContract.ttContract[0].cContractNumber + '</span><br><hr width="75%" color="#006652"><br><span>EASYPAY NUMBER</span><br><spanclass="lightgreen">' + result.text + '</span>');



                        }, // requestSuccess

                        requestError: function(d, e, c) {

                            console.log(d + " -- " + e + " -- " + c);

                            mipAlert.show(e, {
                                title: c
                            }); // mipAlert.show(e)
                        }, // requestError

                        spinner: {
                            message: "Logging in...",
                            title: "Login",
                            isFixed: true
                        }, // spinner

                        availableOfflineFor: {
                            minutes: 120
                        } // availableOfflineFor

                    }; // restParams1

                    mipRest.request(restParams1);

                },
                function(error) {
                    alert("Scanning failed: " + error);
                }, {
                    "preferFrontCamera": false, // iOS and Android
                    "showFlipCameraButton": true, // iOS and Android
                    "prompt": "Place a barcode inside the scan area", // supported on Android only
                    "formats": "CODE_128", // default: all but PDF_417 and RSS_EXPANDED
                    "orientation": "portrait" // Android only (portrait|landscape), default unset so it rotates with the device
                }
            );

        }, // scanEasyPay: function ()

        completeApplication: function() {
            mipAlert.show("The policy has been successfully created.", { title: "Success" });
            window.app.mobileApp.navigate("components/home/view.html", "slide");
        }, // completeApplication: function () 

        editDependent: function(depObj) {
            console.log(depObj);

            localStorage.setItem("depData", JSON.stringify(depObj));


            window.app.mobileApp.navigate("components/covisionApplication/add_dependent.html", "slide");

        },

        gotoBeneficiaryDetails: function() {
            navigator.notification.confirm(
                'Please confirm that the details entered are complete as you will not be able to change these at a later stage in the application process. Tap Ok to continue or Cancel to review.', // message
                onConfirmDepDetails, // callback to invoke with index of button pressed
                "Confirm", // title
                ['Ok', 'Cancel'] // buttonLabels
            );
            // window.app.mobileApp.navigate("components/covisionApplication/step_6.html", "slide");
        }, // gotoBeneficiaryDetails: function 

        startApplication: function() {
            if (fromQuote == true) {
                window.app.mobileApp.navigate("components/covisionApplication/step_2.html", "slide");
            } else {
                covisionApplicationModel.fields.depCoverAmount = "";
                covisionApplicationModel.fields.depFirstNames + "";
                covisionApplicationModel.fields.depLastName = "";
                covisionApplicationModel.fields.depIdNumber = "";
                covisionApplicationModel.fields.depMaritalStatus = "";
                covisionApplicationModel.fields.depRelationship = "";
                covisionApplicationModel.fields.depCoverAmount = "";

                covisionApplicationModel.fields.title = "";
                covisionApplicationModel.fields.firstNames = "";
                covisionApplicationModel.fields.lastName = "";
                covisionApplicationModel.fields.idNumber = "";
                covisionApplicationModel.fields.maritalStatus = "";
                covisionApplicationModel.fields.email = "";
                covisionApplicationModel.fields.age = "";
                covisionApplicationModel.fields.birthDate = "";
                covisionApplicationModel.fields.benefitAmount = "";
                covisionApplicationModel.fields.premium = "";
                covisionApplicationModel.fields.gendertc = "";
                covisionApplicationModel.fields.gender = "";
                covisionApplicationModel.fields.totalMonthlyIncome = "";
                covisionApplicationModel.fields.cellNumber = "";
                covisionApplicationModel.fields.telWork = "";
                covisionApplicationModel.fields.telHome = "";
                covisionApplicationModel.fields.cellOrPost = "";

                /* POSTAL DETAILS */
                covisionApplicationModel.fields.postalStreet = "";
                covisionApplicationModel.fields.postalSuburb = "";
                covisionApplicationModel.fields.postalCity = "";
                covisionApplicationModel.fields.postalCode = "";

                /* RESIDENTIAL DETAILS */
                covisionApplicationModel.fields.residentialStreet = "";
                covisionApplicationModel.fields.residentialSuburb = "";
                covisionApplicationModel.fields.residentialCity = "";
                covisionApplicationModel.fields.residentialCode = "";

                /* BENEFICIARY DETAILS */
                covisionApplicationModel.fields.beneficiaryTitle = "";
                covisionApplicationModel.fields.beneficiaryName = "";
                covisionApplicationModel.fields.beneficiarySurname = "";
                covisionApplicationModel.fields.beneficiaryIDNumber = "";
                covisionApplicationModel.fields.beneficiaryBirthDate = "";
                covisionApplicationModel.fields.beneficiaryRelation = "";
                covisionApplicationModel.fields.beneficiaryContactNo = "";
                covisionApplicationModel.fields.beneficiaryGendertc = "";
                covisionApplicationModel.fields.beneficiaryGender = "";

                /* PAYMENT DETAILS (How will you pay the total amount) */
                covisionApplicationModel.fields.isPayer = true;
                covisionApplicationModel.fields.payerName = "";
                covisionApplicationModel.fields.payerSurname = "";
                covisionApplicationModel.fields.payerMonthlyIncome = "";
                covisionApplicationModel.fields.payerCell = "";
                covisionApplicationModel.fields.payerTelHome = "";
                covisionApplicationModel.fields.payerTelWork = "";
                covisionApplicationModel.fields.paymentMethod = "";
                covisionApplicationModel.fields.payerSignature = "";

                /* BANKING DETAILS (if paying by debit order) */
                covisionApplicationModel.fields.accOwnerTitle = "";
                covisionApplicationModel.fields.accOwnerName = "";
                covisionApplicationModel.fields.accOwnerSurname = "";
                covisionApplicationModel.fields.accOwnerIDNumber = "";
                covisionApplicationModel.fields.accOwnerTelWork = "";
                covisionApplicationModel.fields.accOwnerTelHome = "";
                covisionApplicationModel.fields.accBankName = "";
                covisionApplicationModel.fields.accBranchName = "";
                covisionApplicationModel.fields.accNumber = "";
                covisionApplicationModel.fields.accOwnerCell = "";
                covisionApplicationModel.fields.accDebitDate = "";
                covisionApplicationModel.fields.accBranchCode = "";
                covisionApplicationModel.fields.accType = "";
                covisionApplicationModel.fields.accOwnerSignature = "";

                /* FINALISING */
                covisionApplicationModel.fields.yourSignature = "";
                covisionApplicationModel.fields.yourSignDate = new Date();

                coveredPeople = [];

                console.log(covisionApplicationModel.fields);

                $("input[type=text]").val("");
                $("input[type=tell]").val("");
                $("input[type=email]").val("");
                $("input[type=number]").val("");

                $("#main-benefit-amount").val("");
                $("#main-id-number").val("");
                $("#select-main-marital").val("");
                $("#select-main-salary").val("");

                $("#dep-name").val("");
                $("#dep-surname").val("");
                $("#dep-id-number").val("");
                $("#member-id").val("");
                $("#dep-benefit-amount").val("");
                $("#dep-marital").val("");
                $("#dep-relation").val("");
                $("#currentDependentList").empty();

                $("#select-bene-relation").val("");

                localStorage.setItem("defaultJSON", "");
                coveredPeople = [];

                fromQuote = false;

                window.app.mobileApp.navigate("components/covisionApplication/step_2.html", "slide");
            }
        },
        cascadeCoverAmount: function() {
            // var mainCoverAmount = '15000', // use for testing[must be gotten from the main cover amount that will be assigned to localStorage]
            var mainCoverAmount = parseInt(localStorage.getItem("mainCoverAmount"));
            spouseCoverAmount = ["", 50000, 45000, 40000, 35000, 30000, 25000, 20000, 15000, 10000, 5000],
                parentCoverAmount = ["", 20000, 19000, 18000, 17000, 16000, 15000, 14000, 13000, 12000, 11000, 10000, 9000, 8000, 7000, 6000, 5000, 4000, 3000, 2000, 1000],
                extmemberCoverAmount = ["", 10000, 9000, 8000, 7000, 6000, 5000, 4000, 3000, 2000, 1000];

            console.log("main cover amount : " + mainCoverAmount + " || typeof : " + typeof mainCoverAmount);

            $("#dep_benefit_amount").empty();

            if (($("#dep-relation").val()).toLowerCase() == 'child') {

                $("#cover-amount-li").hide(500);

            } // if (($("#dep-relation").val()).toString() == 'child')
            else if (($("#dep-relation").val()).toLowerCase() == 'spouse') {

                $("#cover-amount-li").show(500);
                for (var i = 0; i < spouseCoverAmount.length; i++) {

                    console.log(parseInt(spouseCoverAmount[i]) + " ---- " + parseInt(mainCoverAmount));
                    if (spouseCoverAmount[i] <= mainCoverAmount) {

                        if (spouseCoverAmount[i] == "") {

                            $("#dep_benefit_amount").append('<option value="' + spouseCoverAmount[i] + '"></option>');
                        } // if (spouseCoverAmount[i] == "")
                        else {

                            $("#dep_benefit_amount").append('<option value="' + spouseCoverAmount[i] + '">R ' + spouseCoverAmount[i] + '</option>');
                        } // else

                    } // if (spouseCoverAmount[i] <= mainCoverAmount) 

                } // for (var i = 0; i < spouseCoverAmount.length; i++)

            } // if (($("#dep-relation").val()).toString() == 'spouse')
            else if (($("#dep-relation").val()).toLowerCase() == 'parent') {

                $("#cover-amount-li").show(500);
                for (var i = 0; i < parentCoverAmount.length; i++) {

                    console.log(parseInt(parentCoverAmount[i]) + " ---- " + parseInt(mainCoverAmount));

                    if (parentCoverAmount[i] <= mainCoverAmount) {

                        if (parentCoverAmount[i] == "") {

                            $("#dep_benefit_amount").append('<option value="' + parentCoverAmount[i] + '"></option>');
                        } // if (parentCoverAmount[i] == "")
                        else {

                            $("#dep_benefit_amount").append('<option value="' + parentCoverAmount[i] + '">R ' + parentCoverAmount[i] + '</option>');
                        } // else

                    } // if (parentCoverAmount[i] <= mainCoverAmount) 

                } // for (var i = 0; i < parentCoverAmount.length; i++)

            } // if (($("#dep-relation").val()).toString() == 'parent')
            else {

                $("#cover-amount-li").show(500);
                for (var i = 0; i < extmemberCoverAmount.length; i++) {

                    console.log(parseInt(extmemberCoverAmount[i]) + " ---- " + parseInt(mainCoverAmount));

                    if (extmemberCoverAmount[i] <= mainCoverAmount) {

                        if (extmemberCoverAmount[i] == "") {

                            $("#dep_benefit_amount").append('<option value="' + extmemberCoverAmount[i] + '"></option>');
                        } // if (extmemberCoverAmount[i] == "")
                        else {

                            $("#dep_benefit_amount").append('<option value="' + extmemberCoverAmount[i] + '">R ' + extmemberCoverAmount[i] + '</option>');
                        } // else

                    } // if (extmemberCoverAmount[i] <= mainCoverAmount) 

                } // for (var i = 0; i < extmemberCoverAmount.length; i++)

            } // else

        }, // cascadeCoverAmount: function ()

    }); // var covisionApplicationModel = kendo.observable

    parent.set("covisionApplicationModel", covisionApplicationModel);

})(app.covisionApplication);
// END_CUSTOM_CODE_covisionApplication

function onConfirmDepDetails(buttonIndex) {
    if (parseInt(buttonIndex) === 1) {
        fromQuote = false;
        window.app.mobileApp.navigate("components/covisionApplication/step_6.html", "slide");
    } else {
        // Do nothing...
    }
}