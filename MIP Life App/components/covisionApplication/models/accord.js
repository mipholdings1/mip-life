var contractRequestData,
    mainPerson;



function createAccordXML(data) {

    effectiveDate = new Date();

    var month = effectiveDate.getMonth() + 2,
        day = '01',
        year = effectiveDate.getFullYear();

    // if (effectiveDate.getDate > 15) {
    //     month = '' + (effectiveDate.getMonth() + 2);
    // } else {
    //     month = '' + (effectiveDate.getMonth() + 1);
    // }

    if (month < 10) {
        month = "0" + month
    }

    effectiveDate = [year, month, day].join("-");
    console.log("createAccordXML");
    console.log("Effective Date: " + effectiveDate);
    console.log(data);
    contractRequestData = "";

    addOlifeNode();

    for (var i = 0; i < data.length; i++) {
        addPersonNode(data[i]);
    }

    addCampaignNode();
    addHoldingNode();
    for (var i = 0; i < data.length; i++) {
        if (typeof data[i].collectionDetail[0] === 'object' || app.covisionApplication.covisionApplicationModel.fields.paymentMethod === "cash") {
            addPolicyNode(data[i]);
            break;
        }
    }

    var familyCoverAdded = 0;
    for (var i = 0; i < data.length; i++) {
        if (data[i].relationship === 'spouse' || data[i].relationship === 'child') {
            familyCoverAdded++;
            if (familyCoverAdded == 1) {
                addFamilyCoverage(data);
            }
        } else {
            addPersonCoverage(data[i]);
        }
    }

    closeHoldingNode();

    for (var i = 0; i < data.length; i++) {
        addPersonRelation(data[i]);
    }

    closeOlifeNode();

    return contractRequestData.replace(/    /g, "");

    // console.log(data);
    // console.log($.parseXML(contractRequestData.replace(/    /g, "")));
    // console.log(contractRequestData.replace(/    /g, ""));
} // createAccordXML

function addOlifeNode() {
    contractRequestData = '<TXLife>\
    <UserAuthRequest/>\
    <TXLifeRequest PrimaryObjectID="new_Policy">\
        <TransRefGUID>adf6e647-5923-423b-8157-8445206dbe21</TransRefGUID>\
        <TransType tc="103">NewPolicy</TransType>\
        <InquiryLevel tc="1">ObjectOnly</InquiryLevel>\
        <OLifE>';
} // addXMLHeader

function addPersonNode(personObj) {
    // console.log(personObj);


    if ((personObj.roleCode).toLowerCase() === "agent") {
        contractRequestData = contractRequestData + '<Party id="Producer_' + personObj.firstNames + '" DataRep="Full">\
                                <FullName>' + personObj.firstNames + '</FullName>\
                                <Client>\
                                  <PrefLanguage tc="9">English</PrefLanguage>\
                                </Client>\
                                <Producer id="Producer_' + personObj.firstNames + '">\
                                  <NIPRNumber>' + personObj.firstNames + '</NIPRNumber>\
                                </Producer>\
                              </Party>';
    } // if ((personObj.roleCode).toLowerCase() === "agent")
    else {
        contractRequestData = contractRequestData + '<Party id="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" tc="1" DataRep="Full">\
                            <PartyTypeCode tc="1">Person</PartyTypeCode>\
                            <FullName>' + personObj.firstNames + " " + personObj.lastName + '</FullName>\
                            <ResidenceCountry>ZAF</ResidenceCountry>\
                            <IDReferenceNo>' + personObj.idNumber + '</IDReferenceNo>\
                            <IDReferenceType tc="4">NationalIdentityNumber</IDReferenceType>';

        if (typeof personObj.address === "object") {
            for (var i = 0; i < personObj.address.length; i++) {
                var addressTC = "";
                var addressType = "";
                if (personObj.address[i].type === 'postal') {
                    addressTC = '17';
                    addressType = 'Mailing';
                } else if (personObj.address[i].type === 'physical') {
                    addressTC = '1';
                    addressType = 'Residential';
                }

                contractRequestData = contractRequestData + '<Address id="StreeAddress_' + i + '_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" DataRep="Partial">\
                <AddressTypeCode tc="' + addressTC + '">' + addressType + '</AddressTypeCode>\
                <Line1>43</Line1>\
                <Line3>' + personObj.address[i].street + '</Line3>\
                <Line4>' + personObj.address[i].suburb + '</Line4>\
                <City>' + personObj.address[i].city + '</City>\
                <AddressState>' + personObj.address[i].province + '</AddressState>\
                <Zip>' + personObj.address[i].code + '</Zip>\
                <AddressCountry>SouthAfrica</AddressCountry>\
            </Address>';

            }
        }
        if (personObj.cellNumber !== "" && personObj.cellNumber !== undefined) {
            contractRequestData = contractRequestData + '<Phone id="Phone3_0" DataRep="Partial">\
        <PhoneTypeCode tc="12">Mobile</PhoneTypeCode>\
        <DialNumber>' + personObj.cellNumber + '</DialNumber>\
    </Phone>';
        }

        contractRequestData = contractRequestData + '<Client>\
        <PrefLanguage tc="9">English</PrefLanguage>\
    </Client>'

        var genderTC = '',
            title = '';
        if (personObj.gender === 'male') {
            genderTC = '1';
            title = 'Mr';
            // console.log("1)" + personObj.gender);

        } else if (personObj.gender === 'female') {
            genderTC = '2';

            if (personObj.maritalStatus === 'single') {
                title = "Ms";
            } else {
                title = "Mrs";
            }

            // console.log("2)" + personObj.gender);
        }

        personObj.title = title;

        contractRequestData = contractRequestData + '<Person id="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" DataRep="Full">\
        <FirstName>' + personObj.firstNames + '</FirstName>\
        <Initials>' + personObj.firstNames.substring(0, 1) + '</Initials>\
        <LastName>' + personObj.lastName + '</LastName>\
        <MarStat tc="2">Single</MarStat>\
        <Gender>\
            <tc>' + genderTC + '</tc>\
            <Gender>' + personObj.gender + '</Gender>\
        </Gender>\
        <BirthDate>' + personObj.dateOfBirth + '</BirthDate>\
        <Prefix>' + personObj.title + '</Prefix>\
    </Person>';

        if (typeof personObj.bankAccount === "object") {
            for (var iBank = 0; iBank < personObj.bankAccount.length; iBank++) {
                contractRequestData = contractRequestData + '<Banking id="Banking_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" DataRep="Full">\
                <CreditDebitType tc="2">\
                    <tc>' + personObj.bankAccount[iBank].accountType + '</tc>\
                </CreditDebitType>\
                <BankAcctType tc="1">' + personObj.bankAccount[iBank].accountType + '</BankAcctType>\
                <AccountNumber>' + personObj.bankAccount[iBank].accountNumber + '</AccountNumber>\
                <AcctHolderName>' + personObj.firstNames + ' ' + personObj.lastName + '</AcctHolderName>\
                <BankName>' + personObj.bankAccount[iBank].bank + '</BankName>\
                <BankBranchName>' + personObj.bankAccount[iBank].branchName + '</BankBranchName>\
                <RoutingNum>' + personObj.bankAccount[iBank].branchCode + '</RoutingNum>\
            </Banking>'
            }
        }
        contractRequestData = contractRequestData + ' </Party>';
    } // else 
} // addPersonNode

function addCampaignNode() {
    contractRequestData = contractRequestData + '<Campaign id="Campaign_">\
        <CampaignCode>COV003</CampaignCode>\
    </Campaign>';
} // addCampaignNode

function addHoldingNode() {
    contractRequestData = contractRequestData + '<Holding id="Holding_1">\
        <HoldingTypeCode tc="2">Policy</HoldingTypeCode>\
        <HoldingForm tc="1">Individual</HoldingForm>';
    if (app.covisionApplication.covisionApplicationModel.fields.easyPayReference != "") {
        contractRequestData = contractRequestData + '<HoldingSyskey>' + app.covisionApplication.covisionApplicationModel.fields.policyObj + '</HoldingSyskey>';
    } // if (covisionApplicationModel.fields.easyPayReference != "")
} // addHoldingNode

function addPolicyNode(policyPayerObj) {
    // to be completed
    // TODO: add in the product code
    // TODO: add in tc's for PaymentMode, PaymentMethod
    // TODOL add in PaymentDraftDay
    // 
    console.log("policyP object:");
    console.log(policyPayerObj);
    contractRequestData = contractRequestData + '<Policy id="Policy_1" ProductID="" CarrierPartyID="" BankID="">\
        <LineOfBusiness tc="1">Life</LineOfBusiness>'

    if (app.covisionApplication.covisionApplicationModel.fields.easyPayReference != "") {
        contractRequestData = contractRequestData + '<PolNumber>' + app.covisionApplication.covisionApplicationModel.fields.policyNumber + '</PolNumber>';
    } // if (covisionApplicationModel.fields.easyPayReference != "") {

    contractRequestData = contractRequestData + '<PolicyStatus tc="57">Quoted</PolicyStatus>\
        <ApplicationInfo>\
            <ApplicationType tc="1">New Application</ApplicationType>\
        </ApplicationInfo>\
        <ProductCode>8040</ProductCode>\
        <EffDate>' + effectiveDate + '</EffDate>\
        <PaymentMode tc="4">MonthOrMonthly</PaymentMode>';
    var paymentTC = "";
    if (typeof policyPayerObj.collectionDetail[0] === 'object') {
        if (policyPayerObj.collectionDetail[0].collectionMethod === "debitOrder") {
            paymentTC = "7";
        } else {
            paymentTC = "1045300022";
        }
    }

    contractRequestData = contractRequestData + '<PaymentMethod tc="' + paymentTC + '">' + policyPayerObj.collectionDetail[0].collectionMethod + '</PaymentMethod>';

    contractRequestData = contractRequestData + '<PaymentDraftDay>25</PaymentDraftDay>\
        <Life id="Life_LifeProductSeriesIndividualFuneralBenefit">';
}

function addFamilyCoverage(familyNodes) {
    console.log("addFamlily Coverage");
    console.log($("#family_benefit_amount").val());

    var rolecodeTC = "";
    var familyCoverAdded = 0;
    if (app.covisionApplication.covisionApplicationModel.fields.famCoverAmount !== "") {

        contractRequestData = contractRequestData + '<Coverage id="family_cover">\
        <ProductCode>1110</ProductCode>\
        <OLifEExtension VendorCode="453" ExtensionCode="CoverageExtension">\
            <CoverageExtension>\
                <PackageCode>' + app.covisionApplication.covisionApplicationModel.fields.famCoverAmount + '</PackageCode>\
            </CoverageExtension>\
        </OLifEExtension>\
        <EffDate>' + effectiveDate + '</EffDate>';
        for (var i = 0; i < familyNodes.length; i++) {
            var rolecodeTC = "";

            switch (familyNodes[i].relationship) {
                case "spouse":
                    rolecodeTC = "1";
                    break;
                case "child":
                    rolecodeTC = "3";
                    break;
            }

            if (familyNodes[i].relationship === "spouse") {
                contractRequestData = contractRequestData + '<EffDate>' + effectiveDate + '</EffDate>\
                    <LifeParticipant id="Participant_' + familyNodes[i].relationship + '_' + familyNodes[i].idNumber + '_' + familyNodes[i].roleCode + '" PartyID="Person_' + familyNodes[i].relationship + '_' + familyNodes[i].idNumber + '_' + familyNodes[i].roleCode + '">\
                        <LifeParticipantRoleCode tc="' + rolecodeTC + '">' + familyNodes[i].relationship + '</LifeParticipantRoleCode>\
                    </LifeParticipant>';
            } else if (familyNodes[i].relationship === "child") {
                contractRequestData = contractRequestData + '<EffDate>' + effectiveDate + '</EffDate>\
                    <LifeParticipant id="Participant_' + familyNodes[i].relationship + '_' + familyNodes[i].idNumber + '_' + familyNodes[i].roleCode + '" PartyID="Person_' + familyNodes[i].relationship + '_' + familyNodes[i].idNumber + '_' + familyNodes[i].roleCode + '">\
                        <LifeParticipantRoleCode tc="' + rolecodeTC + '">' + familyNodes[i].relationship + '</LifeParticipantRoleCode>\
                    </LifeParticipant>';
            }

        }
        contractRequestData = contractRequestData + "</Coverage>";
    }
}

function addPersonCoverage(personObj) {
    console.log("coverage:")
    console.log(personObj)
    if (typeof personObj.coveredBenefits === 'object' && (personObj.roleCode).toLowerCase() === "lifea") {
        contractRequestData = contractRequestData + '<Coverage id="' + personObj.relationship + '_cover_' + personObj.idNumber + '_' + personObj.roleCode + '">\
            <ProductCode>' + personObj.coveredBenefits[0].benefitCode + '</ProductCode>';
        if (personObj.coveredBenefits[0].benefitCode === "1100" || personObj.coveredBenefits[0].benefitCode === "1110") {
            contractRequestData = contractRequestData + '<OLifEExtension VendorCode="453" ExtensionCode="CoverageExtension">\
             <CoverageExtension>\
                <PackageCode>' + personObj.coveredBenefits[0].benefitAmount + '</PackageCode>\
               </CoverageExtension>\
            </OLifEExtension>'
        } else {
            contractRequestData = contractRequestData + '<CurrentAmt>' + personObj.coveredBenefits[0].benefitAmount + '</CurrentAmt>';
        }

        var rolecodeTC = "";

        switch (personObj.relationship) {
            case "spouse":
                rolecodeTC = "1";
                break;
            case "child":
                rolecodeTC = "3";
                break;
            case "self":
                rolecodeTC = "91";
                break;
            case "parent":
                rolecodeTC = "3";
                break;
            case "aunt":
                rolecodeTC = "31";
                break;
            case "uncle":
                rolecodeTC = "30";
                break;
            case "niece":
                rolecodeTC = "33";
                break;
            case "nephew":
                rolecodeTC = "32";
                break;
            case "adult child":
                rolecodeTC = "1045300026";
                break;
            case "brother":
                rolecodeTC = "7";
                break;
            case "sister":
                rolecodeTC = "8";
                break;
            default:
                rolecodeTC = "32";
        }

        contractRequestData = contractRequestData + '<EffDate>' + effectiveDate + '</EffDate>\
            <LifeParticipant id="Participant_' + personObj.relationship + '_' + personObj.idNumber + '" PartyID="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '">\
                <LifeParticipantRoleCode tc="' + rolecodeTC + '">' + personObj.relationship + '</LifeParticipantRoleCode>\
            </LifeParticipant>\
        </Coverage>';
    } else {

        console.log('personObj.coveredBenefits is not an object!');
        console.log(personObj);
    }
} // addPersonCoverage

function closeHoldingNode() {
    contractRequestData = contractRequestData + '</Life>\
        </Policy>\
    </Holding>';
} // closeHoldingNode

function addPersonRelation(personObj) {
    if (personObj.relationship === 'self') {
        mainPerson = personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode;
    }

    var rolecodeTC = "";

    console.log("addPerson Relations..");
    console.log((personObj.roleCode).toLowerCase());

    if ((personObj.roleCode).toLowerCase() === 'lifea') {
        console.log("adding lifeA");
        switch ((personObj.relationship).toLowerCase()) {
            case "spouse":
                console.log("adding sppouse");
                rolecodeTC = "1";
                contractRequestData = contractRequestData + '<Relation id="Relation_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" OriginatingObjectID="Person_' + mainPerson + '" RelatedObjectID="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="6">Party</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="' + rolecodeTC + '">Spouse</RelationRoleCode>\
                </Relation>';
                break;
            case "child":
                console.log("adding child");
                rolecodeTC = "2";
                contractRequestData = contractRequestData + '<Relation id="Relation_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" OriginatingObjectID="Person_' + mainPerson + '" RelatedObjectID="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="6">Party</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="' + rolecodeTC + '">Child</RelationRoleCode>\
                </Relation>';
                break;
            case "self":
                console.log("adding self");
                rolecodeTC = "91";
                contractRequestData = contractRequestData + '<Relation id="Relation_Insured" OriginatingObjectID="holding_1" RelatedObjectID="Person_' + mainPerson + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="4">Holding</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="32">Insured</RelationRoleCode>\
                </Relation>';
                console.log('added main insured');

                contractRequestData = contractRequestData + '<Relation id="Relation_Owner" OriginatingObjectID="holding_1" RelatedObjectID="Person_' + mainPerson + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="4">Holding</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="8">Owner</RelationRoleCode>\
                    <InterestPercent>100</InterestPercent>\
                </Relation>';
                console.log('added main owner');

                if (app.covisionApplication.covisionApplicationModel.fields.isPayer) {
                    contractRequestData = contractRequestData + '<Relation id="Relation_Payer" OriginatingObjectID="holding_1" RelatedObjectID="Person_' + mainPerson + '">\
                        <StartDate>2016-05-01</StartDate>\
                        <OriginatingObjectType tc="4">Holding</OriginatingObjectType>\
                        <RelatedObjectType tc="6">Party</RelatedObjectType>\
                        <RelationRoleCode tc="31">PolicyPayer</RelationRoleCode>\
                    </Relation>';
                }
                console.log("ISPAYER::::::::");
                console.log(app.covisionApplication.covisionApplicationModel.fields.isPayer)
                console.log('added main payer');
                break;
            case "parent":
                console.log("adding parent");
                rolecodeTC = "3";
                contractRequestData = contractRequestData + '<Relation id="Relation_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" OriginatingObjectID="Person_' + mainPerson + '" RelatedObjectID="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="6">Party</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="' + rolecodeTC + '">Parent</RelationRoleCode>\
                </Relation>';
                break;

            case "aunt":
                console.log("adding aunt");
                rolecodeTC = "31";
                contractRequestData = contractRequestData + '<Relation id="Relation_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" OriginatingObjectID="Person_' + mainPerson + '" RelatedObjectID="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="6">Party</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="' + rolecodeTC + '">Aunt</RelationRoleCode>\
                </Relation>';
                break;

            case "uncle":
                console.log("adding uncle");
                rolecodeTC = "30";
                contractRequestData = contractRequestData + '<Relation id="Relation_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" OriginatingObjectID="Person_' + mainPerson + '" RelatedObjectID="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="6">Party</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="' + rolecodeTC + '">Uncle</RelationRoleCode>\
                </Relation>';
                break;

            case "niece":
                console.log("adding niece");
                rolecodeTC = "33";
                contractRequestData = contractRequestData + '<Relation id="Relation_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" OriginatingObjectID="Person_' + mainPerson + '" RelatedObjectID="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="6">Party</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="' + rolecodeTC + '">Niece</RelationRoleCode>\
                </Relation>';
                break;

            case "nephew":
                console.log("adding nephew");
                rolecodeTC = "32";
                contractRequestData = contractRequestData + '<Relation id="Relation_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" OriginatingObjectID="Person_' + mainPerson + '" RelatedObjectID="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="6">Party</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="' + rolecodeTC + '">Nephew</RelationRoleCode>\
                </Relation>';
                break;

            case "adult child":
                console.log("adding adult child");
                rolecodeTC = "1045300026";
                contractRequestData = contractRequestData + '<Relation id="Relation_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" OriginatingObjectID="Person_' + mainPerson + '" RelatedObjectID="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="6">Party</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="' + rolecodeTC + '">Adult Child</RelationRoleCode>\
                </Relation>';
                break;

            case "brother":
                console.log("adding brother");
                rolecodeTC = "7";
                contractRequestData = contractRequestData + '<Relation id="Relation_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" OriginatingObjectID="Person_' + mainPerson + '" RelatedObjectID="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="6">Party</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="' + rolecodeTC + '">Brother</RelationRoleCode>\
                </Relation>';
                break;

            case "sister":
                console.log("adding sister");
                rolecodeTC = "7";
                contractRequestData = contractRequestData + '<Relation id="Relation_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" OriginatingObjectID="Person_' + mainPerson + '" RelatedObjectID="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="6">Party</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="' + rolecodeTC + '">Sister</RelationRoleCode>\
                </Relation>';
                break;

            default:
                break;
                // rolecodeTC = "32";
        }
    } //  if ((personObj.roleCode).toLowerCase === 'lifea') 
    else if ((personObj.roleCode).toLowerCase() === "bene") {
        //rolecodeTC = "3";
        contractRequestData = contractRequestData + '<Relation id="Relation_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '" OriginatingObjectID="Person_' + mainPerson + '" RelatedObjectID="Person_' + personObj.relationship + '_' + personObj.idNumber + '_' + personObj.roleCode + '">\
                    <StartDate>2016-05-01</StartDate>\
                    <OriginatingObjectType tc="4">Holding</OriginatingObjectType>\
                    <RelatedObjectType tc="6">Party</RelatedObjectType>\
                    <RelationRoleCode tc="34">Benefactor</RelationRoleCode>\
                    <InterestPercent>100</InterestPercent>\
                </Relation>';
    } //  else if ((personObj.roleCode).toLowerCase() === "bene") {

    if ((personObj.roleCode).toLowerCase() === "agent") {
        contractRequestData = contractRequestData + '<Relation id="Relation_' + personObj.roleCode + '" OriginatingObjectID="Holding_1" RelatedObjectID="Producer_' + personObj.firstNames + '">\
                            <StartDate>2016-08-01</StartDate>\
                            <OriginatingObjectType tc="4">Holding</OriginatingObjectType>\
                            <RelatedObjectType tc="6">Party</RelatedObjectType>\
                            <RelationRoleCode tc="11">Agent</RelationRoleCode>\
                            <OLifEExtension VendorCode="453" ExtensionCode="PartyExtension" />\
                            <OLifEExtension VendorCode="453" ExtensionCode="RelationExtension" />\
                          </Relation>';
    } // if ((personObj.roleCode).toLowerCase() === "agent")

} // addPersonRelation

function closeOlifeNode() {
    contractRequestData = contractRequestData + '</OLifE></TXLifeRequest></TXLife>';
} // closeOlifeNode