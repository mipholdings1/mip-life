var member = kendo.data.Model.define({
    title: "",
    memberId: "",
    firstNames: "",
    lastName: "",
    idNumber: "",
    maritalStatusTC: "",
    maritalStatus: "",
    email: "",
    cellNo: "",
    telWork: "",
    telHome: "",
    genderTC: "",
    gender: "",
    dateOfBirth: "",
    roleCode: "",
    relationShipTC: "",
    relationship: "",
    totalMonthlySalary: "",
    coveredBenefits: [],
    collectionDetail: {},
    address: [],
    bankAccount: []

});

var coveredBenefits = kendo.data.Model.define({
    benefitCode: "",
    benefitAmount: ""
});
var address = kendo.data.Model.define({
    type: "",
    street: "",
    suburb: "",
    city: "",
    code: "" 
});

var collectionDetail = kendo.data.Model.define({
    collectionMethod: "",
    collectionDay: ""
});

var bankAccount = kendo.data.Model.define({
    accountType: "",
    bank: "",
    branchName: "",
    branchCode: "",
    accountNumber: ""

});