'use strict';

app.membershipDetails = kendo.observable({
    onShow: function () {
        if (localStorage.getItem("appData") === null) {
            localStorage.setItem("appData", "")
        }
    },
    afterShow: function () {}
}); // app.membershipDetails = kendo.observable

// START_CUSTOM_CODE_membershipDetails
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes
(function (parent) {
    var membershipDetailsModel = kendo.observable({
        sendCard: function () {
			var personDetails = JSON.parse(localStorage.getItem("appData")).personalDetails[0];
            var membershipDetails = JSON.parse(localStorage.getItem("appData")).membershipDetails[0]
            var backBase64 = document.getElementById('CardBack').toDataURL(),
                filenameBack = 'LegalWiseCardBack.png',
                base64partsBack = backBase64.split(',');

            base64partsBack[0] = "base64:" + escape(filenameBack) + "//";

            var compatibleAttachmentBack = base64partsBack.join("");
            compatibleAttachmentBack = backBase64;


            var c = document.createElement("CANVAS");

            c.height = 500;
            c.width = 300;

            var ctx = c.getContext("2d");
            var img = document.getElementById("cardImage");

            ctx.drawImage(img, 0, 0);

            var frontBase64 = c.toDataURL(),
                filenameFront = 'LegalWiseCardFront.png',
                base64partsFront = frontBase64.split(',');

            base64partsFront[0] = "base64:" + escape(filenameFront) + "//";

            var compatibleAttachmentFront = base64partsFront.join("");
            compatibleAttachmentFront = frontBase64;

            var to = "",
                subject = "Legalwise membership card for " + personDetails.firstName + " " + personDetails.lastName + "("+ membershipDetails.memberNumber +")",
                body = "Please find attached copies of the membership card",
                attachments = [compatibleAttachmentFront, compatibleAttachmentBack];
            window.plugins.socialsharing.share(body, subject, attachments, null);
            //mipEmail.sendCard(to, subject, body, attachments);
        },
        testSocial: function () {
            window.plugins.socialsharing.share('Message, subject, image and link', 'The subject', ['https://www.google.nl/images/srpr/logo4w.png'], 'http://www.x-services.nl')

        }
    });

    parent.set('membershipDetailsModel', membershipDetailsModel);
})(app.membershipDetails);
// END_CUSTOM_CODE_membershipDetails